## Keyoxide: Privacy policy

Welcome to the Keyoxide app for Android and iOS!

This is an open source app developed by Berker Sen. The source code is available on Codeberg under the AGPL v3.0 license; the app is also available on Google Play Store and Apple App Store.

As an Android user myself, I take privacy very seriously.
I know how irritating it is when apps collect your data without your knowledge.

I hereby state, to the best of my knowledge and belief, that I have not programmed this app to collect any personally identifiable information. All data (app preferences (like theme, etc.) and user profiles) created by the you (the user) is stored on your device only, and can be simply erased by clearing the app's data or uninstalling it.

### Explanation of permissions requested in the app

The list of permissions required by the app can be found in the `AndroidManifest.xml` and `Info.plist` files:

https://codeberg.org/Berker/keyoxide-flutter/src/branch/master/android/app/src/main/AndroidManifest.xml
<br/>
https://codeberg.org/Berker/keyoxide-flutter/src/branch/master/ios/Runner/Info.plist

<br/>

|          Permission           | Why it is required                                               |
|:-----------------------------:|------------------------------------------------------------------|
| `android.permission.INTERNET` | This is required in order to make API calls to view the profile. |                                                                                                                                                                                                                |


 <hr style="border:1px solid gray">

If you find any security vulnerability that has been inadvertently caused by me, or have any question regarding how the app protects your privacy, please send me an email or post a discussion on Codeberg, and I will surely try to fix it/help you.

Yours sincerely,  
Berker Sen
<br/>
keyoxide@fastmail.com
