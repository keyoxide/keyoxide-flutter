# Get translations

#Curl script for downloading translations
#parameter {1} Language
#parameter {2} sublink

downloadTranslation() {
  curl -L  -o ../lang/"${1}".json https://translate.codeberg.org/download/keyoxide/keyoxide-mobile/"${2}" --fail --show-error --create-dirs
}

cd bash_scripts || exit

downloadTranslation "en" "en"
downloadTranslation "de" "de"
downloadTranslation "es" "es"
downloadTranslation "fr" "fr"
downloadTranslation "gl" "gl"
downloadTranslation "nl" "nl"
downloadTranslation "pl" "pl"
downloadTranslation "pt" "pt_BR"
downloadTranslation "tr" "tr"
downloadTranslation "zh" "zh_HANS"
downloadTranslation "ja" "ja"

echo -n "Downloading translations"

echo " DONE"
exit 0