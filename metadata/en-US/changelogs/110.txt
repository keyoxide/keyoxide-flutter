## [1.1.0]
### Added
- Built-in QR code scanner
- CHANGELOG.md
- New screenshots to reflect the changes
### Changed
- Transparent app bar
- Extended profile screen height
### Fixed
- Paddings on homepage