Added
- ASP Profile management
    - create, edit, delete, import, export profiles
    - manage claims
- Save multiple PGP & ASP profiles
- Custom domain and validation support for profile uploads and look-ups
- Check and display server update availability
- Biometric, password, PIN or pattern authentication for profile management
- User defined settings and data encryption
- Translations in Portuguese, Chinese
- Onboarding

Fixed
- Numerous bugs and UI inconsistencies

Changed
- New localization service with in-app translation strings
- User view, Display Profile and Settings UI
- Updated packages
- Updated flutter to 3.16.9