A modern, secure and privacy-friendly platform to establish your decentralized online identity

Keyoxide allows you to prove “ownership” of accounts on websites, domain names, IM, etc., regardless of your username.

That last part is important: you could, for example, be ‘alice’ on Lobste.rs, but ‘@alice24’ on Twitter.
And if your website is ‘thatcoder.tld’, how are people supposed to know that all that online property is yours?

Of course, one could opt for full anonymity! In which case, keep these properties as separated as possible.
But if you’d like these properties to be linked and, by doing so, establish an online identity, you’ll need a clever solution.

Enter Keyoxide.

When you visit someone’s Keyoxide profile and see a green tick next to an account on some website,
it was proven beyond doubt that the same person who set up this profile also holds that account.

<b>App features</b>

* Simple and straightforward UI
* Onboarding
* Guided tour
* Fully open source
* Built with Flutter
* Supports fingerprint and email address as identifiers
* Supports fetching, identities via keyservers and Web Key Directory
* Search history
* Contacts feature
* ASP Profile management, create, edit, delete, import, export profiles, add claims directly within the app
* Hide sensitive user profiles and protect with authentication
* Disable animations and update check
* Add multiple PGP or ASP profiles
* Custom domain validation and support for profile upload and look-up
* Check and display server update availability
* Biometric, password, PIN or pattern authentication for profile management
* User defined settings and data is saved in encrypted form
* Deep linking with 'openpgp4fpr' and 'aspe' URL schemes
* Localization, current available languages: German, English, Dutch, French, Galician, Polish, Spanish, Turkish, Portuguese, Chinese, Japanese
* Material 3 design
* Adaptive themeable icons for Android
* Accessibility enhancements such as semantic labels, dyslexic font, choice of seed color.

<b>About Keyoxide</b>

* Identity => verification using bidirectional linking
* Secure => use of trusted cryptography
* Decentralized => user data sovereignty
* Privacy-friendly => data explicitly provided by identity holder


