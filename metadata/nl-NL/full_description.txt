Een modern, veilig en privacyvriendelijk platform om je gedecentraliseerde online identiteit vast te stellen

Keyoxide stelt je in staat om het "eigendom" van accounts op websites, domeinnamen, IM, enz. te bewijzen, ongeacht je gebruikersnaam.

Dat laatste deel is belangrijk: je zou bijvoorbeeld 'alice' op Lobste.rs kunnen zijn, maar '@alice24' op Twitter.
En als je website 'thatcoder.tld' is, hoe moeten mensen dan weten dat al dat online eigendom van jou is?

Natuurlijk kun je ervoor kiezen om volledig anoniem te blijven! In dat geval houd je deze eigenschappen zo gescheiden mogelijk.
Maar als je wilt dat deze eigenschappen gekoppeld worden en daardoor een online identiteit wilt vestigen, heb je een slimme oplossing nodig.

Maak kennis met Keyoxide.

Wanneer je iemands Keyoxide-profiel bezoekt en een groen vinkje naast een account op een website ziet,
is onomstotelijk bewezen dat dezelfde persoon die dit profiel heeft ingesteld ook dat account bezit.

<b>App-functies</b>

* Eenvoudige en duidelijke gebruikersinterface
* Onboarding
* Geleidelijke tour
* Volledig open source
* Gemaakt met Flutter
* Ondersteunt vingerafdruk en e-mailadres als identificatoren
* Ondersteunt het ophalen van identiteiten via sleutelservers en Web Key Directory
* Zoekgeschiedenis
* Contactenfunctie
* ASP-profielbeheer: profielen maken, bewerken, verwijderen, importeren, exporteren, claims direct binnen de app toevoegen
* Gevoelige gebruikersprofielen verbergen en beschermen met authenticatie
* Animaties en updatecontrole uitschakelen
* Meerdere PGP- of ASP-profielen toevoegen
* Validatie van aangepaste domeinen en ondersteuning voor profielupload en -opzoeking
* Controle en weergave van serverupdatebeschikbaarheid
* Biometrische authenticatie, wachtwoord, PIN of patroon voor profielbeheer
* Door de gebruiker gedefinieerde instellingen en gegevens worden versleuteld opgeslagen
* Deep linking met 'openpgp4fpr' en 'aspe' URL-schema's
* Lokalisatie, momenteel beschikbare talen: Duits, Engels, Nederlands, Frans, Galicisch, Pools, Spaans, Turks, Portugees, Chinees, Japans
* Material 3 ontwerp
* Adaptieve thematische pictogrammen voor Android
* Toegankelijkheidsverbeteringen zoals semantische labels, dyslexie-vriendelijke lettertype, keuze van zaadkleur.

<b>Over Keyoxide</b>

* Identiteit => verificatie door middel van bidirectionele koppeling
* Veilig => gebruik van vertrouwde cryptografie
* Gedecentraliseerd => gebruikersdatasoevereiniteit
* Privacyvriendelijk => gegevens expliciet verstrekt door de identiteitsbeheerder