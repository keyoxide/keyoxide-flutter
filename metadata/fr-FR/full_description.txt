Une plateforme moderne, sécurisée et respectueuse de la vie privée pour établir votre identité en ligne décentralisée

Keyoxide vous permet de prouver la "propriété" de comptes sur des sites web, des noms de domaine, des messageries instantanées, etc., quel que soit votre nom d'utilisateur.

Cette dernière partie est importante : vous pourriez, par exemple, être ‘alice’ sur Lobste.rs, mais ‘@alice24’ sur Twitter.
Et si votre site web est ‘thatcoder.tld’, comment les gens sont-ils censés savoir que toutes ces propriétés en ligne sont les vôtres ?

Bien sûr, on pourrait opter pour l'anonymat complet ! Dans ce cas, gardez ces propriétés aussi séparées que possible.
Mais si vous souhaitez que ces propriétés soient liées et, ce faisant, établir une identité en ligne, vous aurez besoin d'une solution intelligente.

Entrez Keyoxide.

Lorsque vous visitez le profil Keyoxide de quelqu'un et que vous voyez une coche verte à côté d'un compte sur un site web,
il a été prouvé sans aucun doute que la même personne qui a configuré ce profil détient également ce compte.

<b>Fonctionnalités de l'application</b>

* Interface utilisateur simple et directe
* Onboarding
* Visite guidée
* Entièrement open source
* Développé avec Flutter
* Prend en charge l'empreinte digitale et l'adresse e-mail comme identifiants
* Prend en charge la récupération des identités via les serveurs de clés et le Web Key Directory
* Historique de recherche
* Fonctionnalité de contacts
* Gestion des profils ASP : créer, éditer, supprimer, importer, exporter des profils, ajouter des revendications directement dans l'application
* Masquer les profils utilisateur sensibles et les protéger par une authentification
* Désactiver les animations et la vérification des mises à jour
* Ajouter plusieurs profils PGP ou ASP
* Validation de domaine personnalisé et prise en charge du téléchargement et de la recherche de profils
* Vérifier et afficher la disponibilité des mises à jour du serveur
* Authentification biométrique, par mot de passe, PIN ou schéma pour la gestion des profils
* Paramètres définis par l'utilisateur et données enregistrées sous forme cryptée
* Liens profonds avec les schémas d'URL 'openpgp4fpr' et 'aspe'
* Localisation, langues actuellement disponibles : allemand, anglais, néerlandais, français, galicien, polonais, espagnol, turc, portugais, chinois, japonais
* Design Material 3
* Icônes thématiques adaptatives pour Android
* Améliorations d'accessibilité telles que les étiquettes sémantiques, la police adaptée à la dyslexie, le choix de la couleur de la graine.

<b>À propos de Keyoxide</b>

* Identité => vérification par lien bidirectionnel
* Sécurisé => utilisation de cryptographie de confiance
* Décentralisé => souveraineté des données utilisateur
* Respectueux de la vie privée => données fournies explicitement par le titulaire de l'identité