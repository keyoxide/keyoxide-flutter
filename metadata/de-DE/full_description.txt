Eine moderne, sichere und datenschutzfreundliche Plattform zur Etablierung Ihrer dezentralen Online-Identität

Keyoxide ermöglicht es Ihnen, „Besitz“ von Konten auf Websites, Domainnamen, IM usw. nachzuweisen, unabhängig von Ihrem Benutzernamen.

Dieser letzte Teil ist wichtig: Sie könnten beispielsweise 'alice' auf Lobste.rs sein, aber '@alice24' auf Twitter.
Und wenn Ihre Website 'thatcoder.tld' ist, woher sollen die Leute wissen, dass all diese Online-Eigentümer Ihnen gehören?

Natürlich könnte man sich für vollständige Anonymität entscheiden! In diesem Fall halten Sie diese Eigenschaften so getrennt wie möglich.
Aber wenn Sie möchten, dass diese Eigenschaften verknüpft werden und dadurch eine Online-Identität schaffen, benötigen Sie eine clevere Lösung.

Hier kommt Keyoxide ins Spiel.

Wenn Sie das Keyoxide-Profil von jemandem besuchen und neben einem Konto auf einer Website ein grünes Häkchen sehen,
wurde zweifelsfrei bewiesen, dass die gleiche Person, die dieses Profil erstellt hat, auch dieses Konto besitzt.

<b>App-Funktionen</b>

* Einfache und unkomplizierte Benutzeroberfläche
* Onboarding
* Geführte Tour
* Vollständig Open Source
* Entwickelt mit Flutter
* Unterstützt Fingerabdruck und E-Mail-Adresse als Identifikatoren
* Unterstützt das Abrufen von Identitäten über Schlüsselserver und Web Key Directory
* Suchverlauf
* Kontaktfunktion
* ASP-Profilmanagement: Erstellen, Bearbeiten, Löschen, Importieren, Exportieren von Profilen, Hinzufügen von Ansprüchen direkt in der App
* Verbergen sensibler Benutzerprofile und Schutz durch Authentifizierung
* Deaktivieren von Animationen und Update-Check
* Hinzufügen mehrerer PGP- oder ASP-Profile
* Validierung benutzerdefinierter Domains und Unterstützung für Profil-Upload und -Suche
* Überprüfung und Anzeige der Server-Update-Verfügbarkeit
* Biometrische Authentifizierung, Passwort, PIN oder Muster für das Profilmanagement
* Benutzerspezifische Einstellungen und Daten werden verschlüsselt gespeichert
* Deep Linking mit den URL-Schemata 'openpgp4fpr' und 'aspe'
* Lokalisierung, derzeit verfügbare Sprachen: Deutsch, Englisch, Niederländisch, Französisch, Galicisch, Polnisch, Spanisch, Türkisch, Portugiesisch, Chinesisch, Japanisch
* Material 3 Design
* Adaptive, themenfähige Symbole für Android
* Barrierefreiheitsverbesserungen wie semantische Labels, dyslexie-freundliche Schriftart, Wahl der Farbgebung.

<b>Über Keyoxide</b>

* Identität => Verifizierung durch bidirektionale Verknüpfung
* Sicher => Verwendung vertrauenswürdiger Kryptographie
* Dezentral => Benutzerdaten-Souveränität
* Datenschutzfreundlich => Daten werden ausdrücklich vom Identitätsinhaber bereitgestellt