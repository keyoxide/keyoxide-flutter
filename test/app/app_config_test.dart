import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:keyoxide_flutter/app/app_config.dart';
import 'package:keyoxide_flutter/common/constants/keyoxide_enums.dart';

void main() {
  group('AppConfig Tests', () {
    test('AppConfig should be a singleton', () {
      final config1 = AppConfig();
      final config2 = AppConfig();
      expect(identical(config1, config2), isTrue);
    });

    test('setCurrentConfiguration sets correct configuration for development', () {
      final config = AppConfig();
      config.setCurrentConfiguration(Environment.development);
      expect(config.current, isA<DevConfiguration>());
      expect(config.isDev, isTrue);
    });

    test('setCurrentConfiguration sets correct configuration for production', () {
      final config = AppConfig();
      config.setCurrentConfiguration(Environment.production);
      expect(config.current, isA<ReleaseConfiguration>());
      expect(config.isDev, isFalse);
    });
  });

  group('AppEnvironmentBuild Tests', () {
    testWidgets('AppEnvironmentBuild.of returns correct instance', (tester) async {
      final testWidget = AppEnvironmentBuild(
        environment: Environment.development,
        child: Builder(
          builder: (context) {
            expect(AppEnvironmentBuild.of(context)?.environment, Environment.development);
            return Container();
          },
        ),
      );

      await tester.pumpWidget(MaterialApp(home: testWidget));
    });

    testWidgets('updateShouldNotify returns false', (tester) async {
      final testWidget = AppEnvironmentBuild(
        environment: Environment.development,
        child: Container(),
      );

      await tester.pumpWidget(MaterialApp(home: testWidget));
      expect(testWidget.updateShouldNotify(testWidget), isFalse);
    });
  });
}
