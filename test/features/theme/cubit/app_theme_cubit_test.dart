import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:keyoxide_flutter/features/theme/cubit/app_theme_cubit.dart';
import 'package:keyoxide_flutter/features/theme/repository/app_theme_repository.dart';
import 'package:mocktail/mocktail.dart';

class MockAppThemeRepository extends Mock implements AppThemeRepository {}

void main() {
  late MockAppThemeRepository mockAppThemeRepository;
  late AppThemeCubit appThemeCubit;

  setUp(() {
    mockAppThemeRepository = MockAppThemeRepository();

    // Setup default returns for the mock methods
    when(() => mockAppThemeRepository.getAppTheme()).thenReturn(ThemeMode.light);
    when(() => mockAppThemeRepository.getAppThemeColor()).thenReturn(Colors.blue);
    when(() => mockAppThemeRepository.getFont()).thenReturn("Roboto");

    appThemeCubit = AppThemeCubit(themeRepo: mockAppThemeRepository);
  });

  group('AppThemeCubit', () {
    blocTest<AppThemeCubit, AppThemeState>(
      'emits [LoadingAppTheme, CurrentAppTheme] when changeAppTheme is called',
      build: () => appThemeCubit,
      setUp: () {
        // Use specific ThemeMode value instead of any()
        when(() => mockAppThemeRepository.setAppTheme(ThemeMode.dark)).thenAnswer((_) async {});
      },
      act: (cubit) => cubit.changeAppTheme(ThemeMode.dark),
      expect: () => [LoadingAppTheme(), CurrentAppTheme(themeMode: ThemeMode.dark)],
    );

    blocTest<AppThemeCubit, AppThemeState>(
      'emits [LoadingAppTheme, CurrentAppColor] when changeSeedColor is called',
      build: () => appThemeCubit,
      setUp: () {
        // Use specific Color value instead of any()
        when(() => mockAppThemeRepository.setAppThemeColor(Colors.green)).thenAnswer((_) async {});
      },
      act: (cubit) => cubit.changeSeedColor(Colors.green),
      expect: () => [LoadingAppTheme(), CurrentAppColor(seedColor: Colors.green)],
    );

    blocTest<AppThemeCubit, AppThemeState>(
      'emits [LoadingAppTheme, CurrentAppFont] when changeFont is called',
      build: () => appThemeCubit,
      setUp: () {
        // Use specific String value for font instead of any()
        when(() => mockAppThemeRepository.setFont("OpenSans")).thenAnswer((_) async {});
      },
      act: (cubit) => cubit.changeFont("OpenSans"),
      expect: () => [LoadingAppTheme(), CurrentAppFont(font: "OpenSans")],
    );
  });

  tearDown(() {
    appThemeCubit.close();
  });
}
