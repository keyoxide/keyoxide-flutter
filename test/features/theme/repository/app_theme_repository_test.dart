import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:keyoxide_flutter/common/constants/keyoxide_constants.dart';
import 'package:keyoxide_flutter/features/settings/model/user_settings_model.dart';
import 'package:keyoxide_flutter/features/theme/repository/app_theme_repository.dart';
import 'package:mocktail/mocktail.dart';
import 'package:keyoxide_flutter/features/settings/cubit/settings_cubit.dart';
import 'package:keyoxide_flutter/features/user/cubit/user_cubit.dart';
import 'package:keyoxide_flutter/common/services/locator_service.dart';
import 'package:keyoxide_flutter/features/user/model/keyoxide_user.dart';

class MockSettingsCubit extends Mock implements SettingsCubit {}
class MockUserCubit extends Mock implements UserCubit {}
class MockKeyoxideUser extends Mock implements KeyoxideUser {}
class MockUserSettings extends Mock implements UserSettingsModel {}

void main() {
  late MockSettingsCubit mockSettingsCubit;
  late MockUserCubit mockUserCubit;
  late MockKeyoxideUser mockKeyoxideUser;
  late MockUserSettings mockUserSettings;
  late AppThemeRepository appThemeRepository;

  setUp(() {
    mockSettingsCubit = MockSettingsCubit();
    mockUserCubit = MockUserCubit();
    mockKeyoxideUser = MockKeyoxideUser();
    mockUserSettings = MockUserSettings();
    getIt.registerSingleton<SettingsCubit>(mockSettingsCubit);
    getIt.registerSingleton<UserCubit>(mockUserCubit);
    appThemeRepository = AppThemeRepository();

    when(() => mockUserCubit.keyoxideUser).thenReturn(mockKeyoxideUser);
    when(() => mockKeyoxideUser.userSettings).thenReturn(mockUserSettings);
  });

  tearDown(() {
    getIt.unregister<SettingsCubit>();
    getIt.unregister<UserCubit>();
  });

  group('AppThemeRepository', () {
    test('getAppTheme returns ThemeMode.dark when user setting is dark mode', () {
      when(() => mockUserSettings.themeMode).thenReturn(darkModeKey);

      final themeMode = appThemeRepository.getAppTheme();

      expect(themeMode, ThemeMode.dark);
    });

    test('getAppTheme returns ThemeMode.light when user setting is light mode', () {
      when(() => mockUserSettings.themeMode).thenReturn(lightModeKey);

      final themeMode = appThemeRepository.getAppTheme();

      expect(themeMode, ThemeMode.light);
    });

    test('getAppTheme returns ThemeMode.system when user setting is system mode', () {
      when(() => mockUserSettings.themeMode).thenReturn(systemModeKey);

      final themeMode = appThemeRepository.getAppTheme();

      expect(themeMode, ThemeMode.system);
    });

    test('setAppTheme updates theme mode to dark', () async {
      when(() => mockSettingsCubit.changeThemeMode(any())).thenAnswer((_) async {});

      await appThemeRepository.setAppTheme(ThemeMode.dark);

      verify(() => mockSettingsCubit.changeThemeMode(darkModeKey)).called(1);
    });

    test('getAppThemeColor returns the correct color', () {
      when(() => mockUserSettings.seedColor).thenReturn('#FFFFFF');

      final color = appThemeRepository.getAppThemeColor();

      expect(color, Colors.white);
    });

    test('setAppThemeColor updates theme color correctly', () async {
      when(() => mockSettingsCubit.changeSeedColor(any())).thenAnswer((_) async {});

      await appThemeRepository.setAppThemeColor(Colors.blue);

      final blueHex = colorToHex(Colors.blue); // Assuming colorToHex is a function that converts Color to a hex string
      verify(() => mockSettingsCubit.changeSeedColor(blueHex)).called(1);
    });

    test('getFont returns the correct font', () {
      when(() => mockUserSettings.font).thenReturn('Roboto');

      final font = appThemeRepository.getFont();

      expect(font, 'Roboto');
    });

    test('setFont updates font correctly', () async {
      when(() => mockSettingsCubit.changeFont(any())).thenAnswer((_) async {});

      await appThemeRepository.setFont('OpenSans');

      verify(() => mockSettingsCubit.changeFont('OpenSans')).called(1);
    });
  });
}
