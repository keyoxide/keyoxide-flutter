import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:keyoxide_flutter/features/authentication/cubit/local_authentication_cubit.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  const MethodChannel channel = MethodChannel('plugins.flutter.io/local_auth');

  group('LocalAuthenticationCubit', () {
    late LocalAuthenticationCubit localAuthenticationCubit;

    setUp(() {
      localAuthenticationCubit = LocalAuthenticationCubit();

      // Mock the platform channel
      channel.setMockMethodCallHandler((MethodCall methodCall) async {
        if (methodCall.method == 'authenticate') {
          return true; // Simulate successful authentication
        }
        return null;
      });
    });

    test('initial state is LocalAuthenticationInitial', () {
      expect(localAuthenticationCubit.state, equals(LocalAuthenticationInitial()));
    });

    blocTest<LocalAuthenticationCubit, LocalAuthenticationState>(
      'emits [LocalAuthenticationLoading, LocalAuthenticationSuccess] when authentication succeeds',
      build: () => localAuthenticationCubit,
      act: (cubit) async {
        await cubit.authenticate(LocalAuthFor.authentication);
      },
      expect: () => [LocalAuthenticationLoading(), LocalAuthenticationSuccess()],
    );

    tearDown(() {
      localAuthenticationCubit.close();
      channel.setMockMethodCallHandler(null); // Remove the mock handler
    });
  });
}
