import 'package:flutter_test/flutter_test.dart';
import 'package:keyoxide_flutter/common/api/crud/dio_crud_api.dart';
import 'package:mocktail/mocktail.dart';
import 'package:dio/dio.dart';

class MockDio extends Mock implements Dio {}

void main() {
  late DioCrudApi dioCrudApi;
  late MockDio mockDio;

  setUp(() {
    mockDio = MockDio();
    dioCrudApi = DioCrudApi();
    dioCrudApi.dio = mockDio; // Inject the mock Dio instance
  });

  group('DioCrudApi Tests', () {
    test('GET request returns a successful response', () async {
      // Arrange
      final mockResponse = Response(
        requestOptions: RequestOptions(path: 'https://example.com'),
        data: 'Test Data',
        statusCode: 200,
      );
      when(() => mockDio.get(
        any(),
        queryParameters: any(named: 'queryParameters'),
        options: any(named: 'options'),
        cancelToken: any(named: 'cancelToken'),
        onReceiveProgress: any(named: 'onReceiveProgress'),
      )).thenAnswer((_) async => mockResponse);

      // Act
      final response = await dioCrudApi.get('https://example.com');

      // Assert
      expect(response, mockResponse);
      verify(() => mockDio.get('https://example.com')).called(1);
    });

    test('GET request throws an exception', () async {
      // Arrange
      when(() => mockDio.get(
        any(),
        queryParameters: any(named: 'queryParameters'),
        options: any(named: 'options'),
        cancelToken: any(named: 'cancelToken'),
        onReceiveProgress: any(named: 'onReceiveProgress'),
      )).thenThrow(DioError(
          requestOptions: RequestOptions(path: 'https://example.com')));

      // Act & Assert
      expect(
              () => dioCrudApi.get('https://example.com'), throwsA(isA<DioError>()));
    });
  });
}
