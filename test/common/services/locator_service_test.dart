import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:keyoxide_flutter/common/api/crud/dio_crud_api.dart';
import 'package:keyoxide_flutter/common/services/locator_service.dart';
import 'package:keyoxide_flutter/common/utils/debouncer.dart';
import 'package:keyoxide_flutter/router/app_router.dart';

void main() {
  setUpAll(() {
    // Setup GetIt before tests
    setupLocatorService();
  });

  tearDownAll(() {
    // Reset GetIt after each test
    GetIt.I.reset();
  });

  // Testing just a few of the services.

  test('AppRouter should be registered', () {
    expect(GetIt.I.isRegistered<AppRouter>(), isTrue);
    var instance = GetIt.I.get<AppRouter>();
    expect(instance, isNotNull);
    expect(instance, isA<AppRouter>());
  });

  test('DioCrudApi should be registered', () {
    expect(GetIt.I.isRegistered<DioCrudApi>(), isTrue);
    var instance = GetIt.I.get<DioCrudApi>();
    expect(instance, isNotNull);
    expect(instance, isA<DioCrudApi>());
  });

  test('Singleton instances should be the same', () {
    var instance1 = GetIt.I.get<AppRouter>();
    var instance2 = GetIt.I.get<AppRouter>();
    expect(identical(instance1, instance2), isTrue);
  });

  test('Unregistered service should throw exception', () {
    expect(() => GetIt.I.get<Debouncer>(), throwsA(isA<StateError>()));
  });
}
