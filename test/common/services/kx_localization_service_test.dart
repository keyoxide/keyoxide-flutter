import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:keyoxide_flutter/common/services/kx_localization_service.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  group('KxLocalizationService Tests', () {
    test('Localization load test', () async {
      const String languageCode = 'en';
      final Map<String, String> mockLocalizedStrings = {'hello': 'Hello'};

      // Mocking the rootBundle response
      final String jsonString = json.encode(mockLocalizedStrings);
      TestDefaultBinaryMessengerBinding.instance!.defaultBinaryMessenger.setMockMessageHandler('flutter/assets', (message) async {
        final buffer = utf8.encoder.convert(jsonString);
        return Future.value(ByteData.view(buffer.buffer));
      });

      KxLocalizationService service = KxLocalizationService();
      service.locale = Locale(languageCode);
      await service.load();

      expect(service.get('hello'), equals('Hello'));

      // Resetting the mock
      TestDefaultBinaryMessengerBinding.instance!.defaultBinaryMessenger.setMockMessageHandler('flutter/assets', null);
    });

  });
}
