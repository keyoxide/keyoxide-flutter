import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:keyoxide_flutter/common/services/kx_storage_service.dart';
import 'package:mocktail/mocktail.dart';

class MockFlutterSecureStorage extends Mock implements FlutterSecureStorage {}

void main() {
  late MockFlutterSecureStorage mockStorage;
  late KxStorageService kxStorageService;

  setUp(() {
    mockStorage = MockFlutterSecureStorage();
    kxStorageService = KxStorageService(storage: mockStorage);
  });

  test('should retrieve an existing encryption key from storage', () async {
    // Arrange
    const testKey = 'testEncryptionKey';
    when(() => mockStorage.read(key: any(named: 'key')))
        .thenAnswer((_) async => testKey);

    // Act
    final result = await kxStorageService.getStorageEncryptionKey();

    // Assert
    expect(result, equals(testKey));
    verify(() => mockStorage.read(key: any(named: 'key'))).called(1);
  });

  test('should generate a new encryption key if none exists and store it',
      () async {
    // Arrange
    when(() => mockStorage.read(key: any(named: 'key')))
        .thenAnswer((_) async => null);
    when(() => mockStorage.write(
        key: any(named: 'key'),
        value: any(named: 'value'))).thenAnswer((_) async {});

    // Act
    final result = await kxStorageService.getStorageEncryptionKey();

    // Assert
    expect(result.length, 32);
    verify(() => mockStorage.read(key: any(named: 'key'))).called(1);
    verify(() => mockStorage.write(
        key: any(named: 'key'), value: any(named: 'value'))).called(1);
  });
}
