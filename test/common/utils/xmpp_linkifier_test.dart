import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:keyoxide_flutter/common/utils/irc_linkifier.dart';
import 'package:keyoxide_flutter/common/utils/xmpp_linkifier.dart';

void main() {
  group('XmppLinkifier', () {
    final xmppLinkifier = XmppLinkifier();

    test('parses no URLs from a plain text', () {
      final result = xmppLinkifier.parse([TextElement('Just some text.')], LinkifyOptions());
      expect(result.length, 1);
      expect(result.first, isA<TextElement>());
      expect((result.first as TextElement).text, 'Just some text.');
    });

    test('parses and formats an XMPP URL', () {
      final result = xmppLinkifier.parse([TextElement('Contact me on xmpp:user@example.com')], LinkifyOptions());
      expect(result.length, 2);
      expect(result[1], isA<KeyoxideUrlElement>());
      expect((result[1] as KeyoxideUrlElement).url, 'xmpp:user@example.com');
    });

    test('applies humanize option to XMPP URLs', () {
      final options = LinkifyOptions(humanize: true);
      final result = xmppLinkifier.parse([TextElement('My XMPP is xmpp:user@example.com')], options);
      expect(result.any((element) => element is KeyoxideUrlElement && element.url == 'xmpp:user@example.com'), isTrue);
    });

    test('removes trailing period if excludeLastPeriod is true', () {
      final options = LinkifyOptions(excludeLastPeriod: true);
      final result = xmppLinkifier.parse([TextElement('Find me at xmpp:user@example.com.')], options);
      expect((result.whereType<KeyoxideUrlElement>().first).url, 'xmpp:user@example.com');
    });
  });
}
