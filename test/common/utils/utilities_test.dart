import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';
import 'package:keyoxide_flutter/common/utils/utilities.dart';
import 'package:keyoxide_flutter/features/generate_profile/model/keyoxide_profile.dart';
import 'package:keyoxide_flutter/features/user/model/pgp_profile_model.dart';
import 'package:mocktail/mocktail.dart';
import 'package:intl/intl.dart';
import 'dart:typed_data';

class MockBuildContext extends Mock implements BuildContext {}
class MockNetworkAssetBundle extends Mock implements NetworkAssetBundle {}

void main() {
  group('Utilities Tests', ()
  {
    test('Mock Dio HTTP request', () async {
      final dio = Dio(); // Your Dio instance
      final dioAdapter = DioAdapter(dio: dio); // Mock adapter
      dio.httpClientAdapter = dioAdapter;

      const imageUrl = 'https://example.com/image.png';
      final mockData = Uint8List.fromList([0, 1, 2, 3]);

      dioAdapter.onGet(
        imageUrl,
            (request) => request.reply(200, mockData),
        data: mockData,
      );
    });

      testWidgets('formatDateTimeToLocal formats correctly', (
          WidgetTester tester) async {
        final dateTime = DateTime(2022, 1, 1, 12, 0);
        String formattedDate = '';
        await tester.pumpWidget(MaterialApp(
          home: Builder(builder: (context) {
            formattedDate = Utilities.formatDateTimeToLocal(context, dateTime);
            return Container();
          }),
          localizationsDelegates: const [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: const [Locale('en', 'US')],
        ));
        final expectedFormat = DateFormat.yMd('en_US').add_jm().format(
            dateTime);
        expect(formattedDate, expectedFormat);
      });

      test('generateRandomString generates string of correct length', () {
        final len = 10;
        final result = Utilities.generateRandomString(len);
        expect(result.length, len);
      });
    });
  }
