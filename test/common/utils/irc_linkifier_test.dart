import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:keyoxide_flutter/common/utils/irc_linkifier.dart';

void main() {
  group('IrcLinkifier', () {
    final ircLinkifier = IrcLinkifier();

    test('parses no URLs from a plain text', () {
      final result = ircLinkifier.parse([TextElement('Just some text.')], LinkifyOptions());
      expect(result.length, 1);
      expect(result.first, isA<TextElement>());
      expect((result.first as TextElement).text, 'Just some text.');
    });

    test('parses and formats a strict URL', () {
      final result = ircLinkifier.parse([TextElement('Check this site: www.example.com')], LinkifyOptions());
      expect(result.length, 2);
      expect(result[1], isA<KeyoxideUrlElement>());
      expect((result[1] as KeyoxideUrlElement).url, 'www.example.com');
    });

    test('parses and formats a URL with looseUrl option enabled', () {
      final options = LinkifyOptions(looseUrl: true);
      final result = ircLinkifier.parse([TextElement('Visit example.com now')], options);
      expect(result.any((element) => element is KeyoxideUrlElement), isTrue);
    });

    test('removes trailing period if excludeLastPeriod is true', () {
      final options = LinkifyOptions(excludeLastPeriod: true);
      final result = ircLinkifier.parse([TextElement('Find it at www.example.com.')], options);
      expect((result.whereType<KeyoxideUrlElement>().first).url, 'www.example.com');
    });
  });
}
