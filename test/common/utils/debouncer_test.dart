import 'dart:async';

import 'package:flutter_test/flutter_test.dart';
import 'package:keyoxide_flutter/common/utils/debouncer.dart';

void main() {
  group('Debouncer', () {
    test('should execute action only once within the debounce duration', () async {
      // Arrange
      int counter = 0;
      final debouncer = Debouncer(milliseconds: 100); // 100ms debounce time
      final action = () => counter++;

      // Act
      debouncer.run(action);
      debouncer.run(action);
      debouncer.run(action);
      // Wait for the debounce time to pass
      await Future.delayed(Duration(milliseconds: 101));

      // Assert
      expect(counter, 1); // Action should only be executed once
    });

    test('should execute the last action if multiple actions are triggered within the debounce duration', () async {
      // Arrange
      int counter = 0;
      final debouncer = Debouncer(milliseconds: 100); // 100ms debounce time
      final firstAction = () => counter = 1;
      final lastAction = () => counter = 2;

      // Act
      debouncer.run(firstAction);
      // Wait for a bit but less than the debounce time
      await Future.delayed(Duration(milliseconds: 50));
      debouncer.run(lastAction);
      // Wait for the debounce time to pass
      await Future.delayed(Duration(milliseconds: 101));

      // Assert
      expect(counter, 2); // Only the last action should be executed
    });
  });
}
