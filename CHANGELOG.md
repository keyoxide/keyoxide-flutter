# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.4.2] - 2024-08-12
### Fixed
- Some strings not updating on lang change
- User profile not going into edit mode after auth
### Changed
- Updated packages

## [2.4.1] - 2024-08-09
### Fixed
- Crash caused by choosing Japanese Language
- App theme switch
### Changed
- Updated packages
- Updated flutter to 3.24

## [2.4.0] - 2024-07-18
### Added
- Offline-mode
- Swipe right to refresh on contact tile
- Refresh all contacts at once from menu
### Fixed
- Unresponsive on language change when offline
### Changed
- Update packages
- Update flutter to 3.22.2

## [2.3.1] - 2024-06-08
### Changed
- Update packages

## [2.3.0] - 2024-06-02
### Added
- Long-press copy links to clipboard
- Disable animations in settings
- Disable update check in settings
- Save and manage search history
- Adaptive themeable monochrome icons for Android

### Changed
- Snack-bar position to top
- Updated packages
- Updated flutter to 3.22.1

### Fixed
- Bug and UI fixes
- Breaking changes through updated packages

## [2.2.1] - 2024-04-30
### Fixed
- User view hidden profiles container

## [2.2.0] - 2024-04-29
### Added
- Selectable language in Settings
- Hide user profiles and protect with authentication

### Changed
- Updated translations
- Updated packages
- Updated flutter to 3.19.6

### Fixed
- Bug and UI fixes
- Upstream fixes
- Data migration issue

## [2.1.1] - 2024-03-06
### Changed
- Removed donation section from iOS build
- Changed donation link to Open Collective on Android

## [2.1.0] - 2024-03-05
### Added
- Contacts feature
  - Import & export contacts
- Guided tour

### Changed
- Updated translations
- Updated packages
- Updated flutter to 3.19.2

### Fixed
- Bug and UI fixes
- Upstream fixes

## [2.0.3] - 2024-02-14
### Added
- Japanese Language

### Changed
- Updated translations

### Fixed
- Bug and UI fixes
- Upstream fixes

## [2.0.2] - 2024-02-08
### Added
- Updated translations

### Fixed
- Illegal state exception on Android 8 and below

## [2.0.1] - 2024-02-06
### Added
- Updated translations

### Fixed
- Upstream fixes for claims

## [2.0.0] - 2024-01-01
### Added
- ASP Profile management
    - create, edit, delete, import, export profiles
    - manage claims
- Save multiple PGP & ASP profiles
- Custom domain and validation support for profile uploads and look-ups
- Check and display server update availability
- Biometric, password, PIN or pattern authentication for profile management
- User defined settings and data encryption
- Translations in Portuguese, Chinese
- Onboarding

### Fixed
- Numerous bugs and UI inconsistencies

### Changed
- New localization service with in-app translation strings
- User view, Display Profile and Settings UI
- Updated packages
- Updated flutter to 3.16.9

## [1.6.0] - 2023-10-05
### Added
- ASP profile support
- ASP deep linking support
- ASP qr-code support

### Fixed
- A few bugs and UI inconsistencies

### Changed
- Design overhaul mirroring web client
- Service provider icons in claim tiles
- Loading flow with claim count
- Upgrade packages
- Updated flutter to 3.13.6

## [1.5.1] - 2023-07-06
### Fixed
- Workaround for #87 (white box on the bottom of profile screen)

## [1.5.0] - 2023-07-01
### Added
- Unit & Bloc tests
- Accessibility enhancements
    - Dyslexic font (Toggle in settings)
    - Semantic labels
    - Change seed color (Toggle in settings)
- Open collective link in donate section
### Changed
- Updated to Material 3 design
- Upgrade packages
- Updated flutter to 3.10.5
### Fixed
- Bug and UI fixes here and there

## [1.4.3] - 2023-06-02
### Changed
- Updated packages
### Security
- Removed SSL fingerprint check which rendered app unusable after certificate update

## [1.4.2] - 2023-05-22
### Changed
- Weblate token to environmental variables
- Updated packages
- Updated flutter to 3.10.1
### Fixed
- Breaking changes caused by package updates
### Security
- Hardening for ROS security audit findings:
  - incomplete Cleanup of User Profile,
  - improper Certificate Validation,
  - improper Input Validation of User Query,
  - improper Input Validation of Server Response.

## [1.4.1] - 2023-04-18
### Changed
- Updated packages
- Updated flutter to 3.7.11
### Added
- Localization
  Current languages available: English, German, French,
  Spanish, Dutch, Polish, Turkish, Galician
- Deep linking with openpgp4fpr URL scheme

## [1.3.3] - 2023-03-17
### Changed
- Updated packages
- Code clean-up & re-factoring
- Primary email and claims will be shown first
### Removed
- Unused imports
### Fixed
- Replaced all deprecated methods

## [1.3.2] - 2023-03-09
### Fixed
- Open user profile not working for WKD only profiles
### Removed
- No proxy warning on claim details

## [1.3.1] - 2023-03-04
### Fixed
- HKP and WKD profile share links
### Removed
- Fingerprint protocol
### Added
- Android Google Play and iOS App Store releases
- Development and Production build flavors
- Privacy policy
- Back button to qr-scanner
### Changed
- Qr-scanner button texts to icons
- Screenshots on store pages

## [1.3.0] - 2023-02-20
### Changed
- Updated packages
- Share link is built with primary email instead of fingerprint
### Added
- Flutter as submodule
### Fixed
- Replace deprecated text styles
- Breaking changes caused by package updates
- Fixes for iOS devices

## [1.2.2] - 2022-11-05
### Changed
- Updated packages
- Qr scanner package
### Fixed
- User profile view scroll issue
- Let's Encrypt certificate issue on Android 5 and below

## [1.2.0] - 2022-10-10
### Added
- Profile view animations
- User profile view for quick access
- Speed dial button
- Share fingerprint & profile link
- Save user profile
- Settings view
- Bottom navigation bar
- CHANGELOG.md
### Changed
- Complete re-write & re-structure
- Updated packages
- Updated flutter version to 3
### Fixed
- Overall design & performance improvements

## [1.1.0]
### Added
- Built-in QR code scanner
- CHANGELOG.md
- New screenshots to reflect the changes
### Changed
- Transparent app bar
- Extended profile screen height
### Fixed
- Paddings on homepage

## [1.0.4] - 2021-11-14
### Changed
- Migration to Navigator 2.0
- F-Droid metadata
### Fixed
- API hostname

## [1.0.1] - 2021-11-10
### Added
- F-Droid store availability

## [1.0.0] - 2021-11-09
Initial release