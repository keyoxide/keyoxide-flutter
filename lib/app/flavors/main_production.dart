import 'dart:convert';
import 'dart:io';

import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';

import '../../common/api/license/license_api.dart';
import '../../common/constants/keyoxide_enums.dart';
import '../../common/services/kx_storage_service.dart';
import '../../common/services/locator_service.dart';
import '../../features/settings/cubit/settings_cubit.dart';
import '../../features/theme/cubit/app_theme_cubit.dart';
import '../app_config.dart';
import '../keyoxide_app.dart';

main() async {
  AppConfig().setCurrentConfiguration(Environment.production);
  WidgetsFlutterBinding.ensureInitialized();
  await setupLocatorService();
  String storageEncryptionKey =
      await KxStorageService().getStorageEncryptionKey();
  final bytesKey = sha256.convert(utf8.encode(storageEncryptionKey)).bytes;
  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: await getApplicationDocumentsDirectory(),
    encryptionCipher: HydratedAesCipher(bytesKey),
  );
  // Adds keyoxides license to the licenses
  LicenseApi().initMyLibrary();
  getIt<AppThemeCubit>().getAppTheme();
  getIt<SettingsCubit>().checkServerVersion();
  ByteData data =
      await PlatformAssetBundle().load('assets/ca/lets-encrypt-r3.pem');
  SecurityContext.defaultContext
      .setTrustedCertificatesBytes(data.buffer.asUint8List());
  var configuredApp = AppEnvironmentBuild(
    environment: Environment.production,
    child: const KeyoxideApp(),
  );
  runApp(configuredApp);
}
