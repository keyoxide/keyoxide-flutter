import 'package:flutter/material.dart';
import '../common/constants/keyoxide_enums.dart';

/// App Environment Build
class AppEnvironmentBuild extends InheritedWidget {
  AppEnvironmentBuild({super.key,
    required this.environment,
    required Widget child,
  }) : super(child: child);

  final Environment environment;

  static AppEnvironmentBuild? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType(
        aspect: AppEnvironmentBuild);
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}

// CONFIG
class AppConfig {
  static final AppConfig _singleton = AppConfig._internal();

  AppConfig._internal();

  factory AppConfig() {
    return _singleton;
  }

  late AppConfiguration current;

  bool get isDev => current.environment == Environment.development;

  void setCurrentConfiguration(Environment appEnvironment) {
    switch (appEnvironment) {
      case Environment.development:
        current = DevConfiguration();
        break;

      case Environment.production:
        current = ReleaseConfiguration();
        break;
    }
  }
}

abstract class AppConfiguration {
  String? name;

  //String? apiBaseUrl;
  //String? sha256Fingerprint;
  String packageName;
  Environment? environment;

  AppConfiguration({
    this.name,
    //this.apiBaseUrl,
    //this.sha256Fingerprint,
    this.packageName = "org.keyoxide.keyoxide",
    this.environment,
  });
}

/// DEV CONFIGURATION
class DevConfiguration extends AppConfiguration {
  @override
  String? name = "[DEV]Keyoxide";

  // @override
  // String? apiBaseUrl = APIConstants().developmentBaseUrl;

  //String? sha256Fingerprint = "05:20:32:6C:BD:8D:BD:57:58:64:88:CA:87:EC:64:B2:D8:C3:0F:E4:7A:B4:7A:CF:34:F9:21:2A:BD:BE:59:A6";

  @override
  String packageName = "org.keyoxide.keyoxide.dev";

  @override
  Environment? environment = Environment.development;
}

/// RELEASE CONFIGURATION
class ReleaseConfiguration extends AppConfiguration {
  @override
  String? name = "Keyoxide";

  // @override
  // String? apiBaseUrl = APIConstants().releaseBaseUrl;

  //String? sha256Fingerprint = "A0:3E:EC:91:E9:28:3D:8C:F1:AA:7F:FF:2E:49:9C:EC:60:FB:B9:15:8D:19:F7:D1:37:61:D1:19:1D:7E:F3:5B";

  @override
  String packageName = "org.keyoxide.keyoxide";

  @override
  Environment? environment = Environment.production;
}
