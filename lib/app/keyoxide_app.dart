import 'dart:async';

import 'package:app_links/app_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:keyoxide_flutter/common/services/kx_localization_service.dart';
import 'package:keyoxide_flutter/features/settings/cubit/settings_cubit.dart';
import 'package:keyoxide_flutter/features/settings/widget/language_selection_widget.dart';
import 'package:keyoxide_flutter/features/theme/repository/app_theme_repository.dart';

import '../common/services/locator_service.dart';
import '../features/generate_profile/cubit/generate_profile_cubit.dart';
import '../features/theme/app_theme.dart';
import '../features/theme/cubit/app_theme_cubit.dart';
import '../features/user/cubit/user_cubit.dart';
import '../router/app_router.dart';

class KeyoxideApp extends StatefulWidget {
  const KeyoxideApp({super.key});

  @override
  KeyoxideAppState createState() => KeyoxideAppState();
}

class KeyoxideAppState extends State<KeyoxideApp> {
  ThemeMode currentTheme = ThemeMode.system;
  late Color seedColor;
  late AppLinks _appLinks;
  late String appFont;
  late Languages language;
  StreamSubscription<Uri>? _linkSubscription;

  @override
  void initState() {
    super.initState();
    initDeepLinks();
    seedColor = getIt<AppThemeRepository>().getAppThemeColor();
    appFont = getIt<AppThemeRepository>().getFont();
    String userLangString =
        getIt<UserCubit>().keyoxideUser.userSettings.language;
    language = userLangString == ''
        ? Languages.system
        : Languages.values.byName(userLangString);
  }

  @override
  void dispose() {
    _linkSubscription?.cancel();
    super.dispose();
  }

  Future<void> initDeepLinks() async {
    _appLinks = AppLinks();

    // Check initial link if app was in cold state (terminated)
    final appLink = await _appLinks.getInitialLink();
    if (appLink != null) {
      debugPrint('getInitialLink: $appLink');
      openAppLink(appLink);
    }

    // Handle link when app is in warm state (front or background)
    _linkSubscription = _appLinks.uriLinkStream.listen((uri) {
      debugPrint('onAppLink: $uri');
      openAppLink(uri);
    });
  }

  void openAppLink(Uri uri) {
    String query = uri.toString();
    getIt<GenerateProfileCubit>().getProfileClaimCount(
        query.startsWith('aspe') ? query : query.substring(12, query.length),
        GenerateProfileFor.displayProfileView,
        false);
  }

  @override
  Widget build(BuildContext context) {
    final router = getIt<AppRouter>().router;
    return BlocBuilder<SettingsCubit, SettingsState>(
        bloc: getIt<SettingsCubit>(),
        builder: (context, state) {
          if (state is ChangeLanguageSuccess) {
            language = state.selectedLanguage;
          }
          return BlocBuilder<AppThemeCubit, AppThemeState>(
            bloc: getIt<AppThemeCubit>(),
            builder: (context, state) {
              if (state is CurrentAppTheme) {
                currentTheme = state.themeMode;
              }
              if (state is CurrentAppColor) {
                seedColor = state.seedColor;
              }
              if (state is CurrentAppFont) {
                appFont = state.font;
              }
              return MaterialApp.router(
                key: ValueKey(language),
                showSemanticsDebugger: false,
                routeInformationParser: router.routeInformationParser,
                routerDelegate: router.routerDelegate,
                routeInformationProvider: router.routeInformationProvider,
                debugShowCheckedModeBanner: false,
                title: 'Keyoxide',
                theme: AppTheme.lightTheme(seedColor, appFont),
                darkTheme: AppTheme.darkTheme(seedColor, appFont),
                themeMode: currentTheme,
                locale: language.locale,
                supportedLocales: KxLocalizationService.supportedLocales,
                localizationsDelegates: const [
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                  KxLocalizationService.delegate,
                ],
              );
            },
          );
        });
  }
}
