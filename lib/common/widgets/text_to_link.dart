import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/common/widgets/keyoxide_snackbar.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:share_plus/share_plus.dart';

import '../utils/irc_linkifier.dart';
import '../utils/xmpp_linkifier.dart';

// The urls in the text becomes links that launches with webview.
class TextToLink extends StatelessWidget {
  const TextToLink({
    super.key,
    required this.urlText,
    required this.urlTextStyle,
    required this.textStyle,
  });

  final String urlText;
  final TextStyle urlTextStyle;
  final TextStyle textStyle;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: () {
        FlutterClipboard.copy(urlText);
        if (!context.mounted) return;
        KeyoxideSnackBar().showOverlaySnackBar(
            context, 'general_txt_clipboard_copy'.localizeWithPlaceholders([urlText])!);
      },
      child: Linkify(
        // Clicking on xmpp or irc links doesn't work..
        linkifiers: const [
          UrlLinkifier(),
          XmppLinkifier(),
          IrcLinkifier(),
        ],
        options: const LinkifyOptions(
          humanize: false,
        ),
        onOpen: (link) async {
          Uri url = Uri.parse(link.url);
          if (await canLaunchUrl(url)) {
            await launchUrl(
              mode: LaunchMode.externalApplication,
              url,
            );
          } else if (!link.url.startsWith("http")) {
            Share.share(link.url);
          } else {
            if (!context.mounted) return;
            KeyoxideSnackBar()
                .showOverlaySnackBar(context, 'Error launching $link');
          }
        },
        text: urlText,
        textAlign: TextAlign.start,
        style: textStyle,
        linkStyle: urlTextStyle,
      ),
    );
  }
}
