import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/features/tutorial/cubit/tutorial_cubit.dart';

import '../constants/keyoxide_constants.dart';
import '../services/locator_service.dart';

class AppBarWidget extends StatelessWidget implements PreferredSizeWidget {
  const AppBarWidget({super.key});

  @override
  Size get preferredSize => const Size.fromHeight(50);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      actions: [
        Semantics(
          excludeSemantics: true,
          label: 'appbar_scanner_btn_semantics'.localize,
          child: IconButton(
            key: getIt<TutorialCubit>().appBarQrScannerKey,
              onPressed: () {
                FocusManager.instance.primaryFocus?.unfocus();
                context.push(qrCodeScannerPath);
              },
              icon: const Icon(Icons.qr_code_scanner_outlined)),
        ),
        Semantics(
          excludeSemantics: true,
          label:'appbar_settings_btn_semantics'.localize,
          child: IconButton(
            key: getIt<TutorialCubit>().appBarSettingsKey,
              onPressed: () {
                FocusManager.instance.primaryFocus?.unfocus();
                context.push(settingsPath);
              },
              icon: const Icon(Icons.settings_outlined)),
        ),
      ],
    );
  }
}
