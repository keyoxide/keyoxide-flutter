import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/features/generate_profile/cubit/generate_profile_cubit.dart';

import '../services/locator_service.dart';

class ClaimCountLoadingOverlay extends StatefulWidget {
  const ClaimCountLoadingOverlay({super.key});

  @override
  State<ClaimCountLoadingOverlay> createState() =>
      _ClaimCountLoadingOverlayState();
}

class _ClaimCountLoadingOverlayState extends State<ClaimCountLoadingOverlay> {
  int _claimCount = 0;
  bool _showClaimText = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Center(
        child: Container(
            width: double.infinity,
            height: 200,
            decoration: BoxDecoration(
                color: Theme.of(context).brightness == Brightness.dark
                    ? Colors.black.withOpacity(0.9)
                    : Colors.white.withOpacity(0.9),
                borderRadius: const BorderRadius.all(Radius.circular(20))),
            child: BlocBuilder<GenerateProfileCubit, GenerateProfileState>(
              bloc: getIt<GenerateProfileCubit>(),
              builder: (context, state) {
                if (state is UserViewGetProfileClaimsSuccess) {
                  _claimCount = state.claimCount;
                  _showClaimText = true;
                } else if (state is GetProfileClaimsSuccess) {
                  _claimCount = state.claimCount;
                  _showClaimText = true;
                } else if (state is ContactsViewGetProfileClaimsSuccess) {
                  _claimCount = state.claimCount;
                  _showClaimText = true;
                }
                return Center(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const CircularProgressIndicator(),
                    const SizedBox(height: 10),
                    Text(
                      _showClaimText
                          ? 'view_page_loading_claim_count'
                              .localizeWithPlaceholders(
                                  [_claimCount.toString()])!
                          : 'view_page_loading_searching'.localize,
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: 10),
                    if (_showClaimText)
                      Text('view_page_loading_verifying'.localize),
                  ],
                ));
              },
            )),
      ),
    );
  }
}
