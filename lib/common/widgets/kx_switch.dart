import 'package:flutter/material.dart';

class KxSwitch extends StatefulWidget {
  KxSwitch(
      {super.key,
      required this.isEnabled,
      required this.text,
      required this.onChanged});

  bool isEnabled;
  String text;
  VoidCallback onChanged;

  @override
  State<KxSwitch> createState() => _KxSwitchState();
}

class _KxSwitchState extends State<KxSwitch> {
  final MaterialStateProperty<Icon?> thumbIcon =
      MaterialStateProperty.resolveWith<Icon?>(
    (Set<MaterialState> states) {
      if (states.contains(MaterialState.selected)) {
        return const Icon(Icons.check);
      }
      return const Icon(Icons.close);
    },
  );

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Text(widget.text),
      Switch(
        thumbIcon: thumbIcon,
        value: widget.isEnabled,
        onChanged: (bool value) async {
          setState(() {
            //widget.isEnabled = value;
            widget.onChanged();
          });
        },
      ),
    ]);
  }
}
