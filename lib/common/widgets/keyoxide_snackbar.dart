import 'package:flutter/material.dart';

class KeyoxideSnackBar {
  void show(BuildContext context, String text) {
    ScaffoldMessenger.of(context).showSnackBar(_snackBar(text));
  }

  SnackBar _snackBar(String text) {
    return SnackBar(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      behavior: SnackBarBehavior.floating,
      content: Text(text),
    );
  }

  void showOverlaySnackBar(BuildContext context, String message) {
    final animationController = AnimationController(
      duration: const Duration(milliseconds: 300),
      vsync: Navigator.of(context),
    );

    OverlayEntry overlayEntry = OverlayEntry(
      builder: (context) {
        return Positioned(
          top: 70.0, // Change this from `bottom` to `top`
          left: MediaQuery.of(context).size.width * 0.05,
          right: MediaQuery.of(context).size.width * 0.05,
          child: SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(0, -1), // Change the starting position
              end: Offset.zero,
            ).animate(
              CurvedAnimation(
                parent: animationController,
                curve: Curves.easeOut,
              ),
            ),
            child: Material(
              color: Colors.transparent,
              child: LayoutBuilder(
                builder: (context, constraints) {
                  return Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 14.0, horizontal: 24.0),
                    decoration: BoxDecoration(
                      color: Theme.of(context).canvasColor,
                      borderRadius: BorderRadius.circular(4.0),
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.black45,
                          blurRadius: 8.0,
                          offset: Offset(0, 2),
                        ),
                      ],
                    ),
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                        maxHeight: MediaQuery.of(context).size.height * 0.8,
                      ),
                      child: SingleChildScrollView(
                        child: Text(
                          message,
                          style: Theme.of(context)
                              .snackBarTheme
                              .contentTextStyle,
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        );
      },
    );

    animationController.forward();

    // Inserting the overlay entry into the Overlay
    Overlay.of(context).insert(overlayEntry);

    // Automatically remove the entry after 3 seconds to give it time to show up and stay a bit
    Future.delayed(const Duration(seconds: 3), () {
      overlayEntry.remove();
      animationController.dispose();
    });
  }
}