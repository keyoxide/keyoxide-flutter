import 'package:go_router/go_router.dart';
import 'package:keyoxide_flutter/common/services/kx_localization_service.dart';

extension GoRouterLocation on GoRouter {
  String get location {
    final RouteMatch lastMatch = routerDelegate.currentConfiguration.last;
    final RouteMatchList matchList = lastMatch is ImperativeRouteMatch
        ? lastMatch.matches
        : routerDelegate.currentConfiguration;
    return matchList.fullPath;
  }
}

extension LocalizeString on String? {
  String get localize =>
      KxLocalizationService().get(this, showKeyOfMissingString: true);

  String? localizeWithPlaceholders(List<String> parameters) =>
      KxLocalizationService().getInterpolatedString(this, parameters);
}
