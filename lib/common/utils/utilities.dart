import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:keyoxide_flutter/common/services/kx_localization_service.dart';
import 'package:keyoxide_flutter/features/settings/widget/language_selection_widget.dart';

import '../../features/generate_profile/model/keyoxide_profile.dart';
import '../../features/user/model/pgp_profile_model.dart';
import '../constants/keyoxide_enums.dart';

class Utilities {
  static Future<Uint8List> urlImageToUInt8List(String imageUrl) async {
    Uint8List bytes =
        (await NetworkAssetBundle(Uri.parse(imageUrl)).load(imageUrl))
            .buffer
            .asUint8List();
    return bytes;
  }

  static Future<ProfileModel> keyoxideProfileToProfileModel(KeyoxideProfile profile) async {
    int primaryPersonaIndex = profile.primaryPersonaIndex!.toInt();
    String avatar = profile.personas![profile.primaryPersonaIndex!].avatarUrl.toString();
    FingerprintProtocol protocol = FingerprintProtocol.fromString(
        profile.publicKey!.fetch!.method.toString());

    return ProfileModel(
      fingerprint: profile.publicKey!.fingerprint.toString(),
      identifier: profile.identifier.toString(),
      primaryEmail: profile.personas![primaryPersonaIndex].email.toString(),
      protocol: FingerprintProtocol.fromString(
          profile.publicKey!.fetch!.method.toString()),
      name: profile.personas![primaryPersonaIndex].name.toString(),
      image: await Utilities.urlImageToUInt8List(
          protocol == FingerprintProtocol.aspe
              ? avatar.replaceAll('svg', 'png')
              : avatar),
      isHidden: false,
      profile: profile,
      updatedAt: DateTime.now(),
    );
  }

  static String formatDateTimeToLocal(BuildContext context, DateTime dateTime) {
    var locale = Localizations.localeOf(context).toString();
    var dateFormat = DateFormat.yMd(locale).add_jm();
    return dateFormat.format(dateTime);
  }

  static String generateRandomString(int len) {
    final random = Random();
    final result = String.fromCharCodes(
        List.generate(len, (index) => random.nextInt(33) + 89));
    return result;
  }

  static int getClaimCountFromProfile(KeyoxideProfile profile) {
    int claimCount = 0;
    for (var persona in profile.personas!) {
      if (persona.isRevoked != null || persona.isRevoked == false) {
        claimCount += persona.claims!.length;
      }
    }
    return claimCount;
  }

  static Languages getLocale(String lang) {
    if (lang == '') {
      Locale systemLocale = getSystemLocale();
      if (KxLocalizationService.supportedLocales.contains(systemLocale))  {
        return Languages.system;
      } else {
        return Languages.english;
      }
    } else {
      return Languages.values.byName(lang);
    }
  }

  static Locale getSystemLocale() {
    final String systemLocale = Platform.localeName;
    return Locale(systemLocale);
  }
}


