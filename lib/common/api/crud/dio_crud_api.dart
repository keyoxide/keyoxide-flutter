import 'package:dio/dio.dart';
import '../../constants/keyoxide_constants.dart';

class DioCrudApi {
  DioCrudApi() {
    dio = Dio()
      ..options.connectTimeout =
          const Duration(milliseconds: APIConstants.connectionTimeout)
      ..options.receiveTimeout =
          const Duration(milliseconds: APIConstants.receiveTimeout)
      ..options.responseType = ResponseType.json;
      // ..interceptors.add(
      //   CertificatePinningInterceptor(
      //     allowedSHAFingerprints: [
      //       AppConfig().current.sha256Fingerprint.toString()
      //     ],
      //   ),
      // );
  }

// dio instance
  late Dio dio;

  // GET
  Future<Response> get(
    String url, {
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    ProgressCallback? onReceiveProgress,
  }) async {
    try {
      final response = await dio.get(
        url,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onReceiveProgress: onReceiveProgress,
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

// // POST
// Future<Response> post(
//     String url, {
//       dynamic data,
//       Map<String, dynamic>? queryParameters,
//       Options? options,
//       CancelToken? cancelToken,
//       ProgressCallback? onSendProgress,
//       ProgressCallback? onReceiveProgress,
//     }) async {
//   try {
//     final response = await dio.post(
//       url,
//       data: data,
//       queryParameters: queryParameters,
//       options: options,
//       cancelToken: cancelToken,
//       onSendProgress: onSendProgress,
//       onReceiveProgress: onReceiveProgress,
//     );
//     return response;
//   } catch (e) {
//     rethrow;
//   }
// }

// // PUT (UPDATE)
// Future<Response> put(
//     String url, {
//       dynamic data,
//       Map<String, dynamic>? queryParameters,
//       Options? options,
//       CancelToken? cancelToken,
//       ProgressCallback? onSendProgress,
//       ProgressCallback? onReceiveProgress,
//     }) async {
//   try {
//     final response = await dio.put(
//       url,
//       data: data,
//       queryParameters: queryParameters,
//       options: options,
//       cancelToken: cancelToken,
//       onSendProgress: onSendProgress,
//       onReceiveProgress: onReceiveProgress,
//     );
//     return response;
//   } catch (e) {
//     rethrow;
//   }
// }

// // DELETE
// Future<dynamic> delete(
//     String url, {
//       dynamic data,
//       Map<String, dynamic>? queryParameters,
//       Options? options,
//       CancelToken? cancelToken,
//       ProgressCallback? onSendProgress,
//       ProgressCallback? onReceiveProgress,
//     }) async {
//   try {
//     final response = await dio.delete(
//       url,
//       data: data,
//       queryParameters: queryParameters,
//       options: options,
//       cancelToken: cancelToken,
//     );
//     return response.data;
//   } catch (e) {
//     rethrow;
//   }
// }
}
