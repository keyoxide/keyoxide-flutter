class KeyoxideResult<T> {
  final T? data;
  final KeyoxideError? error;

  const KeyoxideResult({this.data, this.error})
      : assert(data != null && error == null || data == null && error != null);
}

class KeyoxideError {
  final String? error;
  final String? message;
  final int? code;

  const KeyoxideError({this.error, this.message, this.code});

  factory KeyoxideError.fromJson(Map<String, dynamic> json) {
    return KeyoxideError(
      error: json['error'] as String?,
      message: json['message'] as String?,
      code: json['code'] as int?,
    );
  }
}
