enum FingerprintProtocol {
  aspe(name: 'aspe'),
  hkp(name: 'hkp'),
  wkd(name: 'wkd'),
  http(name: 'http'),
  none(name: 'none');

  const FingerprintProtocol({required this.name});

  final String name;

  factory FingerprintProtocol.fromString(String protocol) {
    switch (protocol.toLowerCase()) {
      case 'aspe':
        return FingerprintProtocol.aspe;
      case 'hkp':
        return FingerprintProtocol.hkp;
      case 'wkd':
        return FingerprintProtocol.wkd;
      case 'http':
        return FingerprintProtocol.http;
      default:
        return FingerprintProtocol.none;
    }
  }
}

/// Build Environment Flavors
enum Environment {
  development,
  production,
}

enum ClaimStatus {
  notYetMatched(code: 100, message: 'claim_status_100'),
  matchedButNotYetVerified(code: 101, message: 'claim_status_101'),
  claimSuccessful(code: 200, message: 'claim_status_200'),
  claimSuccessfulThroughProxy(code: 201, message: 'claim_status_201'),
  unknownMatchingError(code: 300, message: 'claim_status_300'),
  matchingError(code: 301, message: 'claim_status_301'),
  unknownVerificationError(code: 400, message: 'claim_status_400'),
  verificationError(code: 401, message: 'claim_status_401');

  const ClaimStatus({required this.code, required this.message});

  final int code;
  final String message;

  factory ClaimStatus.fromString(int statusCode) {
    switch (statusCode) {
      case 100:
        return ClaimStatus.notYetMatched;
      case 101:
        return ClaimStatus.matchedButNotYetVerified;
      case 200:
        return ClaimStatus.claimSuccessful;
      case 201:
        return ClaimStatus.claimSuccessfulThroughProxy;
      case 300:
        return ClaimStatus.unknownMatchingError;
      case 301:
        return ClaimStatus.matchingError;
      case 400:
        return ClaimStatus.unknownVerificationError;
      case 401:
        return ClaimStatus.verificationError;

      default:
        return ClaimStatus.notYetMatched;
    }
  }
}

enum ServiceProviders {
  fallbackprovider(icon: 'assets/service-providers/_/icon.svg'),
  activitypub(icon: 'assets/service-providers/activitypub/icon.svg'),
  discourse(icon: 'assets/service-providers/discourse/icon.svg'),
  dns(icon: 'assets/service-providers/dns/icon.svg'),
  forem(icon: 'assets/service-providers/forem/icon.svg'),
  forgejo(icon: 'assets/service-providers/forgejo/icon.svg'),
  friendica(icon: 'assets/service-providers/friendica/icon.svg'),
  gitea(icon: 'assets/service-providers/gitea/icon.svg'),
  github(icon: 'assets/service-providers/github/icon.svg'),
  gitlab(icon: 'assets/service-providers/gitlab/icon.svg'),
  hackernews(icon: 'assets/service-providers/hackernews/icon.svg'),
  irc(icon: 'assets/service-providers/irc/icon.svg'),
  kbin(icon: 'assets/service-providers/kbin/icon.svg'),
  keybase(icon: 'assets/service-providers/keybase/icon.svg'),
  lemmy(icon: 'assets/service-providers/lemmy/icon.svg'),
  liberapay(icon: 'assets/service-providers/liberapay/icon.svg'),
  lichess(icon: 'assets/service-providers/lichess/icon.svg'),
  lobsters(icon: 'assets/service-providers/lobsters/icon.svg'),
  mastodon(icon: 'assets/service-providers/mastodon/icon.svg'),
  matrix(icon: 'assets/service-providers/matrix/icon.svg'),
  opencollective(icon: 'assets/service-providers/opencollective/icon.svg'),
  owncast(icon: 'assets/service-providers/owncast/icon.svg'),
  peertube(icon: 'assets/service-providers/peertube/icon.svg'),
  pixelfed(icon: 'assets/service-providers/pixelfed/icon.svg'),
  pleroma(icon: 'assets/service-providers/pleroma/icon.svg'),
  reddit(icon: 'assets/service-providers/reddit/icon.svg'),
  stackexchange(icon: 'assets/service-providers/stackexchange/icon.svg'),
  telegram(icon: 'assets/service-providers/telegram/icon.svg'),
  twitter(icon: 'assets/service-providers/twitter/icon.svg'),
  writefreely(icon: 'assets/service-providers/writefreely/icon.svg'),
  xmpp(icon: 'assets/service-providers/xmpp/icon.svg');

  const ServiceProviders({required this.icon});

  final String icon;

  factory ServiceProviders.fromName(String name) {
    switch (name) {
      case 'activitypub':
        return ServiceProviders.activitypub;
      case 'discourse':
        return ServiceProviders.discourse;
      case 'dns':
        return ServiceProviders.dns;
      case 'forem':
        return ServiceProviders.forem;
      case 'forgejo':
        return ServiceProviders.forgejo;
      case 'friendica':
        return ServiceProviders.friendica;
      case 'gitea':
        return ServiceProviders.gitea;
      case 'github':
        return ServiceProviders.github;
      case 'gitlab':
        return ServiceProviders.gitlab;
      case 'hackernews':
        return ServiceProviders.hackernews;
      case 'irc':
        return ServiceProviders.irc;
      case 'kbin':
        return ServiceProviders.kbin;
      case 'keybase':
        return ServiceProviders.keybase;
      case 'lemmy':
        return ServiceProviders.lemmy;
      case 'liberapay':
        return ServiceProviders.liberapay;
      case 'lichess':
        return ServiceProviders.lichess;
      case 'lobsters':
        return ServiceProviders.lobsters;
      case 'mastodon':
        return ServiceProviders.mastodon;
      case 'matrix':
        return ServiceProviders.matrix;
      case 'opencollective':
        return ServiceProviders.opencollective;
      case 'owncast':
        return ServiceProviders.owncast;
      case 'peertube':
        return ServiceProviders.peertube;
      case 'pixelfed':
        return ServiceProviders.pixelfed;
      case 'pleroma':
        return ServiceProviders.pleroma;
      case 'reddit':
        return ServiceProviders.reddit;
      case 'stackexchange':
        return ServiceProviders.stackexchange;
      case 'telegram':
        return ServiceProviders.telegram;
      case 'twitter':
        return ServiceProviders.twitter;
      case 'writefreely':
        return ServiceProviders.writefreely;
      case 'xmpp':
        return ServiceProviders.xmpp;
      default:
        return ServiceProviders.fallbackprovider;
    }
  }
}
