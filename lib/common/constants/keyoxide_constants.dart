import 'package:flutter/material.dart';

class APIConstants {
  // receiveTimeout
  static const int receiveTimeout = 100000;

  // connectTimeout
  static const int connectionTimeout = 100000;

  static const String developmentBaseUrl = 'dev.keyoxide.org';
  static const String releaseBaseUrl = 'keyoxide.org';
  static const String endPoint = '/api/3/profile';
  static const String serverValidationEndpoint = '/.well-known/keyoxide/version';
  static const String codebergApiBaseUrl = 'codeberg.org';
  static const String codebergApiUrlPrefix = '/api/v1/repos/keyoxide';
  static const String codebergApiEndpoint = '/releases/latest';
  static const String verificationServerProjectName = '/keyoxide-web';
  static const String aspeServerProjectName = '/aspe-server-rs';
}

/// ROUTE NAMES
const String rootRouteName = 'root';
const String homeRouteName = 'home';
const String generateProfileRouteName = 'generate';
const String contactsRouteName = 'contacts';
const String displayProfileRouteName = 'displayProfile';
const String userRouteName = 'user';
const String aboutRouteName = 'about';
const String qrCodeScannerRouteName = 'qrCodeScanner';
const String settingsRouteName = 'settings';
const String onboardingRouteName = 'onboarding';

/// PATHS
const String rootPath = '/';
const String homePath = '/home';
const String generateProfilePath = '/generate';
const String displayProfilePath = '/display_profile';
const String contactsPath = '/contacts';
const String userPath = '/user';
const String aboutPath = '/about';
const String qrCodeScannerPath = "/qr_code_scanner";
const String settingsPath = "/settings";
const String onboardingPath = "/onboarding";

/// KEYS
const String themeModeKey = 'appThemeMode';
const String themeColorKey = 'themeColor';
const String lightModeKey = 'lightMode';
const String darkModeKey = 'darkMode';
const String systemModeKey = 'systemMode';
const String fontKey = 'fontKey';
const String storageEncryptionKey = 'storageEncryptionKey';

/// FONTS
class FontConsts {
static String defaultFont = 'defaultFont';
static String openDyslexic3 = 'OpenDyslexic3';
}

/// COLORS
// These are the original colors used on the website. Reference: https://docs.keyoxide.org/other/design/
class ColorConsts {
  static const defaultSeedColor = Color(0xff673ab7);
  // Light theme
  static const headerTextLight = Color(0xFF6855c3);
  static const linkTextLight = Color(0xFF086191);
  static const htmlLinkText = Color(0xFF9ecaff);
  static const backgroundLight = Color(0xFFffffff);
  static const cardBackgroundLight = Color(0xFFf9f8fb);
  static const cardBorderLight = Color(0xFFddd9f2);
  static const claimCardBackgroundLight = Color(0xFFeeecf8);
  static const buttonBackgroundLight = Color(0xFF7868ca);
  static const verificationSuccessLight = Color(0xFF479438);
  static const verificationFailureLight = Color(0xFFd6705c);

  // Dark Theme
  static const headerTextDark = Color(0xFFcdc6eb);
  static const linkTextDark = Color(0xFF43b0ea);
  static const backgroundDark = Color(0xFF121212);
  static const cardBackgroundDark = Color(0xFF191720);
  static const cardBorderDark = Color(0xFF26203a);
  static const claimCardBackgroundDark = Color(0xFF26203a);
  static const buttonBackgroundDark = Color(0xFF7868ca);
  static const verificationSuccessDark = Color(0xFF7ac76b);
  static const verificationFailureDark = Color(0xFFd6705c);
}
