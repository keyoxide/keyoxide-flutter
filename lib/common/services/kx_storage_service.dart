import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:keyoxide_flutter/common/constants/keyoxide_constants.dart';

import '../utils/utilities.dart';

class KxStorageService {
  KxStorageService(
      {this.storage = const FlutterSecureStorage(
          aOptions: AndroidOptions(
        encryptedSharedPreferences: true,
      ))});

  final FlutterSecureStorage storage;

  Future<String> getStorageEncryptionKey() async {
    String? key = await storage.read(key: storageEncryptionKey);

    if (key == null) {
      key = Utilities.generateRandomString(32);
      await storage.write(key: storageEncryptionKey, value: key);
    }

    return key;
  }
}
