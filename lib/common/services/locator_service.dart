import 'package:get_it/get_it.dart';
import 'package:keyoxide_flutter/features/authentication/cubit/local_authentication_cubit.dart';
import 'package:keyoxide_flutter/features/contacts/cubit/contacts_cubit.dart';
import 'package:keyoxide_flutter/features/contacts/repository/contacts_repository.dart';
import 'package:keyoxide_flutter/features/create_edit_asp_profile/cubit/asp_cubit.dart';
import 'package:keyoxide_flutter/features/settings/cubit/settings_cubit.dart';
import 'package:keyoxide_flutter/features/settings/repository/settings_repository.dart';
import 'package:keyoxide_flutter/features/tutorial/cubit/tutorial_cubit.dart';

import '../../features/generate_profile/cubit/generate_profile_cubit.dart';
import '../../features/generate_profile/repository/keyoxide_repository.dart';
import '../../features/theme/cubit/app_theme_cubit.dart';
import '../../features/theme/repository/app_theme_repository.dart';
import '../../features/user/cubit/user_cubit.dart';
import '../../router/app_router.dart';
import '../api/crud/dio_crud_api.dart';

GetIt getIt = GetIt.instance;

Future<void> setupLocatorService() async {
  getIt
    ..registerLazySingleton<AppRouter>(() => AppRouter())
    ..registerLazySingleton<DioCrudApi>(
      () => DioCrudApi(
          //allowedShaFingerprintList: AppConfig().current.sha256Fingerprints!,
          ),
    )
    ..registerLazySingleton<KeyoxideRepository>(
        () => KeyoxideRepository(crudApi: getIt<DioCrudApi>()))
    ..registerLazySingleton<AppThemeRepository>(
        () => const AppThemeRepository())
    ..registerLazySingleton<AppThemeCubit>(
        () => AppThemeCubit(themeRepo: getIt<AppThemeRepository>()))
    ..registerLazySingleton<GenerateProfileCubit>(() => GenerateProfileCubit())
    ..registerLazySingleton<UserCubit>(() => UserCubit())
    ..registerLazySingleton(() => SettingsCubit())
    ..registerLazySingleton(() => SettingsRepository())
    ..registerLazySingleton(() => AspCubit())
    ..registerLazySingleton(() => LocalAuthenticationCubit())
    ..registerLazySingleton(() => ContactsCubit())
    ..registerLazySingleton(() => ContactsRepository())
    ..registerLazySingleton(() => TutorialCubit());
}

// Future<void> initializeKeyoxideRepoWithDio(String userBaseUrl) async {
//   // Optionally, dispose of the existing instance if necessary
//   if (getIt.isRegistered<DioCrudApi>()) {
//     getIt.unregister<DioCrudApi>();
//   }
//   // Register a new instance
//   getIt.registerLazySingleton<DioCrudApi>(
//         () => DioCrudApi(
//       apiConstants: APIConstants(),
//     ),
//   );
//
//   if (getIt.isRegistered<KeyoxideRepository>()) {
//     getIt.unregister<KeyoxideRepository>();
//   }
//
//   // Register a new instance
//   getIt.registerLazySingleton<KeyoxideRepository>(
//         () => KeyoxideRepository(crudApi: getIt<DioCrudApi>()),
//   );
// }
