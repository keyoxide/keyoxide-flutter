import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class KxLocalizationService {
  KxLocalizationService._internal();

  static final KxLocalizationService _singleton =
      KxLocalizationService._internal();

  factory KxLocalizationService() {
    return _singleton;
  }

  late Locale locale;

  static KxLocalizationService? of(BuildContext context) {
    return Localizations.of<KxLocalizationService>(
        context, KxLocalizationService);
  }

  static const LocalizationsDelegate<KxLocalizationService> delegate =
      _AppLocalizationsDelegate();
  static const List<Locale> supportedLocales = [
    Locale('en'),
    Locale('de'),
    Locale('es'),
    Locale('fr'),
    Locale('gl'),
    Locale('nl'),
    Locale('pl'),
    Locale('pt'),
    Locale('tr'),
    Locale('zh'),
    Locale('ja'),
  ];

  Map<String, String> _localizedStrings = {};
  Map<String, String> _fallbackLocalizedStrings = {}; // English as fallback

  Future<bool> load() async {
    _localizedStrings = await loadLanguage(locale.languageCode);
    _fallbackLocalizedStrings = await loadLanguage('en');
    return true;
  }

  Future<Map<String, String>> loadLanguage(String languageCode) async {
    String data = await rootBundle.loadString('lang/$languageCode.json');
    Map<String, dynamic> jsonMap = json.decode(data);

    return jsonMap.map((key, value) {
      return MapEntry(key, value.toString());
    });
  }

  String get(String? key, {bool showKeyOfMissingString = true}) {
    String? localizedString = _localizedStrings[key!] ?? _fallbackLocalizedStrings[key] ?? "Missing($key)";
    return localizedString;
  }

  String? getInterpolatedString(String? key, List<String> params) {
    String? result = _localizedStrings[key!] ?? _fallbackLocalizedStrings[key] ?? "Missing($key)";

    if (result != "Missing($key)") {
      RegExp exp = RegExp(r'\{([^\}]+)\}');
      int i = 0;

      result = result.replaceAllMapped(exp, (Match match) {
        if (i < params.length) {
          String replacement = params[i];
          i++;
          return replacement;
        } else {
          return match[0]!;
        }
      });
    }
    return result;
  }
}

class _AppLocalizationsDelegate
    extends LocalizationsDelegate<KxLocalizationService> {
  const _AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return KxLocalizationService.supportedLocales.any((supportedLocale) =>
        supportedLocale.languageCode == locale.languageCode);
  }

  @override
  Future<KxLocalizationService> load(Locale locale) async {
    KxLocalizationService localizations = KxLocalizationService();
    localizations.locale = locale;
    await localizations.load();
    return localizations;
  }

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}
