import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';
import '../../../common/services/locator_service.dart';
import '../../settings/cubit/settings_cubit.dart';

part 'tutorial_state.dart';

class TutorialCubit extends Cubit<TutorialState> {
  TutorialCubit() : super(TutorialInitial());
  GlobalKey navBarViewKey = GlobalKey();
  GlobalKey navBarContactsKey = GlobalKey();
  GlobalKey navBarUserKey = GlobalKey();
  GlobalKey appBarQrScannerKey = GlobalKey();
  GlobalKey appBarSettingsKey = GlobalKey();
  GlobalKey searchTextFieldKey = GlobalKey();

  String skipText = '';

  TutorialCoachMark? tutorialCoachMark;
  final List<TargetFocus> _targets = [];

  void showTutorial(BuildContext context) {
    tutorialCoachMark?.show(context: context);
  }

  void initTutorial() {
    tutorialCoachMark = TutorialCoachMark(
      targets: _createTargets(),
      colorShadow: Colors.red,
      textSkip: 'tutorial_btn_skip'.localize,
      paddingFocus: 10,
      opacityShadow: 0.5,
      imageFilter: ImageFilter.blur(sigmaX: 8, sigmaY: 8),
      onFinish: () {
        getIt<SettingsCubit>().completeTutorial();
      },
      onSkip: () {
        tutorialCoachMark?.finish();
        return true;
      },
      showSkipInLastTarget: false,
    );
  }

  List<TargetFocus> _createTargets() {
    _targets.add(
      TargetFocus(
        identify: "navBarViewBtn",
        keyTarget: navBarViewKey,
        color: Colors.purple,
        contents: [
          TargetContent(
            align: ContentAlign.top,
            builder: (context, controller) {
              return Column(
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "tutorial_title_welcome".localize,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 20.0,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: Text(
                          "tutorial_txt_welcome_desc".localize,
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 80,
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "tutorial_title_view".localize,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 20.0,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: Text(
                          "tutorial_txt_view_desc".localize,
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                      const SizedBox(height: 10),
                      ElevatedButton(
                        onPressed: () {
                          controller.next();
                        },
                        child: const Icon(Icons.chevron_right_outlined),
                      ),
                    ],
                  ),
                ],
              );
            },
          )
        ],
        shape: ShapeLightFocus.RRect,
        radius: 5,
      ),
    );

    _targets.add(
      TargetFocus(
        identify: "searchFieldAndBtn",
        keyTarget: searchTextFieldKey,
        color: Colors.purple,
        contents: [
          TargetContent(
            align: ContentAlign.bottom,
            builder: (context, controller) {
              return Column(
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: Text(
                          "tutorial_txt_search_desc".localize,
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                      const SizedBox(height: 10),
                      ElevatedButton(
                        onPressed: () {
                          controller.next();
                        },
                        child: const Icon(Icons.chevron_right_outlined),
                      ),
                    ],
                  ),
                ],
              );
            },
          )
        ],
        shape: ShapeLightFocus.RRect,
        radius: 5,
      ),
    );

    _targets.add(
      TargetFocus(
        identify: "qrScannerBtn",
        keyTarget: appBarQrScannerKey,
        color: Colors.purple,
        contents: [
          TargetContent(
            align: ContentAlign.bottom,
            builder: (context, controller) {
              return Column(
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: Text(
                          "tutorial_txt_qr_desc".localize,
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                      const SizedBox(height: 10),
                      ElevatedButton(
                        onPressed: () {
                          controller.next();
                        },
                        child: const Icon(Icons.chevron_right_outlined),
                      ),
                    ],
                  ),
                ],
              );
            },
          )
        ],
        shape: ShapeLightFocus.RRect,
        radius: 5,
      ),
    );
    _targets.add(
      TargetFocus(
        identify: "settingsBtn",
        keyTarget: appBarSettingsKey,
        color: Colors.purple,
        contents: [
          TargetContent(
            align: ContentAlign.bottom,
            builder: (context, controller) {
              return Column(
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: Text(
                          "tutorial_txt_settings_desc".localize,
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                      const SizedBox(height: 10),
                      ElevatedButton(
                        onPressed: () {
                          controller.next();
                        },
                        child: const Icon(Icons.chevron_right_outlined),
                      ),
                    ],
                  ),
                ],
              );
            },
          )
        ],
        shape: ShapeLightFocus.RRect,
        radius: 5,
      ),
    );

    _targets.add(
      TargetFocus(
        identify: "navBarContactsBtn",
        keyTarget: navBarContactsKey,
        color: Colors.purple,
        contents: [
          TargetContent(
            align: ContentAlign.top,
            builder: (context, controller) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "tutorial_title_contacts".localize,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 20.0,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Text(
                      "tutorial_txt_contacts_desc".localize,
                      style: const TextStyle(color: Colors.white),
                    ),
                  ),
                  const SizedBox(height: 10),
                  ElevatedButton(
                    onPressed: () {
                      controller.next();
                    },
                    child: const Icon(Icons.chevron_right_outlined),
                  ),
                ],
              );
            },
          )
        ],
        shape: ShapeLightFocus.RRect,
        radius: 5,
      ),
    );

    _targets.add(
      TargetFocus(
        identify: "navBarUserBtn",
        keyTarget: navBarUserKey,
        color: Colors.purple,
        contents: [
          TargetContent(
            align: ContentAlign.top,
            builder: (context, controller) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "tutorial_title_user".localize,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 20.0,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Text(
                      "tutorial_txt_user_desc".localize,
                      style: const TextStyle(color: Colors.white),
                    ),
                  ),
                  const SizedBox(height: 10),
                  ElevatedButton(
                    onPressed: () {
                      controller.next();
                    },
                    child: Text(
                      'general_txt_done'.localize,
                      textAlign: TextAlign.start,
                    ),
                  ),
                ],
              );
            },
          )
        ],
        shape: ShapeLightFocus.RRect,
        radius: 5,
      ),
    );
    return _targets;
  }
}
