part of 'tutorial_cubit.dart';

abstract class TutorialState extends Equatable {
  const TutorialState();
}

class TutorialInitial extends TutorialState {
  @override
  List<Object> get props => [];
}
