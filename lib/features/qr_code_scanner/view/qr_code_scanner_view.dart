import 'dart:developer';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import '../../../common/services/locator_service.dart';
import '../../../common/widgets/keyoxide_snackbar.dart';
import '../../generate_profile/cubit/generate_profile_cubit.dart';

class QrCodeScannerView extends StatefulWidget {
  const QrCodeScannerView({super.key});

  @override
  State<StatefulWidget> createState() => _QrCodeScannerViewState();
}

class _QrCodeScannerViewState extends State<QrCodeScannerView> {
  Barcode? result;
  QRViewController? controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  Future<void> reassemble() async {
    super.reassemble();
    if (Platform.isAndroid) {
      await controller!.pauseCamera();
    }
    await controller!.resumeCamera();
  }

  @override
  Widget build(BuildContext context) {
    // it's wrong to have this in build method but otherwise scanner launches with camera off
    controller?.resumeCamera();
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(flex: 4, child: _buildQrView(context)),
          Expanded(
            flex: 1,
            child: FittedBox(
              fit: BoxFit.contain,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  if (result != null)
                    Text(
                        'Barcode Type: ${describeEnum(result!.format)}   Data: ${result!.code}')
                  else
                    Text(
                      'qr_scanner_title_scan_code'.localize,
                      semanticsLabel:
                          'qr_scanner_title_scan_code_semantics'.localize,
                    ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.all(8),
                        child: Semantics(
                          excludeSemantics: true,
                          label: 'qr_scanner_btn_back_semantics'.localize,
                          child: FilledButton(
                              onPressed: () async {
                                context.pop();
                              },
                              child: const Icon(Icons.arrow_back_outlined)),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.all(8),
                        child: Semantics(
                          excludeSemantics: true,
                          label: 'qr_scanner_btn_flash_semantics'.localize,
                          child: FilledButton(
                              onPressed: () async {
                                await controller?.toggleFlash();
                                setState(() {});
                              },
                              child: FutureBuilder(
                                future: controller?.getFlashStatus(),
                                builder: (context, snapshot) {
                                  return FittedBox(
                                      child: Icon(snapshot.data == false
                                          ? Icons.flash_off_outlined
                                          : Icons.flash_on_outlined));
                                },
                              )),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.all(8),
                        child: Semantics(
                          excludeSemantics: true,
                          label:
                              'qr_scanner_btn_flip_camera_semantics'.localize,
                          child: FilledButton(
                              onPressed: () async {
                                await controller?.flipCamera();
                                setState(() {});
                              },
                              child: FutureBuilder(
                                future: controller?.getCameraInfo(),
                                builder: (context, snapshot) {
                                  if (snapshot.data != null) {
                                    return const FittedBox(
                                      child: Icon(Icons.cameraswitch_outlined),
                                    );
                                  } else {
                                    return Text(
                                      'general_txt_loading'.localize,
                                    );
                                  }
                                },
                              )),
                        ),
                      )
                    ],
                  ),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.center,
                  //   crossAxisAlignment: CrossAxisAlignment.center,
                  //   children: <Widget>[
                  //     Container(
                  //       margin: const EdgeInsets.all(8),
                  //       child: ElevatedButton(
                  //         onPressed: () async {
                  //           _isCameraOn ? await controller?.pauseCamera() :
                  //           await controller?.resumeCamera();
                  //           setState(() {
                  //             _isCameraOn = !_isCameraOn;
                  //           });
                  //         },
                  //         child: Text(_isCameraOn ? 'off' : 'on',
                  //             style: const TextStyle(fontSize: 20)),
                  //       ),
                  //     )
                  //   ],
                  // ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildQrView(BuildContext context) {
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 150.0
        : 300.0;
    // To ensure the Scanner view is properly sizes after rotation
    // we need to listen for Flutter SizeChanged notification and update controller
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(
          borderColor: Colors.red,
          borderRadius: 10,
          borderLength: 30,
          borderWidth: 10,
          cutOutSize: scanArea),
      onPermissionSet: (ctrl, p) => _onPermissionSet(context, ctrl, p),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
    });
    controller.scannedDataStream.listen((scanData) {
      controller.stopCamera();
      setState(() {
        result = scanData;
      });
      context.pop();

      if (result!.code.toString().contains('@') ||
          result!.code.toString().startsWith('aspe')) {
        getIt<GenerateProfileCubit>().getProfileClaimCount(
            result!.code.toString(),
            GenerateProfileFor.displayProfileView,
            false);
      } else {
        getIt<GenerateProfileCubit>().getProfileClaimCount(
            result!.code
                .toString()
                .substring(12, result!.code.toString().length),
            GenerateProfileFor.displayProfileView,
            false);
      }
    });
  }

  void _onPermissionSet(BuildContext context, QRViewController ctrl, bool p) {
    log('${DateTime.now().toIso8601String()}_onPermissionSet $p');
    if (!p) {
      KeyoxideSnackBar().showOverlaySnackBar(context, "No permission");
    }
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}
