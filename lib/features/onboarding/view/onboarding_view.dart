import 'package:flutter/material.dart';
import 'package:flutter_onboarding_slider/flutter_onboarding_slider.dart';
import 'package:go_router/go_router.dart';
import 'package:keyoxide_flutter/common/constants/keyoxide_constants.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/features/settings/cubit/settings_cubit.dart';

import '../../../common/services/locator_service.dart';

class OnBoardingView extends StatelessWidget {
  const OnBoardingView({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double sizedBoxHeight = screenHeight < 900 ? 260 : 400;
    TextStyle textStyle = TextStyle(
      color: Theme.of(context).hintColor,
      fontSize: 18,
      fontWeight: FontWeight.w600,
    );
    return OnBoardingSlider(
      background: [
        Padding(
          padding: const EdgeInsets.only(top: 8),
          child: SizedBox(
            height: sizedBoxHeight,
            width: screenWidth,
            child: Image.asset('assets/onboarding/slide_1.png',
                fit: BoxFit.contain),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8),
          child: SizedBox(
            height: sizedBoxHeight,
            width: screenWidth,
            child: Image.asset('assets/onboarding/slide_2.png',
                fit: BoxFit.contain),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8),
          child: SizedBox(
            height: sizedBoxHeight,
            width: screenWidth,
            child: Image.asset('assets/onboarding/slide_3.png',
                fit: BoxFit.contain),
          ),
        ),
      ],
      headerBackgroundColor: Theme.of(context).scaffoldBackgroundColor,
      controllerColor: Theme.of(context).colorScheme.primary,
      finishButtonText: 'onboarding_btn_get_started'.localize,
      finishButtonStyle: FinishButtonStyle(
        backgroundColor: Theme.of(context).colorScheme.primary,
      ),
      skipTextButton: Text(
        'onboarding_btn_skip'.localize,
        style: textStyle.copyWith(color: Theme.of(context).colorScheme.primary),
      ),
      pageBackgroundColor: Theme.of(context).scaffoldBackgroundColor,
      onFinish: () {
        getIt<SettingsCubit>().completeOnboarding();
        context.pushReplacementNamed(rootRouteName);
      },
      totalPage: 3,
      speed: 1.8,
      pageBodies: [
        Container(
          padding: const EdgeInsets.only(left: 30, right: 30, bottom: 60),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: sizedBoxHeight,
              ),
              const Spacer(),
              Text(
                'onboarding_slide_1'.localize,
                textAlign: TextAlign.center,
                style: textStyle,
              ),
              const SizedBox(
                height: 40,
              ),
            ],
          ),
        ),
        Container(
          padding: const EdgeInsets.only(left: 30, right: 30, bottom: 60),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: sizedBoxHeight,
              ),
              const Spacer(),
              Text(
                'onboarding_slide_2'.localize,
                textAlign: TextAlign.center,
                style: textStyle,
              ),
              const SizedBox(
                height: 40,
              ),
            ],
          ),
        ),
        Container(
          padding: const EdgeInsets.only(left: 30, right: 30, bottom: 60),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: sizedBoxHeight,
              ),
              const Spacer(),
              Text(
                'onboarding_slide_3'.localize,
                textAlign: TextAlign.center,
                style: textStyle,
              ),
              const SizedBox(
                height: 40,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
