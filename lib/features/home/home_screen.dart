import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/common/widgets/app_bar_widget.dart';
import 'package:keyoxide_flutter/features/tutorial/cubit/tutorial_cubit.dart';

import '../../common/constants/keyoxide_constants.dart';
import '../../common/services/locator_service.dart';
import '../contacts/view/contacts_view.dart';
import '../generate_profile/view/generate_profile_view.dart';
import '../user/cubit/user_cubit.dart';
import '../user/view/user_view.dart';
import 'dart:io';

class HomeScreen extends StatefulWidget {

  const HomeScreen({required String tab, super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();

  // static int indexFrom(String tab) {
  //   switch (tab) {
  //     case generateProfileRouteName:
  //       return 0;
  //     case contactsRouteName:
  //       return 1;
  //     case userRouteName:
  //       return 2;
  //     default:
  //       return 0;
  //   }
  // }
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedIndex = 0;
  late bool _isIos;

  @override
  void initState() {
    super.initState();
    // Tutorial is initialized here because the localized strings are needed
    if (!getIt<UserCubit>().keyoxideUser.userSettings.isTutorialComplete) {
      getIt<TutorialCubit>().initTutorial();
      getIt<TutorialCubit>().showTutorial(context);
    }
    //_selectedIndex = widget.index;
    _isIos = Platform.isIOS;
  }

  // @override
  // void didChangeDependencies() {
  //   super.didChangeDependencies();
  //   _selectedIndex = widget.index;
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      resizeToAvoidBottomInset: true,
      appBar: const AppBarWidget(),
      bottomNavigationBar: SizedBox(
        height: _isIos ? 95 : 80,
        child: NavigationBar(
          destinations: [
            Semantics(
                label: 'bottom_navigation_bar_title_view_semantics'.localize,
                child: NavigationDestination(
                  key: getIt<TutorialCubit>().navBarViewKey,
                  icon: const Icon(
                    Icons.person_search_outlined,
                    //size: 40,
                  ),
                  label: 'bottom_navigation_bar_title_view'.localize,
                )),
            Semantics(
              label: 'bottom_navigation_bar_title_contacts_semantics'.localize,
              child: NavigationDestination(
                  key: getIt<TutorialCubit>().navBarContactsKey,
                  icon: const Icon(
                    Icons.contacts_outlined,
                  ),
                  label: 'bottom_navigation_bar_title_contacts'.localize),
            ),
            Semantics(
                label: 'bottom_navigation_bar_title_user_semantics'.localize,
                child: NavigationDestination(
                  key: getIt<TutorialCubit>().navBarUserKey,
                  icon: const Icon(
                    Icons.account_circle_outlined,
                    //size: 40,
                  ),
                  label: 'bottom_navigation_bar_title_user'.localize,
                )),
          ],
          //backgroundColor: Theme.of(context).colorScheme.background,
          selectedIndex: _selectedIndex,
          elevation: 0,
          indicatorShape: const StadiumBorder(),
          // indicatorColor:
          //     Theme.of(context).floatingActionButtonTheme.backgroundColor,
          onDestinationSelected: (index) {
            setState(
              () {
                _selectedIndex = index;
                switch (index) {
                  case 0:
                    context.namedLocation(
                      homeRouteName,
                      pathParameters: {'tab': generateProfileRouteName},
                    );
                    break;
                  case 1:
                    context.namedLocation(
                      homeRouteName,
                      pathParameters: {'tab': contactsRouteName},
                    );
                    break;
                  case 2:
                    context.namedLocation(
                      homeRouteName,
                      pathParameters: {'tab': userRouteName},
                    );
                    break;
                }
              },
            );
          },
        ),
      ),
      body: SafeArea(
        child: IndexedStack(
          index: _selectedIndex,
          children: const [GenerateProfileView(), ContactsView(), UserView()],
        ),
      ),
    );
  }
}
