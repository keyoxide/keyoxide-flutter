import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/services.dart';
import 'package:keyoxide_flutter/common/models/keyoxide_result.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:local_auth/local_auth.dart';
import 'package:local_auth/error_codes.dart' as auth_error;

part 'local_authentication_state.dart';

enum LocalAuthFor {
  toggleSettings,
  authentication,
}

class LocalAuthenticationCubit extends Cubit<LocalAuthenticationState> {
  LocalAuthenticationCubit() : super(LocalAuthenticationInitial());

  final LocalAuthentication _localAuthentication = LocalAuthentication();

  Future<void> authenticate(LocalAuthFor authType) async {
    emit(LocalAuthenticationLoading());

    try {
      bool isAuthenticated = await _localAuthentication.authenticate(
          localizedReason: 'auth_screen_reason'.localize,
          options: const AuthenticationOptions(
            stickyAuth: true,
          ));

      if (isAuthenticated) {
        emit(authType == LocalAuthFor.authentication
            ? LocalAuthenticationSuccess()
            : LocalAuthenticationToggleSuccess());
      }
    } on PlatformException catch (e) {
      String errorMessage = 'auth_screen_local_error'.localize;
      if (e.code == auth_error.notAvailable) {
        errorMessage = 'auth_screen_no_auth_method_error'.localize;
      } else if (e.code == auth_error.notEnrolled) {
        errorMessage = 'auth_screen_no_enroll_error'.localize;
      }
      emit(LocalAuthenticationFailure(
          error: KeyoxideError(message: errorMessage)));
    }
  }
}
