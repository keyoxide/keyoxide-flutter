part of 'local_authentication_cubit.dart';

abstract class LocalAuthenticationState extends Equatable {
  const LocalAuthenticationState();

  @override
  List<Object> get props => [];
}

class LocalAuthenticationInitial extends LocalAuthenticationState {}

class LocalAuthenticationLoading extends LocalAuthenticationState {}

class LocalAuthenticationSuccess extends LocalAuthenticationState {}

class LocalAuthenticationFailure extends LocalAuthenticationState {
  final KeyoxideError error;

  const LocalAuthenticationFailure({required this.error});
}

class LocalAuthenticationToggleSuccess extends LocalAuthenticationState {}
