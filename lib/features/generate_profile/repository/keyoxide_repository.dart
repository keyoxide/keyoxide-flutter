import 'package:dio/dio.dart';
import 'package:keyoxide_flutter/app/app_config.dart';
import 'package:keyoxide_flutter/common/constants/keyoxide_constants.dart';
import 'package:keyoxide_flutter/common/constants/keyoxide_enums.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/features/generate_profile/model/keyoxide_profile.dart';
import 'package:keyoxide_flutter/features/user/cubit/user_cubit.dart';

import '../../../common/api/crud/dio_crud_api.dart';
import '../../../common/api/crud/dio_exceptions.dart';
import '../../../common/models/keyoxide_result.dart';
import '../../../common/services/locator_service.dart';

class KeyoxideRepository {
  final DioCrudApi crudApi;

  KeyoxideRepository({required this.crudApi});

  Future<KeyoxideResult> generateProfile(String query,
      {FingerprintProtocol? protocol}) async {
    try {
      String baseUrl =
          getIt<UserCubit>().keyoxideUser.userSettings.verificationApiDomain;
      final url = Uri(
              scheme: 'https',
              host: baseUrl,
              path: '${APIConstants.endPoint}/fetch')
          .toString();
      Map<String, dynamic> queryParameters = {
        'query': query,
        'doVerification': true
      };
      if (protocol != null) {
        queryParameters['protocol'] = protocol.name;
      }
      final response = await crudApi.get(url, queryParameters: queryParameters);
      KeyoxideError error = KeyoxideError(
          code: response.statusCode, message: response.statusMessage);
      if (response.statusCode == 200) {
        if (!isDataInJsonFormat(response)) {
          error = const KeyoxideError(
              code: 422, message: 'Invalid server response format.');
          return KeyoxideResult(error: error);
        } else {
          return KeyoxideResult(data: KeyoxideProfile.fromJson(response.data));
        }
      } else {
        if (response.statusCode == 500) {
          error = KeyoxideError(
            code: 500,
            message: 'view_page_profile_generation_error'.localize,
          );
        }
        return KeyoxideResult(error: error);
      }
    } on DioError catch (e) {
      final errorMessage = DioExceptions.fromDioError(e).toString();
      return KeyoxideResult(
          error: KeyoxideError(
              message: errorMessage,
              error: "DIO ERROR",
              code: e.response?.statusCode));
    }
  }

  Future<KeyoxideResult> getProfileClaimCount(String query,
      {FingerprintProtocol? protocol}) async {
    try {
      String baseUrl =
          getIt<UserCubit>().keyoxideUser.userSettings.verificationApiDomain;
      final url = Uri(
              scheme: 'https',
              host: baseUrl,
              path: '${APIConstants.endPoint}/fetch')
          .toString();
      Map<String, dynamic> queryParameters = {
        'query': query,
        'doVerification': false,
      };
      if (protocol != null) {
        queryParameters['protocol'] = protocol.name;
      }
      final response = await crudApi.get(url, queryParameters: queryParameters);

      if (response.statusCode == 200) {
        if (!isDataInJsonFormat(response)) {
          return const KeyoxideResult(
              error: KeyoxideError(
                  // We assign invalid format error here.
                  code: 422,
                  message: "Invalid server response format."));
        } else {
          var profile = KeyoxideProfile.fromJson(response.data);

          return KeyoxideResult(data: profile);
        }
      } else {
        return KeyoxideResult(
            error: KeyoxideError(
                code: response.statusCode, message: response.statusMessage));
      }
    } on DioError catch (e) {
      final errorMessage = DioExceptions.fromDioError(e).toString();
      return KeyoxideResult(
          error: KeyoxideError(
              message: errorMessage,
              error: "DIO ERROR",
              code: e.response?.statusCode));
    }
  }

  bool isDataInJsonFormat(Response response) {
    return response.data is Map<String, dynamic>;
  }
}
