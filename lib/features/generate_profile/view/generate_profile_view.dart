import 'dart:async';

import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:go_router/go_router.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/common/widgets/claim_count_loading_overlay.dart';
import 'package:keyoxide_flutter/features/generate_profile/model/search_history_item.dart';
import 'package:keyoxide_flutter/features/tutorial/cubit/tutorial_cubit.dart';
import 'package:keyoxide_flutter/features/user/cubit/user_cubit.dart';

import '../../../common/constants/keyoxide_constants.dart';
import '../../../common/services/locator_service.dart';
import '../../../common/widgets/keyoxide_snackbar.dart';
import '../cubit/generate_profile_cubit.dart';

class GenerateProfileView extends StatefulWidget {
  const GenerateProfileView({Key? key}) : super(key: key);

  @override
  State<GenerateProfileView> createState() => _GenerateProfileViewState();
}

class _GenerateProfileViewState extends State<GenerateProfileView> {
  final _form = GlobalKey<FormState>();
  final FocusNode _generateButtonFocusNode = FocusNode();
  final FocusNode _textFieldFocusNode = FocusNode();
  final _controller = TextEditingController();
  String _pasteValue = "";
  String _queryValue = "";
  bool _isLoading = false;
  SuggestionsController<SearchHistoryItem> _suggestionsController =
      SuggestionsController();

  @override
  void didChangeDependencies() {
    if (_pasteValue != "") {
      _queryValue = _pasteValue;
      _controller.text = _pasteValue;
      _fetchProfile();
    }
    context.dependOnInheritedWidgetOfExactType();
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _queryValue = "";
    _pasteValue = "";
    _textFieldFocusNode.unfocus();
    _textFieldFocusNode.dispose();
    _generateButtonFocusNode.unfocus();
    _generateButtonFocusNode.dispose();
    super.dispose();
  }

  bool _saveForm() {
    if (_form.currentState != null) {
      final isValid = _form.currentState!.validate();
      if (isValid) {
        _form.currentState!.save();
        return true;
      }
    }
    return false;
  }

  void _fetchProfile() async {
    if (_saveForm() || _pasteValue != "") {
      getIt<GenerateProfileCubit>().getProfileClaimCount(
          _queryValue, GenerateProfileFor.displayProfileView, false);
    }
  }

  Widget _buildMobileView() {
    return Stack(children: [
      SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                width: double.infinity,
                child: Column(
                  children: [
                    const SizedBox(height: 50),
                    SvgPicture.asset('assets/logo/keyoxide_outline.svg',
                        width: 80,
                        height: 80,
                        color: Theme.of(context).colorScheme.primary),
                    const SizedBox(height: 40),
                    // Fingerprint text field
                    Column(
                      key: getIt<TutorialCubit>().searchTextFieldKey,
                      children: [
                        Form(
                          key: _form,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Semantics(
                              textField: true,
                              excludeSemantics: true,
                              label: 'view_page_text_field_semantics'.localize,
                              child: TypeAheadField<SearchHistoryItem>(
                                focusNode: _textFieldFocusNode,
                                controller: _controller,
                                suggestionsController: _suggestionsController,
                                hideOnEmpty: true,
                                hideOnUnfocus: true,
                                suggestionsCallback: (search) =>
                                    getIt<UserCubit>().searchHistory(search),
                                builder: (context, controller, focusNode) {
                                  return TextFormField(
                                    controller: controller,
                                    focusNode: focusNode,
                                    key: Key(_pasteValue),
                                    maxLines: 2,
                                    onTapOutside: (_) => focusNode.unfocus(),
                                    onFieldSubmitted: (_) =>
                                        FocusScope.of(context).requestFocus(
                                            _generateButtonFocusNode),
                                    textInputAction: TextInputAction.done,
                                    decoration: InputDecoration(
                                      suffixIconColor:
                                          Theme.of(context).colorScheme.primary,
                                      hintText:
                                          'view_page_text_field_hint'.localize,
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary)),
                                      errorBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .error)),
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary)),
                                      suffixIcon: IconButton(
                                        onPressed: () {
                                          FlutterClipboard.paste()
                                              .then((value) {
                                            setState(() {
                                              _pasteValue = value;
                                              _controller.text = value;
                                            });
                                          });
                                        },
                                        icon: const Icon(
                                          Icons.paste,
                                          //color: ColorConsts.buttonBackground,
                                        ),
                                      ),
                                    ),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'view_page_text_field_empty_error'
                                            .localize;
                                      }
                                      return null;
                                    },
                                    onSaved: (value) {
                                      _queryValue = value.toString();
                                    },
                                  );
                                },
                                itemBuilder: (context, searchHistoryItem) {
                                  return ListTile(
                                    leading: const Icon(Icons.history_outlined),
                                    title: Text(
                                        searchHistoryItem.profileIdentifier),
                                    subtitle:
                                        Text(searchHistoryItem.profileName),
                                    trailing: IconButton(
                                        icon: const Icon(
                                            Icons.remove_circle_outline),
                                        onPressed: () async {
                                          await getIt<UserCubit>()
                                              .updateSearchHistory(
                                                  searchHistoryItem:
                                                      searchHistoryItem,
                                                  isDeleting: true);
                                          _suggestionsController.refresh();
                                        }),
                                  );
                                },
                                onSelected: (searchHistoryItem) {
                                  FocusScope.of(context).unfocus();
                                  _queryValue =
                                      searchHistoryItem.profileIdentifier;
                                  getIt<GenerateProfileCubit>()
                                      .getProfileClaimCount(
                                          _queryValue,
                                          GenerateProfileFor.displayProfileView,
                                          false);
                                },
                              ),
                            ),
                          ),
                        ),
                        Semantics(
                          excludeSemantics: true,
                          label:
                              'view_page_btn_view_profile_semantics'.localize,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: FilledButton.icon(
                              style: FilledButton.styleFrom(),
                              onPressed: () {
                                _textFieldFocusNode.unfocus();
                                _fetchProfile();
                              },
                              icon: const Icon(Icons.person_search_outlined),
                              label: Text(
                                'view_page_btn_view_profile'.localize,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      // Shows a loading circle when the API fetches the data
      if (_isLoading) ClaimCountLoadingOverlay(),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocConsumer<GenerateProfileCubit, GenerateProfileState>(
          bloc: getIt<GenerateProfileCubit>(),
          listener: (context, state) {
            if (state is ProfileGenerationFailure) {
              String errorMessage = state.error.message.toString();

              if (state.error.code == 500) {
                errorMessage = 'view_page_profile_generation_error'.localize;
              }
              KeyoxideSnackBar().showOverlaySnackBar(context, errorMessage);
            } else if (state is GetProfileClaimsSuccess) {
              getIt<GenerateProfileCubit>().generateProfile(state.queryValue,
                  GenerateProfileFor.displayProfileView, false,
                  protocol: state.protocol);
            } else if (state is ProfileGenerationSuccess) {
              Future.delayed(Duration.zero, () async {
                context.push(displayProfilePath);
              });
            }
          },
          builder: (context, state) {
            if (state is ProfileLoading || state is GetProfileClaimsSuccess) {
              _isLoading = true;
            } else {
              _isLoading = false;
            }

            return _buildMobileView();
          },
        ),
      ),
    );
  }
}
