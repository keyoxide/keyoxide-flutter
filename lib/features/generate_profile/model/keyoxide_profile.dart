class KeyoxideProfile {
  KeyoxideProfile({
    this.profileVersion,
    this.profileType,
    this.identifier,
    this.personas,
    this.primaryPersonaIndex,
    this.publicKey,
    this.verifiers,
  });

  KeyoxideProfile.fromJson(dynamic json) {
    profileVersion = json['profileVersion'];
    profileType = json['profileType'];
    identifier = json['identifier'];
    if (json['personas'] != null) {
      personas = [];
      json['personas'].forEach((v) {
        personas?.add(Personas.fromJson(v));
      });
    }
    primaryPersonaIndex = json['primaryPersonaIndex'];
    publicKey = json['publicKey'] != null
        ? PublicKey.fromJson(json['publicKey'])
        : null;
    if (json['verifiers'] != null) {
      verifiers = [];
      json['verifiers'].forEach((v) {
        verifiers?.add(Verifiers.fromJson(v));
      });
    }
  }

  int? profileVersion;
  String? profileType;
  String? identifier;
  List<Personas>? personas;
  int? primaryPersonaIndex;
  PublicKey? publicKey;
  List<Verifiers>? verifiers;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['profileVersion'] = profileVersion;
    map['profileType'] = profileType;
    map['identifier'] = identifier;
    if (personas != null) {
      map['personas'] = personas?.map((v) => v.toJson()).toList();
    }
    map['primaryPersonaIndex'] = primaryPersonaIndex;
    if (publicKey != null) {
      map['publicKey'] = publicKey?.toJson();
    }
    if (verifiers != null) {
      map['verifiers'] = verifiers?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Verifiers {
  Verifiers({
    this.name,
    this.url,
  });

  Verifiers.fromJson(dynamic json) {
    name = json['name'];
    url = json['url'];
  }

  String? name;
  String? url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = name;
    map['url'] = url;
    return map;
  }
}

class PublicKey {
  PublicKey({
    this.keyType,
    this.fingerprint,
    this.encoding,
    this.encodedKey,
    this.fetch,
  });

  PublicKey.fromJson(dynamic json) {
    keyType = json['keyType'];
    fingerprint = json['fingerprint'];
    encoding = json['encoding'];
    encodedKey = json['encodedKey'];
    fetch = json['fetch'] != null ? Fetch.fromJson(json['fetch']) : null;
  }

  String? keyType;
  String? fingerprint;
  String? encoding;
  String? encodedKey;
  Fetch? fetch;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['keyType'] = keyType;
    map['fingerprint'] = fingerprint;
    map['encoding'] = encoding;
    map['encodedKey'] = encodedKey;
    if (fetch != null) {
      map['fetch'] = fetch?.toJson();
    }
    return map;
  }
}

class Fetch {
  Fetch({
    this.method,
    this.query,
    this.resolvedUrl,
  });

  Fetch.fromJson(dynamic json) {
    method = json['method'];
    query = json['query'];
    resolvedUrl = json['resolvedUrl'];
  }

  String? method;
  String? query;
  String? resolvedUrl;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['method'] = method;
    map['query'] = query;
    map['resolvedUrl'] = resolvedUrl;
    return map;
  }
}

class Personas {
  Personas({
    this.identifier,
    this.name,
    this.email,
    this.description,
    this.avatarUrl,
    this.isRevoked,
    this.claims,
    this.themeColor,
  });

  Personas.fromJson(dynamic json) {
    identifier = json['identifier'];
    name = json['name'];
    email = json['email'];
    description = json['description'];
    avatarUrl = json['avatarUrl'];
    isRevoked = json['isRevoked'];
    if (json['claims'] != null) {
      claims = [];
      json['claims'].forEach((v) {
        claims?.add(Claims.fromJson(v));
      });
    }
    themeColor = json['themeColor'];
  }

  dynamic identifier;
  String? name;
  dynamic email;
  dynamic description;
  String? avatarUrl;
  bool? isRevoked;
  List<Claims>? claims;
  String? themeColor;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['identifier'] = identifier;
    map['name'] = name;
    map['email'] = email;
    map['description'] = description;
    map['avatarUrl'] = avatarUrl;
    map['isRevoked'] = isRevoked;
    if (claims != null) {
      map['claims'] = claims?.map((v) => v.toJson()).toList();
    }
    map['themeColor'] = themeColor;
    return map;
  }
}

class Claims {
  Claims({
    this.claimVersion,
    this.uri,
    this.proofs,
    this.matches,
    this.status,
    this.display,
  });

  Claims.fromJson(dynamic json) {
    claimVersion = json['claimVersion'];
    uri = json['uri'];
    proofs = json['proofs'] != null ? json['proofs'].cast<String>() : [];
    if (json['matches'] != null) {
      matches = [];
      json['matches'].forEach((v) {
        matches?.add(Matches.fromJson(v));
      });
    }
    status = json['status'];
    display =
        json['display'] != null ? Display.fromJson(json['display']) : null;
  }

  int? claimVersion;
  String? uri;
  List<String>? proofs;
  List<Matches>? matches;
  int? status;
  Display? display;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['claimVersion'] = claimVersion;
    map['uri'] = uri;
    map['proofs'] = proofs;
    if (matches != null) {
      map['matches'] = matches?.map((v) => v.toJson()).toList();
    }
    map['status'] = status;
    if (display != null) {
      map['display'] = display?.toJson();
    }
    return map;
  }
}

class Display {
  Display({
    this.profileName,
    this.profileUrl,
    this.proofUrl,
    this.serviceProviderName,
    this.serviceProviderId,
  });

  Display.fromJson(dynamic json) {
    profileName = json['profileName'];
    profileUrl = json['profileUrl'];
    proofUrl = json['proofUrl'];
    serviceProviderName = json['serviceProviderName'];
    serviceProviderId = json['serviceProviderId'];
  }

  String? profileName;
  String? profileUrl;
  String? proofUrl;
  String? serviceProviderName;
  String? serviceProviderId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['profileName'] = profileName;
    map['profileUrl'] = profileUrl;
    map['proofUrl'] = proofUrl;
    map['serviceProviderName'] = serviceProviderName;
    map['serviceProviderId'] = serviceProviderId;
    return map;
  }
}

class Matches {
  Matches({
    this.about,
    this.profile,
    this.claim,
    this.proof,
  });

  Matches.fromJson(dynamic json) {
    about = json['about'] != null ? About.fromJson(json['about']) : null;
    profile =
        json['profile'] != null ? Profile.fromJson(json['profile']) : null;
    claim = json['claim'] != null ? Claim.fromJson(json['claim']) : null;
    proof = json['proof'] != null ? Proof.fromJson(json['proof']) : null;
  }

  About? about;
  Profile? profile;
  Claim? claim;
  Proof? proof;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (about != null) {
      map['about'] = about?.toJson();
    }
    if (profile != null) {
      map['profile'] = profile?.toJson();
    }
    if (claim != null) {
      map['claim'] = claim?.toJson();
    }
    if (proof != null) {
      map['proof'] = proof?.toJson();
    }
    return map;
  }
}

class Proof {
  Proof({
    this.request,
    this.response,
    this.target,
  });

  Proof.fromJson(dynamic json) {
    request =
        json['request'] != null ? KXRequest.fromJson(json['request']) : null;
    response =
        json['response'] != null ? KXResponse.fromJson(json['response']) : null;
    if (json['target'] != null) {
      target = [];
      json['target'].forEach((v) {
        target?.add(Target.fromJson(v));
      });
    }
  }

  KXRequest? request;
  KXResponse? response;
  List<Target>? target;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (request != null) {
      map['request'] = request?.toJson();
    }
    if (response != null) {
      map['response'] = response?.toJson();
    }
    if (target != null) {
      map['target'] = target?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Target {
  Target({
    this.format,
    this.encoding,
    this.relation,
    this.path,
  });

  Target.fromJson(dynamic json) {
    format = json['format'];
    encoding = json['encoding'];
    relation = json['relation'];
    path = json['path'] != null ? json['path'].cast<String>() : [];
  }

  String? format;
  String? encoding;
  String? relation;
  List<String>? path;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['format'] = format;
    map['encoding'] = encoding;
    map['relation'] = relation;
    map['path'] = path;
    return map;
  }
}

class KXResponse {
  KXResponse({
    this.format,
  });

  KXResponse.fromJson(dynamic json) {
    format = json['format'];
  }

  String? format;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['format'] = format;
    return map;
  }
}

class KXRequest {
  KXRequest({
    this.uri,
    this.fetcher,
    this.accessRestriction,
    this.data,
  });

  KXRequest.fromJson(dynamic json) {
    uri = json['uri'];
    fetcher = json['fetcher'];
    accessRestriction = json['accessRestriction'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  String? uri;
  String? fetcher;
  String? accessRestriction;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['uri'] = uri;
    map['fetcher'] = fetcher;
    map['accessRestriction'] = accessRestriction;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

class Data {
  Data({
    this.url,
  });

  Data.fromJson(dynamic json) {
    url = json['url'];
  }

  String? url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['url'] = url;
    return map;
  }
}

class Claim {
  Claim({
    this.uriRegularExpression,
    this.uriIsAmbiguous,
  });

  Claim.fromJson(dynamic json) {
    uriRegularExpression = json['uriRegularExpression'];
    uriIsAmbiguous = json['uriIsAmbiguous'];
  }

  String? uriRegularExpression;
  bool? uriIsAmbiguous;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['uriRegularExpression'] = uriRegularExpression;
    map['uriIsAmbiguous'] = uriIsAmbiguous;
    return map;
  }
}

class Profile {
  Profile({
    this.display,
    this.uri,
    this.qr,
  });

  Profile.fromJson(dynamic json) {
    display = json['display'];
    uri = json['uri'];
    qr = json['qr'];
  }

  String? display;
  String? uri;
  dynamic qr;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['display'] = display;
    map['uri'] = uri;
    map['qr'] = qr;
    return map;
  }
}

class About {
  About({
    this.id,
    this.name,
    this.homepage,
  });

  About.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    homepage = json['homepage'];
  }

  String? id;
  String? name;
  String? homepage;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['homepage'] = homepage;
    return map;
  }
}
