class SearchHistoryItem {
  final String profileIdentifier;
  final String fingerprint;
  final String profileName;

  SearchHistoryItem({
    required this.profileIdentifier,
    required this.fingerprint,
    required this.profileName,
  });

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['profileIdentifier'] = profileIdentifier;
    map['fingerprint'] = fingerprint;
    map['profileName'] = profileName;
    return map;
  }

  factory SearchHistoryItem.fromJson(Map<String, dynamic> map) {
    return SearchHistoryItem(
      profileIdentifier: map['profileIdentifier'] as String,
      fingerprint: map['fingerprint'] as String,
      profileName: map['profileName'] as String,
    );
  }
}
