part of 'generate_profile_cubit.dart';

abstract class GenerateProfileState extends Equatable {
  const GenerateProfileState();

  @override
  List<Object> get props => [];
}

class GenerateProfileInitial extends GenerateProfileState {}

class ProfileGenerationSuccess extends GenerateProfileState {
  final KeyoxideProfile generatedProfile;

  const ProfileGenerationSuccess(this.generatedProfile);
}

class ProfileGenerationFailure extends GenerateProfileState {
  final KeyoxideError error;

  const ProfileGenerationFailure(this.error);
}

class GetProfileClaimsSuccess extends GenerateProfileState {
  final int claimCount;
  final String queryValue;
  final bool isAddingProfile;
  final FingerprintProtocol protocol;

  const GetProfileClaimsSuccess(this.claimCount, this.queryValue, this.isAddingProfile, this.protocol);
}

class UserViewProfileGenerationSuccess extends GenerateProfileState {
  final KeyoxideProfile generatedProfile;
  const UserViewProfileGenerationSuccess(this.generatedProfile);
}

class UserViewProfileGenerationFailure extends GenerateProfileState {
  final KeyoxideError error;
  const UserViewProfileGenerationFailure(this.error);
}

class UserViewGetProfileClaimsSuccess extends GenerateProfileState {
  final int claimCount;
  final String queryValue;
  final bool isAddingProfile;
  final FingerprintProtocol protocol;

  const UserViewGetProfileClaimsSuccess(this.claimCount, this.queryValue, this.isAddingProfile, this.protocol);
}

class ContactsViewProfileGenerationSuccess extends GenerateProfileState {
  final KeyoxideProfile generatedProfile;
  const ContactsViewProfileGenerationSuccess(this.generatedProfile);
}

class ContactsViewProfileGenerationFailure extends GenerateProfileState {
  final KeyoxideError error;
  const ContactsViewProfileGenerationFailure(this.error);
}

class ContactsViewGetProfileClaimsSuccess extends GenerateProfileState {
  final int claimCount;
  final String queryValue;
  final bool isAddingProfile;
  final FingerprintProtocol protocol;

  const ContactsViewGetProfileClaimsSuccess(this.claimCount, this.queryValue, this.isAddingProfile, this.protocol);
}

class ProfileLoading extends GenerateProfileState {}
class UserViewProfileLoading extends GenerateProfileState {}
class ContactsViewProfileLoading extends GenerateProfileState {}
class OpenSavedProfile extends GenerateProfileState {}
