import 'dart:typed_data';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:keyoxide_flutter/common/constants/keyoxide_enums.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/common/utils/utilities.dart';
import 'package:keyoxide_flutter/features/generate_profile/model/keyoxide_profile.dart';
import 'package:keyoxide_flutter/features/generate_profile/model/search_history_item.dart';
import 'package:keyoxide_flutter/features/user/model/pgp_profile_model.dart';

import '../../../common/models/keyoxide_result.dart';
import '../../../common/services/locator_service.dart';
import '../../user/cubit/user_cubit.dart';
import '../repository/keyoxide_repository.dart';

part 'generate_profile_state.dart';

enum GenerateProfileFor {
  userView,
  displayProfileView,
  contactsView,
  aspProfileModel
}

class GenerateProfileCubit extends Cubit<GenerateProfileState> {
  GenerateProfileCubit() : super(GenerateProfileInitial());
  KeyoxideProfile generatedProfile = KeyoxideProfile();
  Uint8List generatedProfileImage = Uint8List(0);

  Future<void> openSavedProfile(KeyoxideProfile profile, Uint8List avatarImage) async {
    emit(ProfileLoading());
    generatedProfile = profile;
    generatedProfileImage = avatarImage;
    emit(OpenSavedProfile());
  }

  // isAdding is for the case where we just view a profile or adding a profile
  Future<void> generateProfile(String fingerprint,
      GenerateProfileFor generateProfileFor, bool isAddingProfile,
      {FingerprintProtocol? protocol}) async {
    KeyoxideResult result = await getIt<KeyoxideRepository>()
        .generateProfile(fingerprint, protocol: protocol);

    if (result.error != null) {
      emit((generateProfileFor == GenerateProfileFor.userView || generateProfileFor == GenerateProfileFor.aspProfileModel)
          ? UserViewProfileGenerationFailure(result.error!)
          : generateProfileFor == GenerateProfileFor.contactsView
              ? ContactsViewProfileGenerationFailure(result.error!)
              : ProfileGenerationFailure(result.error!));
    } else {
      debugPrint("Profile generation success!");

      generatedProfile = result.data;

      FingerprintProtocol protocol = FingerprintProtocol.fromString(
          generatedProfile.publicKey!.fetch!.method.toString());

      String imageUrl = generatedProfile.personas![generatedProfile.primaryPersonaIndex!].avatarUrl.toString();

      generatedProfileImage = await Utilities.urlImageToUInt8List(protocol == FingerprintProtocol.aspe
          ? imageUrl.replaceAll('svg', 'png')
          : imageUrl);

      if (generateProfileFor == GenerateProfileFor.userView &&
          isAddingProfile) {
        // If the profile is not PGP, show 'Not a PGP profile' snackbar.
        if (protocol != FingerprintProtocol.hkp &&
            protocol != FingerprintProtocol.wkd) {
          emit(UserViewProfileGenerationFailure(KeyoxideError(
              code: 4243, message: 'user_profile_add_pgp_error'.localize)));
          return;
        }
        bool isProfileAlreadySaved = getIt<UserCubit>()
            .isProfileAlreadyInUserProfiles(pgpProfile: generatedProfile);
        if (!isProfileAlreadySaved) {
          emit(UserViewProfileGenerationSuccess(generatedProfile));
        } else {
          emit(UserViewProfileGenerationFailure(KeyoxideError(
              code: 4242, message: 'user_profile_duplicate'.localize)));
        }
      } else if (generateProfileFor == GenerateProfileFor.contactsView) {
        emit(ContactsViewProfileGenerationSuccess(generatedProfile));
      } else if (generateProfileFor == GenerateProfileFor.aspProfileModel) {
        // Do nothing and get the saved profile
      }

      else {
        SearchHistoryItem searchHistoryItem = SearchHistoryItem(
            profileIdentifier: generatedProfile.publicKey!.fetch!.query!,
            fingerprint: generatedProfile.identifier!,
            profileName: generatedProfile.personas![generatedProfile.primaryPersonaIndex!].name.toString());

        await getIt<UserCubit>().updateSearchHistory(searchHistoryItem: searchHistoryItem, isDeleting: false);
        emit(ProfileGenerationSuccess(generatedProfile));
      }
    }
  }

  Future<ProfileModel?> refreshPgpProfile(ProfileModel pgpProfile) async {
    KeyoxideResult result = await getIt<KeyoxideRepository>()
        .generateProfile(pgpProfile.identifier);
    if (result.data != null) {
      return await Utilities.keyoxideProfileToProfileModel(result.data);
    } else {
      return null;
    }
  }

  Future<void> getProfileClaimCount(String fingerprint,
      GenerateProfileFor generateProfileFor, bool isAddingProfile,
      {FingerprintProtocol? protocol}) async {
    emit(generateProfileFor == GenerateProfileFor.userView
        ? UserViewProfileLoading()
        : generateProfileFor == GenerateProfileFor.contactsView
            ? ContactsViewProfileLoading()
            : ProfileLoading());

    KeyoxideResult result = await getIt<KeyoxideRepository>()
        .getProfileClaimCount(fingerprint, protocol: protocol);

    if (result.error != null) {
      emit(ProfileGenerationFailure(result.error!));
    } else {
      debugPrint("Profile claim count success!");
      KeyoxideProfile profile = result.data;
      int claimCount = Utilities.getClaimCountFromProfile(profile);
      emit(generateProfileFor == GenerateProfileFor.userView
          ? UserViewGetProfileClaimsSuccess(
              claimCount,
              fingerprint,
              isAddingProfile,
              FingerprintProtocol.fromString(profile.publicKey!.fetch!.method!))
          : generateProfileFor == GenerateProfileFor.contactsView
              ? ContactsViewGetProfileClaimsSuccess(
                  claimCount,
                  fingerprint,
                  isAddingProfile,
                  FingerprintProtocol.fromString(
                      profile.publicKey!.fetch!.method!))
              : GetProfileClaimsSuccess(
                  claimCount,
                  fingerprint,
                  isAddingProfile,
                  FingerprintProtocol.fromString(
                      profile.publicKey!.fetch!.method!)));
    }
  }

  void disposeGeneratedProfile() {
    generatedProfile = KeyoxideProfile();
  }
}
