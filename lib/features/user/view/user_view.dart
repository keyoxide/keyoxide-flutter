import 'package:aspe/aspe.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:go_router/go_router.dart';
import 'package:internet_connection_checker_plus/internet_connection_checker_plus.dart';
import 'package:keyoxide_flutter/common/constants/keyoxide_constants.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/common/widgets/claim_count_loading_overlay.dart';
import 'package:keyoxide_flutter/features/create_edit_asp_profile/model/asp_profile_model.dart';
import 'package:keyoxide_flutter/features/create_edit_asp_profile/widget/load_asp_profile_dialog.dart';
import 'package:keyoxide_flutter/features/generate_profile/model/keyoxide_profile.dart';
import 'package:keyoxide_flutter/features/settings/cubit/settings_cubit.dart';
import 'package:keyoxide_flutter/features/user/model/pgp_profile_model.dart';
import 'package:keyoxide_flutter/features/user/widgets/identifier_entry_dialog.dart';
import 'package:keyoxide_flutter/features/user/widgets/pgp_profile_card.dart';

import '../../../common/services/locator_service.dart';
import '../../../common/utils/utilities.dart';
import '../../../common/widgets/keyoxide_snackbar.dart';
import '../../create_edit_asp_profile/view/create_asp_profile_dialog.dart';
import '../../generate_profile/cubit/generate_profile_cubit.dart';
import '../cubit/user_cubit.dart';
import '../widgets/asp_profile_card.dart';

class UserView extends StatefulWidget {
  const UserView({super.key});

  @override
  State<UserView> createState() => _UserViewState();
}

class _UserViewState extends State<UserView> {
  List<ProfileModel> pgpProfiles = [];
  List<AspProfileModel> aspProfiles = [];
  bool _isLoading = false;
  bool _newProfileLoading = false;
  String? queryValue = '';
  late bool _showHiddenProfiles;

  Future<AspProfileModel> _createNewAspProfile() async {
    String userDomain =
        getIt<UserCubit>().keyoxideUser.userSettings.verificationApiDomain;
    AspProfile newAspProfile = AspProfile.create(userDomain);
    AspProfileModel newProfileModel = AspProfileModel(
      avatarImage: await Utilities.urlImageToUInt8List(
          newAspProfile.avatarUrl().replaceAll('svg', 'png')),
      aspProfile: newAspProfile,
      isHidden: false,
      updatedAt: DateTime.now(),
      keyoxideProfile: KeyoxideProfile(),
    );
    return newProfileModel;
  }

  void _handleGenerateProfileCubitBuilderStates(GenerateProfileState state) {
    if (state is UserViewProfileLoading ||
        state is UserViewGetProfileClaimsSuccess) {
      _isLoading = true;
    } else {
      _isLoading = false;
    }
  }

  Future<void> _handleGenerateProfileCubitListenerStates(
      GenerateProfileState state) async {
    if (state is UserViewProfileGenerationFailure) {
      String errorMessage = state.error.message.toString();
      KeyoxideSnackBar().showOverlaySnackBar(context, errorMessage);
    } else if (state is OpenSavedProfile) {
      Future.delayed(Duration.zero, () async {
        context.push(displayProfilePath);
      });
    } else if (state is UserViewGetProfileClaimsSuccess) {
      getIt<GenerateProfileCubit>().generateProfile(
          state.queryValue, GenerateProfileFor.userView, state.isAddingProfile,
          protocol: state.protocol);
    } else if (state is UserViewProfileGenerationSuccess) {
      KeyoxideProfile profile = state.generatedProfile;
      getIt<UserCubit>().saveUserProfile(
          pgpProfile: await Utilities.keyoxideProfileToProfileModel(profile));
    }
  }

  @override
  void initState() {
    _showHiddenProfiles =
        getIt<UserCubit>().keyoxideUser.userSettings.showHiddenProfiles;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<UserCubit, UserState>(
      bloc: getIt<UserCubit>(),
      listener: (context, state) {
        if (state is SaveUserProfileSuccess) {
          KeyoxideSnackBar().showOverlaySnackBar(
              context, 'user_profile_save_success'.localize);
        }
        if (state is DeleteUserProfileSuccess) {
          KeyoxideSnackBar().showOverlaySnackBar(
              context, 'user_profile_delete_success'.localize);
        }
      },
      builder: (context, state) {
        if (state is UserLoading) {
          _isLoading = true;
        } else {
          _isLoading = false;
        }
        if (state is UserLoaded) {
          pgpProfiles = state.keyoxideUser.pgpProfiles;
          aspProfiles = state.keyoxideUser.aspProfiles;
        }
        if (state is SaveUserProfileSuccess) {
          pgpProfiles = state.pgpProfiles;
          aspProfiles = state.aspProfiles;
        }
        if (state is UserProfileRefreshSuccess) {
          pgpProfiles = state.pgpProfiles;
          aspProfiles = state.aspProfiles;
        }
        if (state is HideUserProfileSuccess) {
          pgpProfiles = state.pgpProfiles;
          aspProfiles = state.aspProfiles;
        }
        return BlocConsumer<GenerateProfileCubit, GenerateProfileState>(
          bloc: getIt<GenerateProfileCubit>(),
          listener: (context, state) async {
            _handleGenerateProfileCubitListenerStates(state);
          },
          builder: (context, state) {
            _handleGenerateProfileCubitBuilderStates(state);
            return BlocBuilder<SettingsCubit, SettingsState>(
              bloc: getIt<SettingsCubit>(),
              builder: (context, state) {
                if (state is ShowHiddenProfiles) {
                  _showHiddenProfiles = state.showHiddenProfiles;
                }
                return Scaffold(
                  body: Stack(children: [
                    Container(
                      child: pgpProfiles.isEmpty && aspProfiles.isEmpty
                          ? _buildEmptyProfilesView()
                          : _buildProfilesGrid(),
                    ),
                    if (_isLoading) const ClaimCountLoadingOverlay(),
                    if (_newProfileLoading)
                      const Center(
                        child: CircularProgressIndicator(),
                      ),
                  ]),
                  floatingActionButtonLocation:
                      FloatingActionButtonLocation.endFloat,
                  floatingActionButton: _buildSpeedDial(),
                );
              },
            );
          },
        );
      },
    );
  }

  Widget _buildEmptyProfilesView() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Text(
          'user_profile_empty'.localize,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  Widget _buildProfilesGrid() {
    return Column(
      children: [
        if (!getIt<UserCubit>()
            .keyoxideUser
            .userSettings
            .isLocalAuthenticationEnabled)
          GestureDetector(
              onTap: () {
                context.push(settingsPath);
              },
              child: Padding(
                padding: const EdgeInsets.only(left: 4.0, right: 4.0),
                child: Card(
                    child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                      width: double.infinity,
                      child: Text(
                        'user_view_auth_warning'.localize,
                        textAlign: TextAlign.center,
                        style: const TextStyle(color: Colors.red),
                      )),
                )),
              )),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5.0),
            child: GridView.count(
                crossAxisSpacing: 5,
                crossAxisCount: 2,
                mainAxisSpacing: 5,
                children: [
                  ...aspProfiles.map((aspProfile) => checkForProfileVisibility(
                      aspProfile.isHidden,
                      AspProfileCard(
                        userProfile: aspProfile,
                        isDialog: false,
                      ))),
                  ...pgpProfiles.map((profile) => checkForProfileVisibility(
                      profile.isHidden,
                      PgpProfileCard(
                        userProfile: profile,
                        isDialog: false,
                      ))),
                ].whereType<Widget>().toList()),
          ),
        ),
      ],
    );
  }

  Widget? checkForProfileVisibility(bool isProfileHidden, Widget profileCard) {
    if (_showHiddenProfiles) {
      return profileCard;
    } else {
      if (!isProfileHidden) {
        return profileCard;
      } else {
        return null;
      }
    }
  }

  Future<bool> hasInternet() async {
    bool hasInternet = await InternetConnection().hasInternetAccess;
    return hasInternet;
  }

  SpeedDial _buildSpeedDial() {
    return SpeedDial(
        icon: Icons.menu_outlined,
        activeIcon: Icons.close,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        renderOverlay: true,
        closeDialOnPop: true,
        overlayOpacity: 0.0,
        animationCurve: Curves.easeInOut,
        children: [
          SpeedDialChild(
            child: const Icon(
              Icons.person_pin_outlined,
            ),
            label: 'user_view_speed_dial_btn_pgp_load'.localize,
            onTap: () async {
              if (!await hasInternet()) {
                if (!mounted) return;
                KeyoxideSnackBar().showOverlaySnackBar(
                    context, 'dio_exception_connection_error'.localize);
              } else {
                queryValue = await showDialog(
                    context: context,
                    builder: (builder) {
                      return const IdentifierEntryDialog();
                    });
                if (queryValue != null && queryValue!.isNotEmpty) {
                  getIt<GenerateProfileCubit>().getProfileClaimCount(
                      queryValue!, GenerateProfileFor.userView, true);
                }
              }
            },
          ),
          SpeedDialChild(
            child: const Icon(Icons.person_add_alt),
            label: 'user_view_speed_dial_btn_asp_load'.localize,
            onTap: () async {
              if (!await hasInternet()) {
                if (!mounted) return;
                KeyoxideSnackBar().showOverlaySnackBar(
                    context, 'dio_exception_connection_error'.localize);
              } else {
                if (!mounted) return;
                showDialog(
                    context: context,
                    builder: (builder) {
                      return const LoadAspProfileDialog();
                    });
              }
            },
          ),
          SpeedDialChild(
            child: const Icon(Icons.person_add_alt),
            label: 'user_view_speed_dial_btn_asp_create'.localize,
            onTap: () async {
              if (!await hasInternet()) {
                if (!mounted) return;
                KeyoxideSnackBar().showOverlaySnackBar(
                    context, 'dio_exception_connection_error'.localize);
              } else {
                setState(() {
                  _newProfileLoading = true;
                });
                AspProfileModel newProfileModel = await _createNewAspProfile();
                setState(() {
                  _newProfileLoading = false;
                });
                if (!mounted) return;
                showDialog(
                    barrierDismissible: false,
                    context: context,
                    builder: (builder) {
                      return CreateASPProfileDialog(
                          profileModel: newProfileModel, isProfileNew: true);
                    });
              }
            },
          ),
        ]);
  }
}
