import 'package:aspe/aspe.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:internet_connection_checker_plus/internet_connection_checker_plus.dart';
import 'package:keyoxide_flutter/common/constants/keyoxide_constants.dart';
import 'package:keyoxide_flutter/common/constants/keyoxide_enums.dart';
import 'package:keyoxide_flutter/common/services/locator_service.dart';
import 'package:keyoxide_flutter/features/generate_profile/model/keyoxide_profile.dart';
import 'package:keyoxide_flutter/features/generate_profile/model/search_history_item.dart';
import 'package:keyoxide_flutter/features/settings/model/user_settings_model.dart';
import 'package:keyoxide_flutter/features/user/model/pgp_profile_model.dart';

import '../../../app/app_config.dart';
import '../../../common/models/keyoxide_result.dart';
import '../../create_edit_asp_profile/cubit/asp_cubit.dart';
import '../../create_edit_asp_profile/model/asp_profile_model.dart';
import '../../generate_profile/cubit/generate_profile_cubit.dart';
import '../../settings/model/server_status_model.dart';
import '../../settings/widget/language_selection_widget.dart';
import '../model/keyoxide_user.dart';

part 'user_state.dart';

class UserCubit extends Cubit<UserState> with HydratedMixin {
  UserCubit()
      : super(UserInitial(
            user: KeyoxideUser(
          pgpProfiles: [],
          aspProfiles: [],
          userSettings: UserSettingsModel(
            isOnboardingComplete: false,
            isTutorialComplete: false,
            isLocalAuthenticationEnabled: false,
            themeMode: systemModeKey,
            font: FontConsts.defaultFont,
            seedColor: colorToHex(ColorConsts.defaultSeedColor),
            verificationApiDomain: AppConfig().isDev
                ? APIConstants.developmentBaseUrl
                : APIConstants.releaseBaseUrl,
            aspeApiDomain: AppConfig().isDev
                ? APIConstants.developmentBaseUrl
                : APIConstants.releaseBaseUrl,
            language: Languages.english.name,
            showHiddenProfiles: false,
            disableServerUpdateCheck: false,
            disableAnimations: false,
            verificationServerStatus: ServerStatusModel(
              latestServerVersion: '',
              userServerVersion: '',
              isUserServerUpToDate: false,
              serverCheckError: true,
              lastCheckedAt: DateTime.now(),
            ),
            aspeServerStatus: ServerStatusModel(
              latestServerVersion: '',
              userServerVersion: '',
              isUserServerUpToDate: false,
              serverCheckError: true,
              lastCheckedAt: DateTime.now(),
            ),
          ),
          contacts: [],
          searchHistory: [],
        ))) {
    hydrate();
  }

  @override
  UserState? fromJson(Map<String, dynamic> json) {
    try {
      keyoxideUser =
          KeyoxideUser.fromJson(json['keyoxideUser'] as Map<String, dynamic>);
      return UserLoaded(keyoxideUser);
    } catch (e) {
      return null;
    }
  }

  @override
  Map<String, dynamic>? toJson(UserState state) {
    if (state is UserLoaded) {
      return state.toJson();
    } else {
      return null;
    }
  }

  KeyoxideUser keyoxideUser = KeyoxideUser(
    pgpProfiles: [],
    aspProfiles: [],
    userSettings: UserSettingsModel(
      isOnboardingComplete: false,
      isTutorialComplete: false,
      isLocalAuthenticationEnabled: false,
      themeMode: systemModeKey,
      seedColor: colorToHex(ColorConsts.defaultSeedColor),
      font: FontConsts.defaultFont,
      verificationApiDomain: AppConfig().isDev
          ? APIConstants.developmentBaseUrl
          : APIConstants.releaseBaseUrl,
      aspeApiDomain: AppConfig().isDev
          ? APIConstants.developmentBaseUrl
          : APIConstants.releaseBaseUrl,
      language: Languages.english.name,
      showHiddenProfiles: false,
      disableServerUpdateCheck: false,
      disableAnimations: false,
      verificationServerStatus: ServerStatusModel(
        latestServerVersion: '',
        userServerVersion: '',
        isUserServerUpToDate: false,
        serverCheckError: true,
        lastCheckedAt: DateTime.now(),
      ),
      aspeServerStatus: ServerStatusModel(
        latestServerVersion: '',
        userServerVersion: '',
        isUserServerUpToDate: false,
        serverCheckError: true,
        lastCheckedAt: DateTime.now(),
      ),
    ),
    contacts: [],
    searchHistory: [],
  );

  Future<void> updateKeyoxideUser(KeyoxideUser updatedUser) async {
    emit(UserLoading());
    keyoxideUser = updatedUser;
    emit(UserLoaded(keyoxideUser));
  }

  Future<void> deleteUserProfile(
      {ProfileModel? pgpProfile, AspProfileModel? aspProfile}) async {
    emit(UserLoading());
    if (pgpProfile != null) {
      List<ProfileModel> newProfiles = keyoxideUser.pgpProfiles;
      newProfiles.removeWhere((profile) => profile == pgpProfile);
      keyoxideUser.pgpProfiles = newProfiles;
      updateKeyoxideUser(keyoxideUser);
      emit(DeleteUserProfileSuccess(deletedPgpProfile: pgpProfile));
    }
    if (aspProfile != null) {
      List<AspProfileModel> newProfiles = keyoxideUser.aspProfiles;
      newProfiles.removeWhere((profile) => profile == aspProfile);
      keyoxideUser.aspProfiles = newProfiles;
      updateKeyoxideUser(keyoxideUser);
      emit(DeleteUserProfileSuccess(deletedAspProfile: aspProfile));
    }
  }

  Future<void> saveUserProfile(
      {ProfileModel? pgpProfile, AspProfileModel? aspProfile}) async {
    emit(UserLoading());
    if (pgpProfile != null) {
      bool isProfileExists = keyoxideUser.pgpProfiles
          .any((element) => element.identifier == pgpProfile.identifier);
      if (isProfileExists) {
        keyoxideUser.pgpProfiles.removeWhere(
            (profile) => profile.identifier == pgpProfile.identifier);
      }
      keyoxideUser.pgpProfiles.add(pgpProfile);
      updateKeyoxideUser(keyoxideUser);
      emit(SaveUserProfileSuccess(
          pgpProfiles: keyoxideUser.pgpProfiles,
          aspProfiles: keyoxideUser.aspProfiles));
    }
    if (aspProfile != null) {
      bool isProfileExists = getIt<UserCubit>()
          .isProfileAlreadyInUserProfiles(aspProfile: aspProfile.aspProfile);
      if (isProfileExists) {
        keyoxideUser.aspProfiles.removeWhere((profile) =>
            profile.aspProfile.fingerprint ==
            aspProfile.aspProfile.fingerprint);
      }
      keyoxideUser.aspProfiles.add(aspProfile);
      updateKeyoxideUser(keyoxideUser);
      emit(SaveUserProfileSuccess(
          pgpProfiles: keyoxideUser.pgpProfiles,
          aspProfiles: keyoxideUser.aspProfiles));
    }
  }

  bool isProfileAlreadyInUserProfiles(
      {KeyoxideProfile? pgpProfile, AspProfile? aspProfile}) {
    bool isProfileAlreadySaved = aspProfile != null
        ? keyoxideUser.aspProfiles.any((profile) =>
            profile.aspProfile.fingerprint == aspProfile.fingerprint)
        : keyoxideUser.pgpProfiles
            .any((profile) => profile.identifier == pgpProfile?.identifier);
    return isProfileAlreadySaved;
  }

  bool isContactAlreadySaved(KeyoxideProfile newContact) {
    return keyoxideUser.contacts
        .any((contact) => contact.identifier == newContact.identifier);
  }

  Future<void> viewUserProfile(ProfileModel userProfile) async {
    emit(UserLoading());
    String subLink = userProfile.protocol == FingerprintProtocol.wkd
        ? userProfile.primaryEmail
        : userProfile.protocol == FingerprintProtocol.aspe
            ? userProfile.identifier
            : userProfile.fingerprint;
    await getIt<GenerateProfileCubit>()
        .generateProfile(subLink, GenerateProfileFor.displayProfileView, false);
    emit(GetUserProfileSuccess(user: userProfile));
  }

  Future<void> hideProfile(
      {AspProfileModel? aspProfile, ProfileModel? pgpProfile}) async {
    emit(UserLoading());
    if (pgpProfile != null) {
      pgpProfile.isHidden = !pgpProfile.isHidden;
      List<ProfileModel> pgpProfiles = keyoxideUser.pgpProfiles;
      pgpProfiles.removeWhere(
          (profile) => profile.fingerprint == pgpProfile.fingerprint);
      pgpProfiles.add(pgpProfile);
      keyoxideUser.pgpProfiles = pgpProfiles;
      updateKeyoxideUser(keyoxideUser);
      emit(HideUserProfileSuccess(
          aspProfiles: keyoxideUser.aspProfiles,
          pgpProfiles: keyoxideUser.pgpProfiles));
    }
    if (aspProfile != null) {
      aspProfile.isHidden = !aspProfile.isHidden;
      List<AspProfileModel> aspProfiles = keyoxideUser.aspProfiles;
      aspProfiles.removeWhere((profile) =>
          profile.aspProfile.fingerprint == aspProfile.aspProfile.fingerprint);
      aspProfiles.add(aspProfile);
      keyoxideUser.aspProfiles = aspProfiles;
      updateKeyoxideUser(keyoxideUser);
      emit(HideUserProfileSuccess(
          aspProfiles: keyoxideUser.aspProfiles,
          pgpProfiles: keyoxideUser.pgpProfiles));
    }
  }

  Future<void> refreshProfile(
      {AspProfileModel? aspProfile, ProfileModel? pgpProfile}) async {
    emit(UserProfileRefreshing());
    bool hasInternet = await InternetConnection().hasInternetAccess;
    if (!hasInternet) {
      emit(const UserProfileRefreshFailed(
          errorMessage: 'dio_exception_connection_error'));
    } else {
      if (pgpProfile != null) {
        ProfileModel? refreshedPgpProfile =
            await getIt<GenerateProfileCubit>().refreshPgpProfile(pgpProfile);

        if (refreshedPgpProfile != null) {
          int profileIndex = keyoxideUser.pgpProfiles.indexOf(pgpProfile);
          keyoxideUser.pgpProfiles[profileIndex] = refreshedPgpProfile;
          updateKeyoxideUser(keyoxideUser);
          emit(UserProfileRefreshSuccess(
              pgpProfiles: keyoxideUser.pgpProfiles,
              aspProfiles: keyoxideUser.aspProfiles));
          return;
        }
      }
      if (aspProfile != null) {
        try {
          AspProfileModel? refreshedAspProfile =
              await getIt<AspCubit>().refreshProfile(aspProfile);

          if (refreshedAspProfile != null) {
            int profileIndex = keyoxideUser.aspProfiles.indexOf(aspProfile);
            keyoxideUser.aspProfiles[profileIndex] = refreshedAspProfile;
            updateKeyoxideUser(keyoxideUser);
            emit(UserProfileRefreshSuccess(
                pgpProfiles: keyoxideUser.pgpProfiles,
                aspProfiles: keyoxideUser.aspProfiles));
            return;
          }
        } on Exception {
          emit(const UserProfileRefreshFailed(
              errorMessage: 'user_profile_refresh_failure'));
        }
      }
      emit(const UserProfileRefreshFailed(
          errorMessage: 'user_profile_refresh_failure'));
    }
  }

  Future<bool> saveContact(ProfileModel contact) async {
    bool isAlreadyInContacts = isContactAlreadySaved(contact.profile);
    if (isAlreadyInContacts) {
      return false;
    } else {
      keyoxideUser.contacts.add(contact);
      await updateKeyoxideUser(keyoxideUser);
      return true;
    }
  }

  Future<void> deleteContact(ProfileModel contact) async {
    keyoxideUser.contacts.remove(contact);
    await updateKeyoxideUser(keyoxideUser);
  }

  Future<int> importNonExistingContacts(List<ProfileModel> newContacts) async {
    int importedCount = 0;
    for (var newContact in newContacts) {
      bool isAlreadyInContacts = keyoxideUser.contacts
          .any((contact) => contact.identifier == newContact.identifier);
      if (!isAlreadyInContacts) {
        keyoxideUser.contacts.add(newContact);
        importedCount++;
      }
    }
    await updateKeyoxideUser(keyoxideUser);
    return importedCount;
  }

  Future<void> updateSearchHistory(
      {required SearchHistoryItem searchHistoryItem,
      required bool isDeleting}) async {
    if (isDeleting) {
      keyoxideUser.searchHistory.removeWhere(
          (element) => element.fingerprint == searchHistoryItem.fingerprint);
    } else {
      if (keyoxideUser.searchHistory.any(
          (element) => element.fingerprint == searchHistoryItem.fingerprint)) {
        return;
      } else {
        keyoxideUser.searchHistory.add(searchHistoryItem);
      }
    }
    await updateKeyoxideUser(keyoxideUser);
  }

  List<SearchHistoryItem> searchHistory(String searchQuery) {
    List<SearchHistoryItem> matchingSearchHistoryItems = [];
    for (var searchHistoryItem in keyoxideUser.searchHistory) {
      if (searchHistoryItem.profileIdentifier.contains(searchQuery) ||
          searchHistoryItem.profileName.contains(searchQuery)) {
        matchingSearchHistoryItems.add(searchHistoryItem);
      }
    }
    return matchingSearchHistoryItems;
  }
}
