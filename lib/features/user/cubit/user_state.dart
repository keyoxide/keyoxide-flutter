part of 'user_cubit.dart';

abstract class UserState extends Equatable {
  const UserState();

  @override
  List<Object> get props => [];

  Map<String, dynamic>? toJson() {
    return null;
  }
}

class UserInitial extends UserState {
  final KeyoxideUser? user;

  const UserInitial({required this.user});
}

class UserLoading extends UserState {}

class GetUserProfileSuccess extends UserState {
  final ProfileModel user;

  const GetUserProfileSuccess({required this.user});
}

class GetUserProfileFailed extends UserState {
  final KeyoxideError error;

  const GetUserProfileFailed({required this.error});
}

class DeleteUserProfileSuccess extends UserState {
  final ProfileModel? deletedPgpProfile;
  final AspProfileModel? deletedAspProfile;

  const DeleteUserProfileSuccess(
      {this.deletedPgpProfile, this.deletedAspProfile});
}

class DeleteUserProfileFailed extends UserState {
  final ProfileModel? deletedPgpProfile;
  final AspProfileModel? deletedAspProfile;
  final AspProfileDeletionType deleteOperation;

  const DeleteUserProfileFailed(
      {this.deletedPgpProfile,
      this.deletedAspProfile,
      required this.deleteOperation});
}

class SaveUserProfileSuccess extends UserState {
  final List<ProfileModel> pgpProfiles;
  final List<AspProfileModel> aspProfiles;

  const SaveUserProfileSuccess(
      {required this.pgpProfiles, required this.aspProfiles});
}

class SaveUserProfileFailed extends UserState {
  final ProfileModel? pgpProfile;
  final AspProfileModel? aspProfile;

  const SaveUserProfileFailed({this.pgpProfile, this.aspProfile});
}

class UserLoaded extends UserState {
  const UserLoaded(this.keyoxideUser);

  final KeyoxideUser keyoxideUser;

  Map<String, dynamic> toJson() {
    return {'keyoxideUser': keyoxideUser.toJson()};
  }
}

class UserProfileRefreshing extends UserState {}

class UserProfileRefreshSuccess extends UserState {
  final List<AspProfileModel> aspProfiles;
  final List<ProfileModel> pgpProfiles;

  const UserProfileRefreshSuccess(
      {required this.pgpProfiles, required this.aspProfiles});
}

class UserProfileRefreshFailed extends UserState {
  final String errorMessage;
  const UserProfileRefreshFailed({required this.errorMessage});
}

class HideUserProfileSuccess extends UserState {
  final List<AspProfileModel> aspProfiles;
  final List<ProfileModel> pgpProfiles;

  const HideUserProfileSuccess(
      {required this.pgpProfiles, required this.aspProfiles});
}
