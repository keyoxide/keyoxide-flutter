import 'dart:convert';
import 'dart:typed_data';

import 'package:equatable/equatable.dart';
import 'package:keyoxide_flutter/common/constants/keyoxide_enums.dart';
import 'package:keyoxide_flutter/common/utils/utilities.dart';

import '../../generate_profile/model/keyoxide_profile.dart';

class ProfileModel extends Equatable {
  String fingerprint;
  String identifier;
  String primaryEmail;
  FingerprintProtocol protocol;
  String name;
  Uint8List image;
  bool isHidden;
  KeyoxideProfile profile;
  DateTime updatedAt;

  ProfileModel({
    required this.fingerprint,
    required this.identifier,
    required this.primaryEmail,
    required this.protocol,
    required this.name,
    required this.image,
    required this.isHidden,
    required this.profile,
    required this.updatedAt,
  });

  factory ProfileModel.fromJson(Map<String, dynamic> json) {
    return ProfileModel(
      fingerprint: json['fingerprint'],
      identifier: json['identifier'],
      primaryEmail: json['primaryEmail'],
      protocol: FingerprintProtocol.values.firstWhere(
        (element) => element.name == json['protocol'],
        orElse: () => FingerprintProtocol.none,
      ),
      name: json['name'],
      image: base64Decode(json['image']),
      isHidden: json['isHidden'] ?? false,
      profile: KeyoxideProfile.fromJson(json['profile']),
      updatedAt: DateTime.tryParse(json['updatedAt']) ?? DateTime.now(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'fingerprint': fingerprint,
      'identifier': identifier,
      'primaryEmail': primaryEmail,
      'protocol': protocol.name,
      'name': name,
      'image': base64Encode(image),
      'isHidden': isHidden,
      'profile': profile.toJson(),
      'updatedAt': updatedAt.toIso8601String(),
    };
  }

  @override
  List<Object?> get props => [fingerprint, identifier, primaryEmail, protocol, name, image, isHidden, profile, updatedAt];
}
