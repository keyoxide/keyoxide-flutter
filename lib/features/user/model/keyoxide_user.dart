import 'package:keyoxide_flutter/features/generate_profile/model/search_history_item.dart';
import 'package:keyoxide_flutter/features/user/model/pgp_profile_model.dart';

import '../../create_edit_asp_profile/model/asp_profile_model.dart';
import '../../settings/model/user_settings_model.dart';

class KeyoxideUser {
  KeyoxideUser({
    required this.pgpProfiles,
    required this.aspProfiles,
    required this.userSettings,
    required this.contacts,
    required this.searchHistory,
  });

  List<ProfileModel> pgpProfiles;
  List<AspProfileModel> aspProfiles;
  UserSettingsModel userSettings;
  List<ProfileModel> contacts;
  List<SearchHistoryItem> searchHistory;

  factory KeyoxideUser.fromJson(Map<String, dynamic> json) {
    return KeyoxideUser(
      pgpProfiles: List<ProfileModel>.from(json["pgpProfiles"]
          .map((pgpProfile) => ProfileModel.fromJson(pgpProfile))),
      aspProfiles: List<AspProfileModel>.from(json["aspProfiles"]
          .map((aspProfile) => AspProfileModel.fromJson(aspProfile))),
      userSettings: UserSettingsModel.fromJson(json["userSettings"]),
      contacts: List<ProfileModel>.from(
          json["contacts"].map((contact) => ProfileModel.fromJson(contact))),
      searchHistory: json["searchHistory"] == null
          ? []
          : List<SearchHistoryItem>.from(json["searchHistory"].map(
              (searchHistoryItem) =>
                  SearchHistoryItem.fromJson(searchHistoryItem))),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "pgpProfiles":
          List<dynamic>.from(pgpProfiles.map((pgpProfile) => pgpProfile)),
      "aspProfiles":
          List<dynamic>.from(aspProfiles.map((aspProfile) => aspProfile)),
      "userSettings": userSettings.toJson(),
      "contacts": List<dynamic>.from(contacts.map((contact) => contact)),
      "searchHistory": List<dynamic>.from(
          searchHistory.map((searchHistoryItem) => searchHistoryItem)),
    };
  }
}
