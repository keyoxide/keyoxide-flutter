import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:internet_connection_checker_plus/internet_connection_checker_plus.dart';
import 'package:keyoxide_flutter/common/constants/keyoxide_enums.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/common/widgets/keyoxide_snackbar.dart';
import 'package:keyoxide_flutter/features/authentication/cubit/local_authentication_cubit.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../../common/services/locator_service.dart';
import '../../../common/utils/utilities.dart';
import '../../../common/widgets/kx_switch.dart';
import '../../create_edit_asp_profile/model/asp_profile_model.dart';
import '../../create_edit_asp_profile/view/create_asp_profile_dialog.dart';
import '../../generate_profile/cubit/generate_profile_cubit.dart';
import '../cubit/user_cubit.dart';

class AspProfileCard extends StatefulWidget {
  AspProfileCard(
      {super.key, required this.userProfile, required this.isDialog});

  AspProfileModel userProfile;
  final bool isDialog;

  @override
  State<AspProfileCard> createState() => _AspProfileCardState();
}

class _AspProfileCardState extends State<AspProfileCard> {
  bool _isRefreshing = false;

  Widget avatarPic() {
    return Center(
      child: Hero(
        tag: widget.userProfile.aspProfile.fingerprint,
        child: SizedBox(
          height: widget.isDialog ? 120 : 70,
          width: widget.isDialog ? 120 : 70,
          child: CircleAvatar(
            maxRadius: 80,
            backgroundColor: Theme.of(context).cardColor,
            child: Semantics(
              label: 'user_profile_page_user_avatar_image_semantics'.localize,
              child: CircleAvatar(
                  maxRadius: 73,
                  backgroundImage: MemoryImage(widget.userProfile.avatarImage)),
            ),
          ),
        ),
      ),
    );
  }

  Widget name() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        widget.userProfile.aspProfile.name ?? '',
        overflow: TextOverflow.ellipsis,
        maxLines: 1,
        semanticsLabel: 'user_profile_page_user_name_semantics'
            .localizeWithPlaceholders(
                [widget.userProfile.aspProfile.name.toString()]),
        style: Theme.of(context).textTheme.bodyLarge,
      ),
    );
  }

  Widget fingerprint() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        widget.userProfile.aspProfile.fingerprint,
        semanticsLabel: 'user_profile_page_fingerprint_semantics'
            .localizeWithPlaceholders(
                [widget.userProfile.aspProfile.fingerprint]),
        overflow: TextOverflow.ellipsis,
        maxLines: 1,
        style: Theme.of(context).textTheme.bodyMedium,
      ),
    );
  }

  Widget description() {
    String description = widget.userProfile.aspProfile.description ?? '';
    return description == ''
        ? Container()
        : Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              description,
              overflow: TextOverflow.ellipsis,
              maxLines: 3,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyMedium,
            ),
          );
  }

  Widget profileCard() {
    return GestureDetector(
      onTap: () {
        showDialog(
            context: context,
            builder: (builder) {
              return AspProfileCard(
                  userProfile: widget.userProfile, isDialog: true);
            });
      },
      child: Stack(children: [
        Card(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              avatarPic(),
              name(),
              fingerprint(),
            ],
          ),
        ),
        Positioned(
            top: 12,
            left: 12,
            child: Container(
              decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.onPrimary,
                  borderRadius: BorderRadius.circular(5)),
              child: const Text(' ASP '),
            ))
      ]),
    );
  }

  Widget dialogProfileCard() {
    return Stack(children: [
      Dialog(
        insetPadding: const EdgeInsets.all(9),
        child: SingleChildScrollView(
          child: Stack(children: [
            Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  const SizedBox(height: 20),
                  avatarPic(),
                  name(),
                  description(),
                  fingerprint(),
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Theme.of(context).cardColor,
                          border: Border.all(
                              style: BorderStyle.solid,
                              color: Theme.of(context).dividerColor),
                          borderRadius: BorderRadius.circular(21.0)),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: SizedBox(
                          width: 200,
                          height: 200,
                          child: QrImageView(
                            semanticsLabel:
                                'user_profile_page_qr_code_semantics'.localize,
                            backgroundColor: Colors.white,
                            data: widget.userProfile.aspProfile.fingerprint,
                            version: QrVersions.auto,
                            size: 200.0,
                            errorStateBuilder: (cxt, err) {
                              return Center(
                                child: Text(
                                  'display_profile_page_qr_failure'.localize,
                                  textAlign: TextAlign.center,
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 20),
                  KxSwitch(
                    isEnabled: widget.userProfile.isHidden,
                    text: 'user_view_hide_profile'.localize,
                    onChanged: () => getIt<UserCubit>()
                        .hideProfile(aspProfile: widget.userProfile),
                  ),
                  const SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('general_txt_updated_at'.localize),
                      Text(Utilities.formatDateTimeToLocal(
                          context, widget.userProfile.updatedAt)),
                    ],
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      FloatingActionButton(
                          onPressed: () async {
                            context.pop();
                            await getIt<GenerateProfileCubit>()
                                .openSavedProfile(
                                    widget.userProfile.keyoxideProfile,
                                    widget.userProfile.avatarImage);
                            // .getProfileClaimCount(
                            //     widget.userProfile.aspProfile.uri
                            //         .toString(),
                            //     GenerateProfileFor.userView,
                            //     false,
                            //     protocol: FingerprintProtocol.aspe);
                          },
                          elevation: 0,
                          child: const Icon(Icons.open_in_full)),
                      FloatingActionButton(
                          onPressed: () async {
                            await getIt<UserCubit>()
                                .refreshProfile(aspProfile: widget.userProfile);
                          },
                          elevation: 0,
                          child: const Icon(Icons.refresh_outlined)),
                      FloatingActionButton(
                          onPressed: () async {
                            bool isAuthEnabled = getIt<UserCubit>()
                                .keyoxideUser
                                .userSettings
                                .isLocalAuthenticationEnabled;
                            if (!isAuthEnabled) {
                              openProfile();
                            } else {
                              await getIt<LocalAuthenticationCubit>()
                                  .authenticate(LocalAuthFor.authentication);
                            }
                          },
                          elevation: 0,
                          child: const Icon(Icons.mode_edit_outlined)),
                    ],
                  ),
                  const SizedBox(height: 20),
                ]),
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                onPressed: () => context.pop(),
                icon: const Icon(Icons.close_outlined),
              ),
            ),
          ]),
        ),
      ),
      if (_isRefreshing) const Center(child: CircularProgressIndicator()),
    ]);
  }

  Future<void> openProfile() async {
    bool hasInternet = await InternetConnection().hasInternetAccess;
    if (!mounted) return;
    if (!hasInternet) {
      KeyoxideSnackBar().showOverlaySnackBar(
          context, 'dio_exception_connection_error'.localize);
    } else {
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (builder) {
            return CreateASPProfileDialog(profileModel: widget.userProfile);
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.isDialog
        ? BlocConsumer<LocalAuthenticationCubit, LocalAuthenticationState>(
            bloc: getIt<LocalAuthenticationCubit>(),
            listener: (context, state) async {
              if (state is LocalAuthenticationSuccess) {
                openProfile();
              }
              if (state is LocalAuthenticationFailure) {
                KeyoxideSnackBar()
                    .showOverlaySnackBar(context, state.error.message!);
              }
            },
            builder: (context, state) {
              return BlocBuilder<UserCubit, UserState>(
                bloc: getIt<UserCubit>(),
                buildWhen: (previousState, currentState) =>
                    currentState is SaveUserProfileSuccess,
                builder: (context, state) {
                  return BlocConsumer<UserCubit, UserState>(
                    bloc: getIt<UserCubit>(),
                    listener: (context, state) {
                      if (state is UserProfileRefreshSuccess) {
                        KeyoxideSnackBar().showOverlaySnackBar(
                            context, 'user_profile_refresh_success'.localize);
                      }
                      if (state is UserProfileRefreshFailed) {
                        KeyoxideSnackBar().showOverlaySnackBar(
                            context, state.errorMessage.localize);
                      }
                    },
                    builder: (context, state) {
                      if (state is UserProfileRefreshing) {
                        _isRefreshing = true;
                      } else {
                        _isRefreshing = false;
                      }
                      if (state is UserProfileRefreshSuccess) {
                        widget.userProfile = state.aspProfiles.firstWhere(
                            (profile) =>
                                profile.aspProfile.fingerprint ==
                                widget.userProfile.aspProfile.fingerprint);
                      }
                      return dialogProfileCard();
                    },
                  );
                },
              );
            },
          )
        : profileCard();
  }
}
