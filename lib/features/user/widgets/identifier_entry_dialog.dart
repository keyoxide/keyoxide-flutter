import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';

class IdentifierEntryDialog extends StatefulWidget {
  const IdentifierEntryDialog({super.key});

  @override
  State<IdentifierEntryDialog> createState() => _IdentifierEntryDialogState();
}

class _IdentifierEntryDialogState extends State<IdentifierEntryDialog> {
  final formKey = GlobalKey<FormState>();
  final FocusNode _identifierFocusNode = FocusNode();
  final FocusNode _addProfileBtnFocusNode = FocusNode();
  String identifier = '';

  bool _saveForm() {
    if (formKey.currentState != null) {
      final isValid = formKey.currentState!.validate();
      if (isValid) {
        formKey.currentState!.save();
        return true;
      }
    }
    return false;
  }

  void loadProfile() {
    if (_saveForm()) {
      context.pop(identifier);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: const EdgeInsets.all(10),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(height: 10),
            Text('load_dialog_pgp_title'.localize,
                style: Theme.of(context).textTheme.titleLarge),
            const SizedBox(height: 20),
            Form(
                key: formKey,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: TextFormField(
                        maxLines: 2,
                        focusNode: _identifierFocusNode,
                        decoration: InputDecoration(
                          border: const OutlineInputBorder(),
                          hintText: 'load_dialog_pgp_hint'.localize,
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'load_dialog_pgp_error_empty_field'.localize;
                          }
                          return null;
                        },
                        onSaved: (value) {
                          identifier = value!;
                        },
                      ),
                    )
                  ],
                )),
            const SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: FilledButton.icon(
                focusNode: _addProfileBtnFocusNode,
                style: FilledButton.styleFrom(),
                onPressed: () {
                  loadProfile();
                },
                icon: const Icon(Icons.person_search_outlined),
                label: Text(
                  'load_dialog_pgp_btn_add'.localize,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
