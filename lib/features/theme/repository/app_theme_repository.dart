import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:keyoxide_flutter/features/settings/cubit/settings_cubit.dart';

import '../../../common/constants/keyoxide_constants.dart';
import '../../../common/services/locator_service.dart';
import '../../user/cubit/user_cubit.dart';

class AppThemeRepository {
  const AppThemeRepository();

  ThemeMode getAppTheme() {
    String savedAppTheme =
        getIt<UserCubit>().keyoxideUser.userSettings.themeMode;
    ThemeMode appThemeMode = ThemeMode.system;
    switch (savedAppTheme) {
      case darkModeKey:
        appThemeMode = ThemeMode.dark;
        break;
      case lightModeKey:
        appThemeMode = ThemeMode.light;
        break;
      default:
        appThemeMode = ThemeMode.system;
    }
    return appThemeMode;
  }

  Future<void> setAppTheme(ThemeMode themeMode) async {
    String appThemeMode = systemModeKey;

    switch (themeMode) {
      case ThemeMode.light:
        appThemeMode = lightModeKey;
        break;
      case ThemeMode.dark:
        appThemeMode = darkModeKey;
        break;
      default:
        appThemeMode = systemModeKey;
        break;
    }

    await getIt<SettingsCubit>().changeThemeMode(appThemeMode);
  }

  Future<void> setAppThemeColor(Color themeColor) async {
    await getIt<SettingsCubit>().changeSeedColor(colorToHex(themeColor));
  }

  Color getAppThemeColor() {
    String colorString = getIt<UserCubit>().keyoxideUser.userSettings.seedColor;
    return colorFromHex(colorString) ?? ColorConsts.defaultSeedColor;
  }

  Future<void> setFont(String appFont) async {
    await getIt<SettingsCubit>().changeFont(appFont);
  }

  String getFont() {
    String font = getIt<UserCubit>().keyoxideUser.userSettings.font;
    return font;
  }
}
