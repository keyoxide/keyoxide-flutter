import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import '../repository/app_theme_repository.dart';

part 'app_theme_state.dart';

class AppThemeCubit extends Cubit<AppThemeState> {
  AppThemeRepository themeRepo;

  AppThemeCubit({required this.themeRepo})
      : super(CurrentAppTheme(themeMode: themeRepo.getAppTheme()));

  late ThemeMode appTheme;
  late Color seedColor;
  late String font;

  ThemeMode getAppTheme() {
    appTheme = themeRepo.getAppTheme();
    return appTheme;
  }

  Future<void> changeAppTheme(ThemeMode theme) async {
    emit(LoadingAppTheme());
    await themeRepo.setAppTheme(theme);
    appTheme = theme;
    emit(CurrentAppTheme(themeMode: theme));
  }

  Color getSeedColor() {
    seedColor = themeRepo.getAppThemeColor();
    return seedColor;
  }

  Future<void> changeSeedColor(Color color) async {
    emit(LoadingAppTheme());
    await themeRepo.setAppThemeColor(color);
    seedColor = color;
    emit(CurrentAppColor(seedColor: color));
  }

  String getFont() {
    font = themeRepo.getFont();
    return font;
  }

  Future<void> changeFont(String appFont) async {
    emit(LoadingAppTheme());
    await themeRepo.setFont(appFont);
    font = appFont;
    emit(CurrentAppFont(font: appFont));
  }
}
