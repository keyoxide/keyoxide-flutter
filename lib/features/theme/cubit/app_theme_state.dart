part of 'app_theme_cubit.dart';

abstract class AppThemeState extends Equatable {
  const AppThemeState();

  @override
  List<Object?> get props => [];
}

class LoadingAppTheme extends AppThemeState {}

class CurrentAppTheme extends AppThemeState {
  final ThemeMode themeMode;

  const CurrentAppTheme({required this.themeMode});
}

class CurrentAppColor extends AppThemeState {
  final Color seedColor;

  const CurrentAppColor({required this.seedColor});
}

class CurrentAppFont extends AppThemeState {
  final String font;

  const CurrentAppFont({required this.font});
}
