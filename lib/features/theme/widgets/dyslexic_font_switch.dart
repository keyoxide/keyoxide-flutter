import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keyoxide_flutter/common/constants/keyoxide_constants.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';

import '../../../common/services/locator_service.dart';
import '../cubit/app_theme_cubit.dart';

class DyslexicFontSwitch extends StatefulWidget {
  const DyslexicFontSwitch({super.key});

  @override
  State<DyslexicFontSwitch> createState() => _DyslexicFontSwitchState();
}

class _DyslexicFontSwitchState extends State<DyslexicFontSwitch> {
  bool _isDyslexicFontEnabled = false;

  @override
  void initState() {
    _isDyslexicFontEnabled =
        getIt<AppThemeCubit>().getFont() == FontConsts.defaultFont
            ? false
            : true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
            style: BorderStyle.solid, color: Theme.of(context).dividerColor),
        borderRadius: BorderRadius.circular(30),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 17.0),
            child: Text(
              'settings_theming_dyslexic_font'.localize,
              semanticsLabel:
                  'settings_theming_dyslexic_font'.localize,
            ),
          ),
          SizedBox(width: 10),
          BlocBuilder<AppThemeCubit, AppThemeState>(
            bloc: getIt<AppThemeCubit>(),
            buildWhen: (previous, current) => current == CurrentAppFont,
            builder: (context, state) {
              return Padding(
                padding: const EdgeInsets.only(right: 17.0),
                child: Switch(
                    value: _isDyslexicFontEnabled,
                    onChanged: (bool isEnabled) {
                      _isDyslexicFontEnabled = isEnabled;
                      getIt<AppThemeCubit>().changeFont(_isDyslexicFontEnabled
                          ? FontConsts.openDyslexic3
                          : FontConsts.defaultFont);
                    }),
              );
            },
          ),
        ],
      ),
    );
  }
}
