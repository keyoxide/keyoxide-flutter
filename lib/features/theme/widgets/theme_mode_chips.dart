import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../common/services/locator_service.dart';
import '../cubit/app_theme_cubit.dart';

class ThemeModeChips extends StatefulWidget {
  const ThemeModeChips({Key? key}) : super(key: key);

  @override
  State<ThemeModeChips> createState() => _ThemeModeChipsState();
}

class _ThemeModeChipsState extends State<ThemeModeChips> {
  late ThemeMode themeMode = ThemeMode.system;

  @override
  void initState() {
    themeMode = getIt<AppThemeCubit>().appTheme;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppThemeCubit, AppThemeState>(
      bloc: getIt<AppThemeCubit>(),
      buildWhen: (previous, current) => current is CurrentAppTheme,
      builder: (context, state) {
        return SegmentedButton<ThemeMode>(
              showSelectedIcon: false,
                  style: ButtonStyle(visualDensity: VisualDensity.standard),
                  segments: <ButtonSegment<ThemeMode>>[
                    ButtonSegment<ThemeMode>(
                        value: ThemeMode.dark,
                        icon: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Icon(Icons.dark_mode_outlined),
                        )),
                    ButtonSegment<ThemeMode>(
                        value: ThemeMode.system,
                        icon: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Icon(Icons.system_security_update_good_outlined),
                        )),
                    ButtonSegment<ThemeMode>(
                        value: ThemeMode.light,
                        icon: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Icon(Icons.light_mode_outlined),
                        )),
                  ],
                  selected: <ThemeMode>{themeMode},
                  onSelectionChanged: (Set<ThemeMode> newSelection) async {
                    themeMode = newSelection.first;
                    await getIt<AppThemeCubit>().changeAppTheme(themeMode);
                  },

        );

      },
    );
  }
}
