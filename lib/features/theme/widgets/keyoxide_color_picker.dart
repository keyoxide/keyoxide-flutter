import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/features/theme/cubit/app_theme_cubit.dart';
import 'package:keyoxide_flutter/features/theme/repository/app_theme_repository.dart';

import '../../../common/services/locator_service.dart';

class KeyoxideColorPicker extends StatefulWidget {
  const KeyoxideColorPicker({super.key});

  @override
  State<KeyoxideColorPicker> createState() => _KeyoxideColorPickerState();
}

class _KeyoxideColorPickerState extends State<KeyoxideColorPicker> {
  late Color seedColor;
  late Color selectedColor;

  @override
  void initState() {
    super.initState();
    seedColor = getIt<AppThemeRepository>().getAppThemeColor();
    selectedColor = seedColor;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content: SingleChildScrollView(
                  child: BlockPicker(
                    pickerColor: seedColor,
                    onColorChanged: (color) {
                      selectedColor = color;
                    },
                  ),
                ),
                actions: <Widget>[
                  ElevatedButton(
                    child: Text(
                      'general_txt_apply'.localize,
                    ),
                    onPressed: () {
                      seedColor = selectedColor;
                      getIt<AppThemeCubit>().changeSeedColor(seedColor);
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            });
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
              style: BorderStyle.solid, color: Theme.of(context).dividerColor),
          borderRadius: BorderRadius.circular(30),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 3.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 17.0),
                child: Text(
                  'settings_theming_seed_color'.localize,
                  semanticsLabel: 'settings_theming_seed_color_txt_semantics'.localize,
                ),
              ),
              SizedBox(width: 10),
              BlocBuilder<AppThemeCubit, AppThemeState>(
                bloc: getIt<AppThemeCubit>(),
                buildWhen: (previous, current) => current == CurrentAppColor,
                builder: (context, state) {
                  return Semantics(
                    label: 'settings_theming_seed_color_container_semantics'.localize,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 22.0, top: 6, bottom: 6),
                      child: Container(
                        width: 30,
                        height: 30,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border:
                              Border.all(color: Theme.of(context).dividerColor),
                          color: seedColor,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
