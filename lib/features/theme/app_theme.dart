import 'package:flutter/material.dart';

import '../../common/constants/keyoxide_constants.dart';

/// Theming class that provides custom application themes.
class AppTheme {
  // const AppTheme._();
  //
  // static late final ThemeData light;
  // static late final ThemeData dark;

  // static void initialize() {
  //   light = lightTheme();
  //   dark = darkTheme();
  // }

  /// Generated light theme for the app.
  static ThemeData lightTheme(Color seedColor, String font) {
    ColorScheme scheme = ColorScheme.fromSeed(seedColor: seedColor);
    String? selectedFont = font == FontConsts.defaultFont ? null : font;
    ThemeData lightThemeData = ThemeData(
        colorScheme: scheme, useMaterial3: true, fontFamily: selectedFont);

    return lightThemeData.copyWith(
        textTheme: lightThemeData.textTheme.copyWith(
      bodySmall: lightThemeData.textTheme.bodySmall!.copyWith(
        fontSize: 14,
        fontWeight: FontWeight.w400,
        color: ColorConsts.linkTextLight,
        decoration: TextDecoration.underline,
        decorationColor: ColorConsts.linkTextLight,
        letterSpacing: 0.1,
      ),
    ));
    //     .copyWith(
    //   useMaterial3: true,
    //   floatingActionButtonTheme: const FloatingActionButtonThemeData(
    //     backgroundColor: ColorConsts.buttonBackgroundLight,
    //     elevation: 15,
    //   ),
    //   elevatedButtonTheme: ElevatedButtonThemeData(
    //     style: ButtonStyle(
    //         backgroundColor: MaterialStateProperty.all(
    //           ColorConsts.buttonBackgroundLight,
    //         ),
    //         foregroundColor: MaterialStateProperty.all(Colors.white)),
    //   ),
    //   iconTheme: theme.iconTheme.copyWith(
    //     color: Colors.black,
    //   ),
    //   buttonTheme: theme.buttonTheme.copyWith(
    //     buttonColor: ColorConsts.buttonBackgroundLight,
    //   ),
    //   appBarTheme: const AppBarTheme(
    //       backgroundColor: Colors.transparent,
    //       elevation: 0,
    //       iconTheme: IconThemeData(color: Colors.black54)),
    //   textTheme: theme.textTheme.copyWith(
    //     titleLarge: const TextStyle(
    //       fontSize: 18.5,
    //       fontWeight: FontWeight.w600,
    //       color: ColorConsts.headerTextLight,
    //       letterSpacing: 0.1,
    //     ),
    //     headlineSmall: const TextStyle(
    //       fontSize: 16.5,
    //       color: ColorConsts.headerTextLight,
    //     ),
    //     // For the links
    //     bodySmall: theme.textTheme.bodySmall!.copyWith(
    //       fontSize: 17,
    //       fontWeight: FontWeight.w400,
    //       color: ColorConsts.linkTextLight,
    //       decoration: TextDecoration.underline,
    //       letterSpacing: 0.1,
    //     ),
    //     titleSmall: theme.textTheme.bodyLarge?.copyWith(
    //       fontSize: 16,
    //       fontWeight: FontWeight.w400,
    //       color: Colors.black,
    //       letterSpacing: 0.1,
    //     ),
    //     bodyLarge: theme.textTheme.bodyLarge?.copyWith(
    //       fontSize: 15,
    //       fontWeight: FontWeight.w400,
    //       color: ColorConsts.headerTextLight,
    //       letterSpacing: 0.1,
    //     ),
    //     bodyMedium: theme.textTheme.bodyMedium?.copyWith(
    //       fontSize: 15,
    //       fontWeight: FontWeight.w400,
    //       color: Colors.black,
    //       letterSpacing: 0.1,
    //     ),
    //   ),
    //   cardTheme: CardTheme(
    //     elevation: 8,
    //     margin: EdgeInsets.zero,
    //     shape: RoundedRectangleBorder(
    //       borderRadius: BorderRadius.circular(25),
    //     ),
    //   ),
    //   dividerColor: ColorConsts.cardBorderLight,
    //   chipTheme: theme.chipTheme.copyWith(
    //     secondarySelectedColor: ColorConsts.buttonBackgroundLight,
    //     labelStyle: const TextStyle(
    //       color: Colors.black,
    //     ),
    //     secondaryLabelStyle: const TextStyle(color: Colors.white),
    //   ),
    //   inputDecorationTheme: theme.inputDecorationTheme.copyWith(
    //     border: const OutlineInputBorder(
    //       borderSide: BorderSide(
    //         color: ColorConsts.cardBorderLight,
    //       ),
    //     ),
    //     enabledBorder: const OutlineInputBorder(
    //       borderSide: BorderSide(
    //         color: ColorConsts.cardBorderLight,
    //       ),
    //     ),
    //     disabledBorder: const OutlineInputBorder(
    //       borderSide: BorderSide(
    //         color: ColorConsts.cardBorderLight,
    //       ),
    //     ),
    //     floatingLabelStyle: theme.textTheme.bodyMedium!
    //         .copyWith(color: ColorConsts.headerTextLight),
    //     labelStyle: theme.textTheme.bodyMedium!.copyWith(color: Colors.grey),
    //   ),
    //   cardColor: ColorConsts.claimCardBackgroundLight,
    //   scaffoldBackgroundColor: ColorConsts.backgroundLight,
    //   colorScheme: theme.colorScheme
    //       .copyWith(
    //         primary: ColorConsts.buttonBackgroundLight,
    //         secondary: ColorConsts.buttonBackgroundLight,
    //       )
    //       .copyWith(background: ColorConsts.cardBackgroundLight),
    // );
  }

  /// Generated dark theme for the app.
  static ThemeData darkTheme(Color seedColor, String font) {
    ColorScheme scheme =
        ColorScheme.fromSeed(seedColor: seedColor, brightness: Brightness.dark);
    String? selectedFont = font == FontConsts.defaultFont ? null : font;
    ThemeData darkThemeData = ThemeData(
        colorScheme: scheme, useMaterial3: true, fontFamily: selectedFont);
    return darkThemeData.copyWith(
        textTheme: darkThemeData.textTheme.copyWith(
      bodySmall: darkThemeData.textTheme.bodySmall!.copyWith(
        fontSize: 14,
        fontWeight: FontWeight.w400,
        color: ColorConsts.linkTextDark,
        decoration: TextDecoration.underline,
        decorationColor: ColorConsts.linkTextDark,
        letterSpacing: 0.1,
      ),
    ));

    //     .copyWith(
    //   useMaterial3: true,
    //   floatingActionButtonTheme: const FloatingActionButtonThemeData(
    //     elevation: 15,
    //     backgroundColor: ColorConsts.buttonBackgroundDark,
    //   ),
    //   elevatedButtonTheme: ElevatedButtonThemeData(
    //     style: ButtonStyle(
    //         backgroundColor: MaterialStateProperty.all(
    //           ColorConsts.buttonBackgroundDark,
    //         ),
    //         foregroundColor: MaterialStateProperty.all(Colors.black)),
    //   ),
    //   iconTheme: theme.iconTheme.copyWith(
    //     color: Colors.white,
    //   ),
    //   buttonTheme: theme.buttonTheme.copyWith(
    //     buttonColor: ColorConsts.buttonBackgroundDark,
    //   ),
    //   snackBarTheme: theme.snackBarTheme.copyWith(
    //     backgroundColor: Colors.black12,
    //     contentTextStyle: const TextStyle(color: Colors.white),
    //   ),
    //   appBarTheme: const AppBarTheme(
    //     backgroundColor: Colors.transparent,
    //     elevation: 0,
    //     iconTheme: IconThemeData(color: Colors.white70),
    //   ),
    //   textTheme: theme.textTheme.copyWith(
    //     titleLarge: const TextStyle(
    //       fontSize: 18.5,
    //       fontWeight: FontWeight.w600,
    //       color: ColorConsts.headerTextDark,
    //       letterSpacing: 0.1,
    //     ),
    //     headlineSmall: const TextStyle(
    //       fontSize: 16.5,
    //       fontWeight: FontWeight.bold,
    //       color: ColorConsts.headerTextDark,
    //     ),
    //     // For the links
    //     bodySmall: theme.textTheme.bodySmall!.copyWith(
    //       fontSize: 17,
    //       fontWeight: FontWeight.w400,
    //       color: ColorConsts.linkTextDark,
    //       decoration: TextDecoration.underline,
    //       letterSpacing: 0.1,
    //     ),
    //     titleSmall: theme.textTheme.bodyLarge?.copyWith(
    //       fontSize: 16,
    //       fontWeight: FontWeight.w400,
    //       color: Colors.white,
    //       letterSpacing: 0.1,
    //     ),
    //     bodyLarge: theme.textTheme.bodyLarge?.copyWith(
    //       fontSize: 15,
    //       fontWeight: FontWeight.w400,
    //       color: Colors.white54,
    //       letterSpacing: 0.1,
    //     ),
    //     bodyMedium: theme.textTheme.bodyMedium?.copyWith(
    //       fontSize: 15,
    //       fontWeight: FontWeight.w400,
    //       color: Colors.white,
    //       letterSpacing: 0.1,
    //     ),
    //   ),
    //   cardTheme: CardTheme(
    //     elevation: 0,
    //     margin: EdgeInsets.zero,
    //     shape: RoundedRectangleBorder(
    //       borderRadius: BorderRadius.circular(25),
    //     ),
    //   ),
    //   dividerColor: ColorConsts.cardBorderDark,
    //   chipTheme: theme.chipTheme.copyWith(
    //     secondarySelectedColor: ColorConsts.buttonBackgroundDark,
    //     labelStyle: const TextStyle(
    //       color: Colors.white,
    //     ),
    //     secondaryLabelStyle: const TextStyle(color: Colors.black),
    //   ),
    //   inputDecorationTheme: theme.inputDecorationTheme.copyWith(
    //     border: const OutlineInputBorder(
    //       borderSide: BorderSide(
    //         color: ColorConsts.cardBorderDark,
    //       ),
    //     ),
    //     enabledBorder: const OutlineInputBorder(
    //       borderSide: BorderSide(
    //         color: ColorConsts.cardBorderDark,
    //       ),
    //     ),
    //     disabledBorder: const OutlineInputBorder(
    //       borderSide: BorderSide(
    //         color: ColorConsts.cardBorderDark,
    //       ),
    //     ),
    //     floatingLabelStyle: theme.textTheme.bodyMedium!
    //         .copyWith(color: ColorConsts.headerTextDark),
    //     labelStyle: theme.textTheme.bodyMedium!.copyWith(color: Colors.grey),
    //   ),
    //   cardColor: ColorConsts.claimCardBackgroundDark,
    //   scaffoldBackgroundColor: ColorConsts.backgroundDark,
    //   colorScheme: theme.colorScheme
    //       .copyWith(
    //         primary: ColorConsts.buttonBackgroundDark,
    //         secondary: ColorConsts.buttonBackgroundDark,
    //       )
    //       .copyWith(background: ColorConsts.cardBackgroundDark),
    // );
  }
}
