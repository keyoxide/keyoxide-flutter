import 'package:flutter/material.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/features/user/cubit/user_cubit.dart';
import 'dart:io';

import '../../../common/constants/keyoxide_constants.dart';
import '../../../common/services/locator_service.dart';
import '../../../common/widgets/text_to_link.dart';
import '../../generate_profile/model/keyoxide_profile.dart';

// Sliding bottom sheet upon pressing a claim. It shows the additional info about the claim.
class ClaimBottomSheet extends StatefulWidget {
  final Display display;
  final int claimStatus;
  final bool isViaProxy;
  final bool isVerifyCompleted;
  final bool isVerified;
  final bool isAmbiguous;

  const ClaimBottomSheet({
    super.key,
    required this.display,
    required this.claimStatus,
    required this.isViaProxy,
    required this.isVerifyCompleted,
    required this.isVerified,
    required this.isAmbiguous,
  });

  @override
  ClaimBottomSheetState createState() => ClaimBottomSheetState();
}

class ClaimBottomSheetState extends State<ClaimBottomSheet> {
  final bool _isIos = Platform.isIOS;
  String userDomain = '';

  Widget _failedAmbiguousClaimWarning() {
    return Row(
      children: [
        const Icon(Icons.warning),
        const SizedBox(width: 8),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'display_profile_page_error_unverified'.localize,
              overflow: TextOverflow.visible,
              style: Theme.of(context).textTheme.bodyMedium,
            ),
          ),
        ),
      ],
    );
  }

  @override
  void initState() {
    userDomain =
        getIt<UserCubit>().keyoxideUser.userSettings.verificationApiDomain;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 8),
            child: (widget.isAmbiguous && !widget.isVerified)
                ? Column(
                    children: [
                      _failedAmbiguousClaimWarning(),
                    ],
                  )
                : Column(
                    children: [
                      Row(
                        children: [
                          const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Icon(
                              Icons.link,
                            ),
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'display_profile_page_claim_sheet_profile_link'
                                            .localize,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headlineSmall,
                                      ),
                                      const Icon(
                                        Icons.arrow_downward_outlined,
                                        size: 15,
                                      ),
                                    ],
                                  ),
                                ),
                                widget.display.profileUrl == null
                                    ? Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          'display_profile_page_claim_sheet_not_accessible'
                                              .localize,
                                          textAlign: TextAlign.start,
                                        ),
                                      )
                                    : Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: TextToLink(
                                          urlText:
                                              widget.display.profileUrl ?? '',
                                          urlTextStyle: Theme.of(context)
                                              .textTheme
                                              .bodySmall!,
                                          textStyle: Theme.of(context)
                                              .textTheme
                                              .bodyMedium!,
                                        ),
                                      ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'display_profile_page_claim_sheet_proof_link'
                                            .localize,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headlineSmall,
                                      ),
                                      const Icon(
                                        Icons.arrow_downward_outlined,
                                        //          color: ColorConsts.textPurple,
                                        size: 15,
                                      )
                                    ],
                                  ),
                                ),
                                widget.display.proofUrl == null
                                    ? Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          'display_profile_page_claim_sheet_not_accessible'
                                              .localize,
                                          textAlign: TextAlign.start,
                                        ),
                                      )
                                    : Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: TextToLink(
                                          urlText:
                                              widget.display.proofUrl ?? '',
                                          urlTextStyle: Theme.of(context)
                                              .textTheme
                                              .bodySmall!,
                                          textStyle: Theme.of(context)
                                              .textTheme
                                              .bodyMedium!,
                                        ),
                                      ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      const Divider(
                          //         color: ColorConsts.buttonBackground,
                          ),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: widget.isVerifyCompleted
                                ? Icon(
                                    Icons.check,
                                    color: Theme.of(context).brightness ==
                                            Brightness.dark
                                        ? ColorConsts.verificationSuccessDark
                                        : ColorConsts.verificationSuccessLight,
                                  )
                                : Icon(
                                    Icons.close,
                                    color: Theme.of(context).brightness ==
                                            Brightness.dark
                                        ? ColorConsts.verificationFailureDark
                                        : ColorConsts.verificationFailureLight,
                                  ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: Text(
                                widget.isVerifyCompleted
                                    ? 'display_profile_page_claim_sheet_verification_completed'
                                        .localize
                                    : 'display_profile_page_claim_sheet_verification_not_completed'
                                        .localize,
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const Divider(
                          //       color: ColorConsts.buttonBackground,
                          ),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: widget.isVerified
                                ? Icon(
                                    Icons.shield_outlined,
                                    color: Theme.of(context).brightness ==
                                            Brightness.dark
                                        ? ColorConsts.verificationSuccessDark
                                        : ColorConsts.verificationSuccessLight,
                                  )
                                : Icon(
                                    Icons.close,
                                    color: Theme.of(context).brightness ==
                                            Brightness.dark
                                        ? ColorConsts.verificationFailureDark
                                        : ColorConsts.verificationFailureLight,
                                  ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: Text(
                                widget.isVerified
                                    ? 'display_profile_page_claim_sheet_verified_proof'
                                        .localize
                                    : 'display_profile_page_claim_sheet_not_verified_proof'
                                        .localize,
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ),
                          ),
                        ],
                      ),
                      if (!widget.isViaProxy)
                        SizedBox(height: _isIos ? 20 : 8.0),
                      if (widget.isViaProxy) ...[
                        const Divider(
                            //           color: ColorConsts.buttonBackground,
                            ),
                        Padding(
                          padding: EdgeInsets.only(bottom: _isIos ? 20 : 8.0),
                          child: Row(
                            children: [
                              const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Icon(Icons.info),
                              ),
                              Expanded(
                                child: Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: TextToLink(
                                        urlText:
                                            'display_profile_page_claim_sheet_proxy_warning'
                                                .localizeWithPlaceholders(
                                                    [userDomain])!,
                                        urlTextStyle: Theme.of(context)
                                            .textTheme
                                            .bodySmall!,
                                        textStyle: Theme.of(context)
                                            .textTheme
                                            .bodyMedium!)),
                              ),
                            ],
                          ),
                        )
                      ]
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}
