import 'package:flutter/material.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../../common/widgets/text_to_link.dart';

// Generates a qr-code and shows in a dialog.
class KeyQrCodeDialog extends StatelessWidget {
  final String qrCode;
  final String keyLink;

  const KeyQrCodeDialog(
      {super.key, required this.qrCode, required this.keyLink});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      semanticLabel:
          'display_profile_page_fingerprint_qr_code_dialog_title_semantics'
              .localize,
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(height: 10),
          Text(
            'display_profile_page_fingerprint_qr_code_dialog_title'.localize,
            style: Theme.of(context).textTheme.titleMedium,
          ),
          const SizedBox(height: 20),
          SizedBox(
            width: 200,
            height: 200,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: QrImageView(
                semanticsLabel: 'user_profile_page_qr_code_semantics'.localize,
                backgroundColor: Colors.white,
                data: qrCode,
                version: QrVersions.auto,
                size: 200.0,
                errorStateBuilder: (cxt, err) {
                  return Center(
                    child: Text(
                      'display_profile_page_qr_failure'.localize,
                      textAlign: TextAlign.center,
                    ),
                  );
                },
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: ExcludeSemantics(
              child: Text(
                'display_profile_page_qr_key_link'.localize,
                style: Theme.of(context).textTheme.headlineSmall,
              ),
            ),
          ),
          Semantics(
            label:
                'display_profile_page_fingerprint_qr_code_dialog_url_semantics'
                    .localizeWithPlaceholders([keyLink]),
            child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: TextToLink(
                  urlText: keyLink,
                  urlTextStyle: Theme.of(context).textTheme.bodySmall!,
                  textStyle: Theme.of(context).textTheme.bodyMedium!,
                )),
          ),
        ],
      ),
      actions: [
        Semantics(
          excludeSemantics: true,
          label: 'general_txt_done_semantics'.localize,
          child: TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'general_txt_done'.localize,
              )),
        )
      ],
    );
  }
}
