import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:keyoxide_flutter/common/constants/keyoxide_enums.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';

import '../../../common/constants/keyoxide_constants.dart';
import '../../generate_profile/model/keyoxide_profile.dart';
import 'claim_bottom_sheet.dart';

// The tile which is shown per claim in profile page
class ClaimTileWidget extends StatefulWidget {
  final Display display;
  final int claimStatus;

  const ClaimTileWidget(
      {super.key, required this.display, required this.claimStatus});

  @override
  State<ClaimTileWidget> createState() => _ClaimTileWidgetState();
}

class _ClaimTileWidgetState extends State<ClaimTileWidget> {
  @override
  Widget build(BuildContext context) {
    bool isAmbiguous = widget.display.serviceProviderName == null;
    bool isVerified = widget.claimStatus >= 200 && widget.claimStatus < 300;
    bool isVerificationCompleted = widget.claimStatus >= 200;
    bool isViaProxy = widget.claimStatus == 201;
    String serviceProviderIcon = widget.display.serviceProviderId == null
        ? ServiceProviders.fallbackprovider.icon
        : ServiceProviders.fromName(widget.display.serviceProviderId!).icon;

    Widget buildTrailingIcon() {
      if (isVerificationCompleted && !isVerified) {
        return Icon(
          Icons.close,
          color: Theme.of(context).brightness == Brightness.dark
              ? ColorConsts.verificationFailureDark
              : ColorConsts.verificationFailureLight,
        );
      }
      if (!isVerified && !isVerificationCompleted) {
        return Icon(
          Icons.close,
          color: Theme.of(context).brightness == Brightness.dark
              ? ColorConsts.verificationFailureDark
              : ColorConsts.verificationFailureLight,
        );
      }
      return Icon(
        Icons.check,
        color: Theme.of(context).brightness == Brightness.dark
            ? ColorConsts.verificationSuccessDark
            : ColorConsts.verificationSuccessLight,
      );
    }

    return Container(
      decoration: const BoxDecoration(
        color: Colors.transparent,
      ),
      child: Semantics(
        label: 'display_profile_page_claim_tile_btn_semantics'.localize,
        child: ListTile(
          horizontalTitleGap: 0,
          contentPadding: EdgeInsets.zero,
          //visualDensity: VisualDensity(horizontal: VisualDensity.minimumDensity, vertical: VisualDensity.minimumDensity),
          leading: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: SvgPicture.asset(serviceProviderIcon,
                width: 25, height: 25, color: Colors.grey),
          ),
          title: Text(
            isAmbiguous ? "[---]" : '[${widget.display.serviceProviderName}]',
            semanticsLabel: isAmbiguous && !isVerified
                ? 'display_profile_page_claim_tile_service_provider_name_ambiguous_not_verified_semantics'.localize
                : 'display_profile_page_claim_tile_service_provider_name_semantics'.localizeWithPlaceholders([widget.display.serviceProviderName.toString()]),
            style: Theme.of(context).textTheme.titleSmall,
          ),
          subtitle: Padding(
            padding: const EdgeInsets.only(right: 5.0),
            child: Text(
              widget.display.profileName!,
              semanticsLabel: 'display_profile_page_claim_tile_user_name_semantics'.localizeWithPlaceholders([widget.display.profileName!]),
              style: Theme.of(context).textTheme.bodyLarge,
            ),
          ),
          trailing: Padding(
            padding: const EdgeInsets.only(right: 12.0),
            child: buildTrailingIcon(),
          ),
          onTap: () {
            showModalBottomSheet(
                elevation: 0,
                isScrollControlled: true,
                //backgroundColor: Colors.transparent,
                context: context,
                builder: (BuildContext context) {
                  //Tapping a claim tile calls the ClaimBottomSheet and shows additional info about the claim.
                  return ClaimBottomSheet(
                    display: widget.display,
                    isViaProxy: isViaProxy,
                    isVerifyCompleted: isVerificationCompleted,
                    isVerified: isVerified,
                    isAmbiguous: isAmbiguous,
                    claimStatus: widget.claimStatus,
                  );
                });
          },
        ),
      ),
    );
  }
}
