import 'package:flutter/material.dart';

// Returns a nice circled profile image with a purple background.
class CircleImage extends StatelessWidget {
  const CircleImage({super.key, required this.imageUrl});

  final String imageUrl;

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: 80,
      backgroundColor: Theme.of(context).cardColor,
      child: CircleAvatar(
        radius: 73,
        backgroundImage: NetworkImage(imageUrl),
      ),
    );
  }
}
