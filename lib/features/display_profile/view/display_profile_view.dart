import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:go_router/go_router.dart';
import 'package:keyoxide_flutter/common/constants/keyoxide_enums.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/common/widgets/keyoxide_snackbar.dart';
import 'package:keyoxide_flutter/features/contacts/cubit/contacts_cubit.dart';
import 'package:keyoxide_flutter/features/generate_profile/model/keyoxide_profile.dart';
import 'package:share_plus/share_plus.dart';

import '../../../common/services/locator_service.dart';
import '../../generate_profile/cubit/generate_profile_cubit.dart';
import '../../user/cubit/user_cubit.dart';
import '../widgets/claim_tile_widget.dart';
import '../widgets/key_qr_code_dialog.dart';

class DisplayProfileView extends StatefulWidget {
  const DisplayProfileView({Key? key}) : super(key: key);

  @override
  State<DisplayProfileView> createState() => _DisplayProfileViewState();
}

class _DisplayProfileViewState extends State<DisplayProfileView>
    with SingleTickerProviderStateMixin {
  late Animation<double> profilePictureAnimation;
  late Animation<double> userNameAnimation;
  late Animation<double> emailAndKeyTitleAnimation;
  late Animation<double> claimTileAnimation;
  late AnimationController _controller;
  late KeyoxideProfile profile;
  bool hasUserSavedHisProfile =
      getIt<UserCubit>().keyoxideUser.pgpProfiles.isNotEmpty;
  late bool isContactAlreadySaved;
  bool _isLoading = false;

  // The profile fetched by the API and passed by the navigator
  int primaryUser = 0;
  Uint8List photo = Uint8List(0);
  String name = "";
  String description = "";
  String primaryEmail = "";
  FingerprintProtocol protocol = FingerprintProtocol.hkp;
  String fingerprint = "";
  String keyLink = "";
  String qrCodeUrl = "";

  //This method takes every claim for every user in the profile and makes a claim_tile_widget, returns it as a list.
  List<Widget> claimsList() {
    List<Widget> allClaimsList = [];
    for (Personas persona in (profile.personas as List<Personas>)) {
      if (persona.isRevoked == false) {
        bool isPrimary =
            persona == profile.personas![profile.primaryPersonaIndex!];
        var email = persona.email;
        Widget userClaimsList = Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (email != null)
              Transform.scale(
                scale: emailAndKeyTitleAnimation.value,
                child: Padding(
                  padding: const EdgeInsets.only(top: 10.0, bottom: 10),
                  child: FittedBox(
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                           Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10.0),
                              child: Text(
                                email,
                                semanticsLabel: isPrimary
                                    ? 'display_profile_page_title_primary_email_semantics'
                                    .localizeWithPlaceholders([email])
                                    : 'display_profile_page_title_email_semantics'
                                    .localizeWithPlaceholders([email]),
                                style: Theme.of(context).textTheme.titleLarge,
                              ),
                            ),
                      const Spacer(),
                          if (isPrimary && email != null)
                            ExcludeSemantics(
                            excluding: true,
                            child: Padding(
                              padding:
                              const EdgeInsets.only(left: 10.0, right: 10),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5.0),
                                  color:
                                  Theme.of(context).colorScheme.background,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    'display_profile_page_title_primary'
                                        .localize,
                                  ),
                                ),
                              ),
                            ),
                          )

                        ],
                      ),
                    ),
                  ),
                ),
              ),
            Transform.translate(
              offset: Offset(claimTileAnimation.value, 0.0),
              child: Column(
                children: (persona.claims as List<Claims>).map((claim) {
                  return ClaimTileWidget(
                    display: claim.display!,
                    claimStatus: claim.status!,
                  );
                }).toList(),
              ),
            )
          ],
        );
        isPrimary
            ? allClaimsList.insert(0, userClaimsList)
            : allClaimsList.add(userClaimsList);
      }
    }

    return allClaimsList;
  }

  @override
  void initState() {
    super.initState();
    bool isAnimationsDisabled = getIt<UserCubit>().keyoxideUser.userSettings.disableAnimations;
    profile = getIt<GenerateProfileCubit>().generatedProfile;
    // The profile fetched by the API and passed by the navigator
    primaryUser = profile.primaryPersonaIndex!.toInt();
    photo = getIt<GenerateProfileCubit>().generatedProfileImage;
    name = profile.personas![profile.primaryPersonaIndex!].name.toString();
    description =
        profile.personas![profile.primaryPersonaIndex!].description ?? '';
    primaryEmail =
        profile.personas![profile.primaryPersonaIndex!].email.toString();
    protocol = FingerprintProtocol.fromString(
        profile.publicKey!.fetch!.method.toString());
    fingerprint = profile.publicKey!.fingerprint!;
    keyLink = profile.publicKey!.fetch!.resolvedUrl!;
    qrCodeUrl = profile.publicKey!.fetch!.resolvedUrl!;
    _controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: isAnimationsDisabled ? 0 : 5),
    );
    isContactAlreadySaved = getIt<UserCubit>().isContactAlreadySaved(profile);

    profilePictureAnimation = Tween<double>(begin: -200.0, end: 0.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: const Interval(0.0, 0.125, curve: Curves.easeOut),
      ),
    );
    userNameAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: const Interval(0.125, 0.250, curve: Curves.easeIn),
      ),
    );
    emailAndKeyTitleAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: const Interval(0.250, 0.375, curve: Curves.elasticOut),
      ),
    );

    claimTileAnimation = Tween<double>(begin: -500.0, end: 0.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve:
            const Interval(0.375, 0.500, curve: Curves.fastLinearToSlowEaseIn),
      ),
    );

    _controller.addListener(() {
      setState(() {});
    });
    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    getIt<GenerateProfileCubit>().disposeGeneratedProfile();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ContactsCubit, ContactsState>(
      bloc: getIt<ContactsCubit>(),
      listener: (context, state) {
        if (state is SaveContactSuccess) {
          KeyoxideSnackBar()
              .showOverlaySnackBar(context, 'contacts_save_success'.localize);
        }
      },
      builder: (BuildContext context, ContactsState state) {
        if (state is ContactsLoading) {
          _isLoading = true;
        } else {
          _isLoading = false;
        }
        if (state is SaveContactSuccess) {
          isContactAlreadySaved =
              getIt<UserCubit>().isContactAlreadySaved(profile);
        }
        return Scaffold(
            extendBodyBehindAppBar: true,
            appBar: AppBar(
              elevation: 0,
              scrolledUnderElevation: 0,
              backgroundColor: Colors.transparent,
              shadowColor: Colors.transparent,
              leading: Semantics(
                label: 'qr_scanner_btn_back_semantics'.localize,
                child: IconButton(
                  onPressed: () {
                    context.pop();
                  },
                  icon: const Icon(Icons.arrow_back),
                ),
              ),
            ),
            body: Stack(children: [
              Padding(
                padding:
                    EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                child: SingleChildScrollView(
                  child: Column(children: [
                    const SizedBox(height: 20),
                    //Profile picture
                    Transform.translate(
                      offset: Offset(0.0, profilePictureAnimation.value),
                      child: Hero(
                        tag: "userAvatar",
                        child: Semantics(
                            label:
                                'user_profile_page_user_avatar_image_semantics'
                                    .localize,
                            child: CircleAvatar(
                                maxRadius: 73,
                                backgroundImage: MemoryImage(photo))),
                      ),
                    ),
                    const SizedBox(height: 10),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 23.0),
                      child: Opacity(
                        opacity: userNameAnimation.value,
                        child: Text(
                          name,
                          semanticsLabel:
                              'user_profile_page_user_name_semantics'
                                  .localizeWithPlaceholders([name]),
                          style: const TextStyle(fontSize: 25),
                        ),
                      ),
                    ),
                    const SizedBox(height: 10),
                    if (description != '') ...[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 23.0),
                        child: Opacity(
                          opacity: userNameAnimation.value,
                          child: Text(
                            description,
                            textAlign: TextAlign.center,
                            style: const TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                    ],
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 23.0),
                        child: Transform.scale(
                            scale: emailAndKeyTitleAnimation.value,
                            child: Text(
                                'display_profile_page_title_identity_claims'
                                    .localize,
                                semanticsLabel:
                                    'display_profile_page_title_identity_claims_semantics'
                                        .localize,
                                style: Theme.of(context).textTheme.titleLarge)),
                      ),
                    ),
                    SizedBox(height: 10),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 23.0),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Theme.of(context).hoverColor),
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 6.0),
                          child: Column(
                            children: claimsList(),
                          ),
                        ),
                      ),
                    ),

                    SizedBox(height: 10),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 23.0),
                        child: Transform.scale(
                            scale: emailAndKeyTitleAnimation.value,
                            child: Text(
                                'display_profile_page_title_profile_info'
                                    .localize,
                                semanticsLabel:
                                    'display_profile_page_title_key_semantics'
                                        .localize,
                                style: Theme.of(context).textTheme.titleLarge)),
                      ),
                    ),
                    SizedBox(height: 10),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 23.0),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Theme.of(context).hoverColor),
                        child: Column(
                          children: [
                            SizedBox(height: 10),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Transform.scale(
                                    scale: emailAndKeyTitleAnimation.value,
                                    child: Text(
                                        'display_profile_page_title_key'
                                            .localize,
                                        semanticsLabel:
                                            'display_profile_page_title_key_semantics'
                                                .localize,
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleMedium)),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8.0),
                              child: Transform.translate(
                                offset: Offset(claimTileAnimation.value, 0.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    // border: Border.all(
                                    //   style: BorderStyle.solid,
                                    //   color: Theme.of(context).dividerColor,
                                    // ),
                                    // borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  child: ListTile(
                                    horizontalTitleGap: 0,
                                    contentPadding: EdgeInsets.zero,
                                    // Fingerprint and protocol is shown here, pressing the qr-code button shows a dialog with the qr-code
                                    title: Text(
                                      '[${profile.publicKey!.keyType} / ${profile.publicKey!.fetch!.method}]',
                                      semanticsLabel:
                                          'display_profile_page_protocol_type_semantics'
                                              .localizeWithPlaceholders(
                                                  [protocol.name]),
                                      style:
                                          Theme.of(context).textTheme.bodyLarge,
                                    ),
                                    subtitle: Text(
                                      fingerprint,
                                      semanticsLabel:
                                          'display_profile_page_fingerprint_semantics'
                                              .localizeWithPlaceholders(
                                                  [fingerprint]),
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleSmall,
                                    ),
                                    trailing: Container(
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        border: Border.all(
                                          style: BorderStyle.solid,
                                          color: Theme.of(context)
                                              .iconTheme
                                              .color!,
                                        ),
                                      ),
                                      child: Semantics(
                                        label:
                                            'display_profile_page_fingerprint_qr_code_button_semantics'
                                                .localize,
                                        child: IconButton(
                                          onPressed: () => showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return KeyQrCodeDialog(
                                                qrCode: qrCodeUrl,
                                                keyLink: keyLink,
                                              );
                                            },
                                          ),
                                          icon: const Icon(
                                            Icons.qr_code_outlined,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 80),
                  ]),
                ),
              ),
              if (_isLoading) const Center(child: CircularProgressIndicator())
            ]),
            floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
            floatingActionButton: Semantics(
              label: 'user_profile_page_btn_menu_semantics'.localize,
              child: SpeedDial(
                icon: Icons.menu_outlined,
                activeIcon: Icons.close,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                renderOverlay: true,
                closeDialOnPop: true,
                overlayOpacity: 0.0,
                animationCurve: Curves.easeInOut,
                children: [
                  SpeedDialChild(
                    visible: !isContactAlreadySaved,
                    child: const Icon(Icons.save),
                    label: 'speed_dial_menu_title_save_contact'.localize,
                    onTap: () {
                      getIt<ContactsCubit>().saveProfileToContacts(profile);
                    },
                  ),
                  SpeedDialChild(
                    child: const Icon(Icons.share_outlined),
                    label: 'speed_dial_menu_title_share_profile_link'.localize,
                    onTap: () {
                      String subLink = protocol == FingerprintProtocol.wkd
                          ? primaryEmail
                          : protocol == FingerprintProtocol.aspe
                              ? profile.identifier!
                              : fingerprint;
                      Share.share("https://keyoxide.org/$subLink");
                    },
                  ),
                  SpeedDialChild(
                    child: const Icon(Icons.share_outlined),
                    label: 'speed_dial_menu_title_share_fingerprint'.localize,
                    onTap: () {
                      Share.share(profile.identifier!);
                    },
                  ),
                ],
              ),
            ));
      },
    );
  }
}
