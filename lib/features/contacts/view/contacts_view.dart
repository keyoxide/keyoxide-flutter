import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:flutter_swipe_action_cell/core/cell.dart';
import 'package:go_router/go_router.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/common/utils/utilities.dart';
import 'package:keyoxide_flutter/features/contacts/cubit/contacts_cubit.dart';
import 'package:keyoxide_flutter/features/contacts/widget/refresh_all_contacts_overlay.dart';
import 'dart:io';

import '../../../common/constants/keyoxide_constants.dart';
import '../../../common/services/locator_service.dart';
import '../../../common/widgets/claim_count_loading_overlay.dart';
import '../../../common/widgets/keyoxide_snackbar.dart';
import '../../generate_profile/cubit/generate_profile_cubit.dart';
import '../../user/cubit/user_cubit.dart';
import '../../user/model/pgp_profile_model.dart';

class ContactsView extends StatefulWidget {
  const ContactsView({super.key});

  @override
  State<ContactsView> createState() => _ContactsViewState();
}

class _ContactsViewState extends State<ContactsView> {
  late List<ProfileModel> contacts;
  bool _isLoading = false;
  bool _isImportExportLoading = false;
  bool _isContactRefreshing = false;
  late int totalContactsCount;
  int refreshedContactCount = 0;
  bool _isAllContactsRefreshing = false;

  Widget _contactTile(ProfileModel contact) {
    return Padding(
      padding: const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
      child: SwipeActionCell(
        key: ValueKey(contact),
        leadingActions: [
          SwipeAction(
            performsFirstActionWithFullSwipe: true,
            backgroundRadius: 5,
            title: "general_txt_refresh".localize,
            style: Theme.of(context).textTheme.bodyMedium!,
            color: Colors.blue,
            forceAlignmentToBoundary: true,
            icon: Icon(Icons.refresh_outlined,
                color: Theme.of(context).textTheme.bodyMedium!.color),
            onTap: (handler) async {
              await getIt<ContactsCubit>().refreshContact(contact: contact);
            },
          ),
        ],
        trailingActions: [
          SwipeAction(
              backgroundRadius: 5,
              title: "general_txt_delete".localize,
              style: Theme.of(context).textTheme.bodyMedium!,
              forceAlignmentToBoundary: true,
              icon: Icon(Icons.delete_outline,
                  color: Theme.of(context).textTheme.bodyMedium!.color),
              nestedAction: SwipeNestedAction(
                  title: "password_dialog_btn_confirm".localize),
              onTap: (handler) async {
                await handler(true);

                await getIt<ContactsCubit>().deleteProfileFromContacts(contact);
              }),
        ],
        child: Container(
          decoration: BoxDecoration(
            color: Theme.of(context).colorScheme.onSecondary,
            border: Border.all(
              style: BorderStyle.solid,
              color: Theme.of(context).dividerColor,
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: ListTile(
            title: Text(contact.name),
            subtitle: Column(
              children: [
                Text(
                    contact.primaryEmail == ''
                        ? contact.fingerprint
                        : contact.identifier,
                    overflow: TextOverflow.ellipsis),
                Row(
                  children: [
                    Text('general_txt_updated_at'.localize),
                    Text(Utilities.formatDateTimeToLocal(
                        context, contact.updatedAt)),
                  ],
                ),
              ],
            ),
            trailing: CircleAvatar(
              maxRadius: 25,
              backgroundColor: Theme.of(context).cardColor,
              child: Semantics(
                label: 'user_profile_page_user_avatar_image_semantics'.localize,
                child: CircleAvatar(
                    //maxRadius: 73,
                    backgroundImage: MemoryImage(contact.image)),
              ),
            ),
            onTap: () async {
              await getIt<GenerateProfileCubit>()
                  .openSavedProfile(contact.profile, contact.image);
              // await getIt<GenerateProfileCubit>().getProfileClaimCount(
              //     contact.profile.publicKey!.fetch!.query!,
              //     GenerateProfileFor.contactsView,
              //     false,
              //     protocol: contact.protocol);
            },
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    contacts = getIt<UserCubit>().keyoxideUser.contacts;
    super.initState();
  }

  Future<void> _handleGenerateProfileCubitListenerStates(
      GenerateProfileState state) async {
    if (state is ContactsViewProfileGenerationFailure) {
      String errorMessage = state.error.message.toString();
      KeyoxideSnackBar().showOverlaySnackBar(context, errorMessage);
    } else if (state is ContactsViewGetProfileClaimsSuccess) {
      getIt<GenerateProfileCubit>().generateProfile(state.queryValue,
          GenerateProfileFor.contactsView, state.isAddingProfile,
          protocol: state.protocol);
    } else if (state is ContactsViewProfileGenerationSuccess) {
      Future.delayed(Duration.zero, () async {
        context.push(displayProfilePath);
      });
    }
  }

  Future<void> _handleGenerateProfileCubitBuilderStates(
      GenerateProfileState state) async {
    if (state is ContactsViewProfileLoading ||
        state is ContactsViewGetProfileClaimsSuccess) {
      _isLoading = true;
    } else if (state is ImportExportContactsLoading) {
      _isImportExportLoading = true;
    } else {
      _isLoading = false;
      _isImportExportLoading = false;
    }
  }

  SpeedDial _buildSpeedDial() {
    return SpeedDial(
        icon: Icons.import_contacts_outlined,
        activeIcon: Icons.close,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        renderOverlay: true,
        closeDialOnPop: true,
        overlayOpacity: 0.0,
        animationCurve: Curves.easeInOut,
        children: [
          SpeedDialChild(
            visible: contacts.isNotEmpty,
            child: const Icon(Icons.import_export_outlined),
            label: 'contacts_btn_export'.localize,
            onTap: () async {
              await getIt<ContactsCubit>().exportContacts();
            },
          ),
          SpeedDialChild(
            child: const Icon(Icons.import_export_outlined),
            label: 'contacts_btn_import'.localize,
            onTap: () async {
              await getIt<ContactsCubit>().importContacts();
            },
          ),
          SpeedDialChild(
            child: const Icon(Icons.refresh_outlined),
            label: 'contacts_btn_refresh_all'.localize,
            onTap: () async {
              await getIt<ContactsCubit>().refreshAllContacts();
            },
          ),
        ]);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<GenerateProfileCubit, GenerateProfileState>(
      bloc: getIt<GenerateProfileCubit>(),
      listener: (context, state) {
        _handleGenerateProfileCubitListenerStates(state);
      },
      builder: (context, state) {
        _handleGenerateProfileCubitBuilderStates(state);
        return BlocConsumer<ContactsCubit, ContactsState>(
          bloc: getIt<ContactsCubit>(),
          listener: (BuildContext context, ContactsState state) {
            if (state is ImportContactsSuccess) {
              KeyoxideSnackBar().showOverlaySnackBar(
                  context,
                  'contacts_import_success'.localizeWithPlaceholders([
                    state.importedContactsCount.toString(),
                  ])!);
            }

            if (state is ExportContactsSuccess) {
              String message = Platform.isIOS
                  ? 'contacts_export_success_ios'.localize
                  : 'contacts_export_success'.localizeWithPlaceholders([
                      state.exportedContactsCount.toString(),
                    ])!;
              KeyoxideSnackBar().showOverlaySnackBar(context, message);
            }
            if (state is ImportContactsFailure) {
              KeyoxideSnackBar()
                  .showOverlaySnackBar(context, state.error.message.localize);
            }
            if (state is ExportContactsFailure) {
              KeyoxideSnackBar()
                  .showOverlaySnackBar(context, state.error.message.localize);
            }

            if (state is RefreshContactFailure) {
              KeyoxideSnackBar().showOverlaySnackBar(
                  context, 'contacts_refresh_failure'.localize);
            }
            if (state is RefreshContactSuccess) {
              KeyoxideSnackBar().showOverlaySnackBar(
                  context, 'contacts_refresh_success'.localize);
            }

            if (state is RefreshAllContactsFailure) {
              KeyoxideSnackBar().showOverlaySnackBar(
                  context,
                  'dio_exception_connection_error'.localize);
            }
          },
          builder: (context, state) {
            if (state is ContactRefreshing) {
              _isContactRefreshing = true;
            } else {
              _isContactRefreshing = false;
            }
            if (state is ImportExportContactsLoading) {
              _isImportExportLoading = true;
            } else {
              _isImportExportLoading = false;
            }
            if (state is RefreshingAllContacts) {
              _isAllContactsRefreshing = true;
            } else {
              _isAllContactsRefreshing = false;
            }
            if (state is SaveContactSuccess) {
              contacts = state.contacts;
            }
            if (state is DeleteContactSuccess) {
              contacts = state.contacts;
            }
            if (state is ImportContactsSuccess) {
              contacts = getIt<UserCubit>().keyoxideUser.contacts;
            }

            return Scaffold(
              body: contacts.isEmpty
                  ? Center(child: Text('contacts_empty_list_warning'.localize))
                  : Stack(
                      children: [
                        ListView(
                          children: contacts
                              .map((contact) => _contactTile(contact))
                              .toList(),
                        ),
                        if (_isLoading) const ClaimCountLoadingOverlay(),
                        if (_isImportExportLoading || _isContactRefreshing)
                          const Center(
                            child: CircularProgressIndicator(),
                          ),
                        if (_isAllContactsRefreshing)
                          const RefreshAllContactsOverlay(),
                      ],
                    ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.endFloat,
              floatingActionButton: _buildSpeedDial(),
            );
          },
        );
      },
    );
  }
}
