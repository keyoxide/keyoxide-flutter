import 'dart:convert';
import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:keyoxide_flutter/common/models/keyoxide_result.dart';
import 'package:path_provider/path_provider.dart';

import '../../user/model/pgp_profile_model.dart';

class ContactsRepository {

  Future<String?> _getDirectoryPath() async {
    final initialDirectory = await getApplicationDocumentsDirectory();
    var directory = await FilePicker.platform.getDirectoryPath(initialDirectory: initialDirectory.path);
    return directory;
  }


  Future<KeyoxideResult?> exportProfilesToJsonFile(List<ProfileModel> models) async {
     final String content = jsonEncode(models.map((model) => model.toJson()).toList());
     final directoryPath = Platform.isIOS ? await getApplicationDocumentsDirectory().then((value) => value.path) : await _getDirectoryPath();

    try {
      if (directoryPath != null) {
        String filePath = '$directoryPath/keyoxide_contacts.json';
        File file = File(filePath);

        await file.writeAsString(content);
        return KeyoxideResult(data: filePath);
      } else {
        return null;
      }
    } catch (e) {
      debugPrint("Error saving file: $e");
      return const KeyoxideResult(error: KeyoxideError(message: "contacts_export_failure"));
    }
  }

  Future<KeyoxideResult?> importProfilesFromJsonFile() async {
    final initialDirectory = await getApplicationDocumentsDirectory();
    FilePickerResult? result = await FilePicker.platform.pickFiles(type: FileType.custom,
      allowedExtensions: ['json'], initialDirectory: initialDirectory.path);

    if (result != null) {
      File file = File(result.files.single.path!);

      try {
        final fileContents = await File(file.path!).readAsString();
        final List<dynamic> jsonData = jsonDecode(fileContents);
        List<ProfileModel> profiles = jsonData.map((item) => ProfileModel.fromJson(item)).toList();

        return KeyoxideResult(
          data: profiles,
        );
      } catch (e) {
        debugPrint("Error reading or parsing file: $e");
        return const KeyoxideResult(error: KeyoxideError(message: "contacts_import_failure"));
      }
    } else {
      // User canceled the picker
      debugPrint("No file selected");
      return null;
    }
  }

}