import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/features/contacts/cubit/contacts_cubit.dart';

import '../../../common/services/locator_service.dart';
import '../../user/cubit/user_cubit.dart';

class RefreshAllContactsOverlay extends StatefulWidget {
  const RefreshAllContactsOverlay({
    super.key,
  });

  @override
  State<RefreshAllContactsOverlay> createState() =>
      _RefreshAllContactsOverlayState();
}

class _RefreshAllContactsOverlayState extends State<RefreshAllContactsOverlay> with TickerProviderStateMixin {
  int refreshedContactCount = 1;
  late int totalContactCount;
  late AnimationController controller;
  late Animation<double> animation;

  @override
  void initState() {
    super.initState();
    totalContactCount = getIt<UserCubit>().keyoxideUser.contacts.length;
    controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1), // Smooth transition duration
    );
    animation = Tween<double>(begin: 0, end: 1).animate(controller)
      ..addListener(() {
        setState(() {});
      });
  }

  void updateProgress() {
    final newProgress = (1.0 / totalContactCount) * refreshedContactCount;
    controller.animateTo(newProgress);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ContactsCubit, ContactsState>(
      bloc: getIt<ContactsCubit>(),
      listener: (context, state) {
        if (state is RefreshingAllContacts) {
          refreshedContactCount++;
          updateProgress();
        }
        if (state is RefreshAllContactsSuccess) {
          controller.stop();
        }
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Center(
          child: Container(
            width: double.infinity,
            height: 200,
            decoration: BoxDecoration(
              color: Theme.of(context).brightness == Brightness.dark
                  ? Colors.black.withOpacity(0.9)
                  : Colors.white.withOpacity(0.9),
              borderRadius: const BorderRadius.all(Radius.circular(20)),
            ),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'contacts_refresh_all_process'.localize,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 10),
                  CircularProgressIndicator(value: animation.value),
                  const SizedBox(height: 10),
                  Text(
                    'contacts_refresh_all_count'.localizeWithPlaceholders([
                      refreshedContactCount.toString(),
                      totalContactCount.toString()
                    ])!,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}