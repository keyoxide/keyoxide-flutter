part of 'contacts_cubit.dart';

abstract class ContactsState extends Equatable {
  const ContactsState();

  @override
  List<Object> get props => [];
}

class ContactsInitial extends ContactsState {}

class ContactsLoading extends ContactsState {}

class SaveContactSuccess extends ContactsState {
  final List<ProfileModel> contacts;

  const SaveContactSuccess({required this.contacts});
}

class SaveContactFailure extends ContactsState {}

class DeleteContactSuccess extends ContactsState {
  final List<ProfileModel> contacts;

  const DeleteContactSuccess({required this.contacts});
}

class ImportExportContactsLoading extends ContactsState {}

class ImportContactsSuccess extends ContactsState {
  final int importedContactsCount;

  const ImportContactsSuccess({required this.importedContactsCount});
}

class ImportContactsFailure extends ContactsState {
  final KeyoxideError error;

  const ImportContactsFailure({required this.error});
}

class ImportContactsCanceled extends ContactsState {}

class ExportContactsSuccess extends ContactsState {
  final int exportedContactsCount;

  const ExportContactsSuccess({required this.exportedContactsCount});
}

class ExportContactsFailure extends ContactsState {
  final KeyoxideError error;

  const ExportContactsFailure({required this.error});
}

class ContactRefreshing extends ContactsState {}

class RefreshContactSuccess extends ContactsState {}

class RefreshContactFailure extends ContactsState {}

class RefreshingAllContacts extends ContactsState {}

class RefreshAllContactsIntermediaryState extends ContactsState {}

class RefreshAllContactsSuccess extends ContactsState {}

class RefreshAllContactsFailure extends ContactsState {}


