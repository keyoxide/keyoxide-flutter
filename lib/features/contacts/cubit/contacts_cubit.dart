import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:internet_connection_checker_plus/internet_connection_checker_plus.dart';
import 'package:keyoxide_flutter/common/models/keyoxide_result.dart';
import 'package:keyoxide_flutter/common/utils/utilities.dart';
import 'package:keyoxide_flutter/features/generate_profile/model/keyoxide_profile.dart';
import 'package:keyoxide_flutter/features/user/model/keyoxide_user.dart';
import 'package:keyoxide_flutter/features/user/model/pgp_profile_model.dart';

import '../../../common/services/locator_service.dart';
import '../../generate_profile/cubit/generate_profile_cubit.dart';
import '../../user/cubit/user_cubit.dart';
import '../repository/contacts_repository.dart';

part 'contacts_state.dart';

class ContactsCubit extends Cubit<ContactsState> {
  ContactsCubit() : super(ContactsInitial());

  Future<bool> refreshContact(
      {required ProfileModel contact, bool isRefreshingAllContacts = false}) async {
    if (!isRefreshingAllContacts) {
      emit(ContactRefreshing());
    }

      ProfileModel? refreshedContact =
      await getIt<GenerateProfileCubit>().refreshPgpProfile(contact);
      if (refreshedContact != null) {
        KeyoxideUser keyoxideUser = getIt<UserCubit>().keyoxideUser;
        int profileIndex = keyoxideUser.contacts.indexOf(contact);
        keyoxideUser.contacts[profileIndex] = refreshedContact;
        await getIt<UserCubit>().updateKeyoxideUser(keyoxideUser);
        if(!isRefreshingAllContacts) {
          emit(RefreshContactSuccess());
        }
        return true;
      } else {
        if (!isRefreshingAllContacts) {
          emit(RefreshContactFailure());
        }
        return false;
      }
  }

  Future<void> refreshAllContacts() async {
    bool hasInternet = await InternetConnection().hasInternetAccess;
    if (!hasInternet) {
      emit(RefreshAllContactsFailure());
    } else {
      List<ProfileModel> contacts = getIt<UserCubit>().keyoxideUser.contacts;

      for (ProfileModel contact in contacts) {
        emit(RefreshingAllContacts());
        await refreshContact(contact: contact, isRefreshingAllContacts: true);
        emit(RefreshAllContactsIntermediaryState());
      }
      emit(RefreshAllContactsSuccess());
    }
  }

  Future<void> saveProfileToContacts(KeyoxideProfile profile) async {
    emit(ContactsLoading());
    ProfileModel contact =
        await Utilities.keyoxideProfileToProfileModel(profile);
    bool isContactSaved = await getIt<UserCubit>().saveContact(contact);

    if (isContactSaved) {
      emit(SaveContactSuccess(
          contacts: getIt<UserCubit>().keyoxideUser.contacts));
    } else {
      emit(SaveContactFailure());
    }
  }

  Future<void> deleteProfileFromContacts(ProfileModel profile) async {
    emit(ContactsLoading());
    await getIt<UserCubit>().deleteContact(profile);
    emit(DeleteContactSuccess(
        contacts: getIt<UserCubit>().keyoxideUser.contacts));
  }

  Future<void> importContacts() async {
    emit(ImportExportContactsLoading());

      KeyoxideResult? result =
          await getIt<ContactsRepository>().importProfilesFromJsonFile();

      if (result != null) {
        if (result.error != null) {
          emit(ImportContactsFailure(error: result.error!));
        } else {
          List<ProfileModel> contacts = result.data!;
          int importedContactsCount =
              await getIt<UserCubit>().importNonExistingContacts(contacts);
          emit(ImportContactsSuccess(
              importedContactsCount: importedContactsCount));
        }
      } else {
        emit(ImportContactsCanceled());
      }

  }

  Future<void> exportContacts() async {
    emit(ImportExportContactsLoading());

      KeyoxideResult? result = await getIt<ContactsRepository>()
          .exportProfilesToJsonFile(getIt<UserCubit>().keyoxideUser.contacts);

      if (result?.data != null) {
        int exportedContactsCount =
            getIt<UserCubit>().keyoxideUser.contacts.length;
        emit(ExportContactsSuccess(
            exportedContactsCount: exportedContactsCount));
      } else {
        emit(ExportContactsFailure(
            error: KeyoxideError(message: result?.error!.message)));
      }

  }
}
