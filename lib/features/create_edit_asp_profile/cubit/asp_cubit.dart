import 'package:aspe/aspe.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:jose_plus/jose.dart';
import 'package:keyoxide_flutter/features/create_edit_asp_profile/model/asp_profile_model.dart';
import 'package:keyoxide_flutter/features/generate_profile/cubit/generate_profile_cubit.dart';
import 'package:keyoxide_flutter/features/generate_profile/model/keyoxide_profile.dart';
import 'package:kx_claim_service/kx_claim_service.dart';

import '../../../common/constants/keyoxide_enums.dart';
import '../../../common/models/keyoxide_result.dart';
import '../../../common/services/locator_service.dart';
import '../../../common/utils/utilities.dart';
import '../../generate_profile/repository/keyoxide_repository.dart';
import '../../settings/repository/settings_repository.dart';
import '../../user/cubit/user_cubit.dart';

part 'asp_state.dart';

enum AspProfileDeletionType {
  fromServer,
  fromDevice,
  fromBoth,
}

class AspCubit extends Cubit<AspState> {
  AspCubit() : super(AspInitial());

  Future<List<KxClaimDefinition>> getAllServiceProviders() async {
    List<KxClaimDefinition> serviceProviderList = [];

    for (var providerId in ServiceProviderId.values) {
      var serviceProvider =
          await KxClaimDefinition.fromServiceProviderId(providerId);
      serviceProviderList.add(serviceProvider);
    }

    return serviceProviderList;
  }

  Future<void> uploadProfile(AspProfile profile) async {
    emit(AspLoading());
    try {
      KeyoxideResult domainValidation = await getIt<SettingsRepository>()
          .aspeDomainValidationRequested(profile.domain);

      if (domainValidation.error != null) {
        emit(UploadProfileFailure(KeyoxideError(
            message: domainValidation.error!.message,
            code: domainValidation.error!.code,
            error: domainValidation.error!.toString())));
      } else {
        final result = await profile.upload();
        if (result.success) {
          await getIt<GenerateProfileCubit>().generateProfile(profile.uri.toString(), GenerateProfileFor.aspProfileModel, false);
          KeyoxideProfile keyoxideProfile = getIt<GenerateProfileCubit>().generatedProfile;
          AspProfileModel profileModel = AspProfileModel(
            avatarImage: await Utilities.urlImageToUInt8List(
                profile.avatarUrl().replaceAll('svg', 'png')),
            aspProfile: profile,
            isHidden: false,
            updatedAt: DateTime.now(),
            keyoxideProfile: keyoxideProfile,
          );

          emit(UploadProfileSuccess(profile: profileModel));
        }
      }
    } on AspeException catch (e) {
      emit(UploadProfileFailure(KeyoxideError(
          message: e.translationKey, code: e.httpStatus, error: e.toString())));
    }
  }

  Future<AspProfileModel?> refreshProfile(AspProfileModel profile) async {
    try {
      AspProfile refreshedProfile = await AspProfile.import(
        profile.aspProfile.domain,
        profile.aspProfile.secretKey,
      );
      await getIt<GenerateProfileCubit>().generateProfile(profile.aspProfile.uri.toString(), GenerateProfileFor.aspProfileModel, false);
      KeyoxideProfile keyoxideProfile = getIt<GenerateProfileCubit>().generatedProfile;
      AspProfileModel aspProfileModel = AspProfileModel(
        avatarImage: await Utilities.urlImageToUInt8List(
            refreshedProfile.avatarUrl().replaceAll('svg', 'png')),
        aspProfile: refreshedProfile,
        isHidden: profile.isHidden,
        updatedAt: DateTime.now(),
        keyoxideProfile: keyoxideProfile,
      );
      return aspProfileModel;
    } on AspeException {
      // More information about the specific cases can be given
      rethrow;
    } catch (e) {
      rethrow;
    }
  }

  Future<void> importProfile(
      String domain, String secretKey, String password) async {
    emit(AspLoading());
    try {
      JsonWebKey unencryptedSecretKey = decryptKey(secretKey, password);
      AspProfile importedProfile =
          await AspProfile.import(domain, unencryptedSecretKey);
      await getIt<GenerateProfileCubit>().generateProfile(importedProfile.uri.toString(), GenerateProfileFor.aspProfileModel, false);
      KeyoxideProfile keyoxideProfile = getIt<GenerateProfileCubit>().generatedProfile;
      AspProfileModel profileModel = AspProfileModel(
        avatarImage: await Utilities.urlImageToUInt8List(
            importedProfile.avatarUrl().replaceAll('svg', 'png')),
        aspProfile: importedProfile,
        isHidden: false,
        updatedAt: DateTime.now(),
        keyoxideProfile: keyoxideProfile,
      );
      emit(LoadProfileSuccess(profile: profileModel));
      await getIt<UserCubit>().saveUserProfile(aspProfile: profileModel);
    } on AspeException catch (e) {
      emit(LoadProfileFailure(KeyoxideError(
          message: e.translationKey, code: e.httpStatus, error: e.toString())));
    }
  }

  Future<bool> deleteProfile(AspProfileModel profileModel,
      AspProfileDeletionType deleteOperation) async {
    emit(AspLoading());
    try {
      if (deleteOperation == AspProfileDeletionType.fromDevice) {
        _deleteFromDevice(profileModel);
        emit(DeleteProfileSuccess(profile: profileModel));
        return true;
      }

      if (deleteOperation == AspProfileDeletionType.fromServer) {
        return await _deleteFromServer(profileModel);
      }

      if (deleteOperation == AspProfileDeletionType.fromBoth) {
        bool isProfileDeleted = await _deleteFromServer(profileModel);
        if (isProfileDeleted) {
          _deleteFromDevice(profileModel);
          return true;
        }
      }
      return false;
    } on AspeException catch (e) {
      emit(DeleteProfileFailure(KeyoxideError(
          message: e.translationKey, code: e.httpStatus, error: e.toString())));
      return false;
    }
  }

  Future<void> _deleteFromDevice(AspProfileModel profileModel) async {
    getIt<UserCubit>().deleteUserProfile(aspProfile: profileModel);
  }

  Future<bool> _deleteFromServer(AspProfileModel profileModel) async {
    try {
      KeyoxideResult domainValidation = await getIt<SettingsRepository>()
          .aspeDomainValidationRequested(profileModel.aspProfile.domain);

      if (domainValidation.error != null) {
        emit(UploadProfileFailure(KeyoxideError(
            message: domainValidation.error!.message,
            code: domainValidation.error!.code,
            error: domainValidation.error!.toString())));
        return false;
      } else {
        await profileModel.aspProfile.delete();

        emit(DeleteProfileSuccess(profile: profileModel));
        return true;
      }
    } on AspeException catch (e) {
      emit(DeleteProfileFailure(KeyoxideError(
          message: e.translationKey, code: e.httpStatus, error: e.toString())));
      return false;
    }
  }
}
