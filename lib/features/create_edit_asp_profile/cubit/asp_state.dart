part of 'asp_cubit.dart';

abstract class AspState extends Equatable {
  const AspState();

  @override
  List<Object> get props => [];
}

class AspInitial extends AspState {}

class AspLoading extends AspState {}

class LoadProfileSuccess extends AspState {
  final AspProfileModel profile;

  const LoadProfileSuccess({required this.profile});
}

class LoadProfileFailure extends AspState {
  final KeyoxideError error;

  const LoadProfileFailure(this.error);
}

class DeleteProfileSuccess extends AspState {
  final AspProfileModel profile;

  const DeleteProfileSuccess({required this.profile});
}

class DeleteProfileFailure extends AspState {
  final KeyoxideError error;

  const DeleteProfileFailure(this.error);
}

class ExportProfileSuccess extends AspState {}

class ExportProfileFailure extends AspState {
  final KeyoxideError error;

  const ExportProfileFailure(this.error);
}

class UploadProfileSuccess extends AspState {
  final AspProfileModel profile;

  const UploadProfileSuccess({required this.profile});
}

class UploadProfileFailure extends AspState {
  final KeyoxideError error;

  const UploadProfileFailure(this.error);
}
