import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/common/widgets/keyoxide_snackbar.dart';
import 'package:keyoxide_flutter/features/create_edit_asp_profile/widget/delete_dialog.dart';
import 'package:keyoxide_flutter/features/create_edit_asp_profile/widget/password_dialog.dart';
import 'package:keyoxide_flutter/features/create_edit_asp_profile/widget/profile_creation_pages/identity_claims_page.dart';
import 'package:keyoxide_flutter/features/create_edit_asp_profile/widget/profile_creation_pages/profile_information_page.dart';
import 'package:keyoxide_flutter/features/create_edit_asp_profile/widget/profile_creation_pages/security_page.dart';
import 'package:keyoxide_flutter/features/create_edit_asp_profile/widget/secret_key_dialog.dart';
import 'package:page_view_dot_indicator/page_view_dot_indicator.dart';

import '../../../common/services/locator_service.dart';
import '../../user/cubit/user_cubit.dart';
import '../cubit/asp_cubit.dart';
import '../model/asp_profile_model.dart';

class CreateASPProfileDialog extends StatefulWidget {
  CreateASPProfileDialog({super.key, required this.profileModel, this.isProfileNew = false});

  AspProfileModel profileModel;
  bool isProfileNew;

  @override
  State<CreateASPProfileDialog> createState() => _CreateASPProfileDialogState();
}

class _CreateASPProfileDialogState extends State<CreateASPProfileDialog> {
  int pageIndex = 0;
  final _profileNameController = TextEditingController();
  final _profileDescriptionController = TextEditingController();
  final _profileDomainController = TextEditingController();
  String hashedProof = '';
  final formKey = GlobalKey<FormState>();
  PageController pageController = PageController();
  bool _isLoading = false;
  bool isLocalAuthRequired = false;

  bool onSavedClaims() {
    if (formKey.currentState != null) {
      final isValid = formKey.currentState!.validate();
      if (isValid) {
        formKey.currentState!.save();
        widget.profileModel.aspProfile.domain =
            _profileDomainController.text;
        widget.profileModel.aspProfile.name =
            _profileNameController.text;
        widget.profileModel.aspProfile.description =
            _profileDescriptionController.text;
        return true;
      } else {
        if (_profileDomainController.text == '') {
          pageController.jumpToPage(2);
        } else {
          pageController.jumpToPage(0);
        }
      }
    }
    return false;
  }

  onNextPage() {
    pageController.nextPage(duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
  }

  generateNewHashedProof() {
    setState(() {
      hashedProof = widget.profileModel.aspProfile.generateHashedProof();
    });
  }

  uploadProfile() async {
    if (onSavedClaims()) {
      await getIt<AspCubit>().uploadProfile(widget.profileModel.aspProfile);
    }
  }

  deleteProfile() async {
    if (widget.isProfileNew) {
      context.pop();
      return;
    }
    AspProfileDeletionType? deletionType = await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (builder) {
          return const DeleteDialog(dialogType: DeleteDialogType.aspProfile);
        });

    if (deletionType != null) {
      if (onSavedClaims()) {
        bool isProfileDeleted = await getIt<AspCubit>().deleteProfile(widget.profileModel, deletionType);
        if (isProfileDeleted) {
          if (!context.mounted) return;
          context.pop();
        }
      }
    }
  }

  exportProfile() async {
    String? password = await showDialog(
        context: context,
        builder: (builder) {
          return PasswordDialog();
        });

    if (password != null) {
      String encryptedSecretKey =
          await widget.profileModel.aspProfile.encryptKeyWithPassword(password);
      if (!context.mounted) return;
      showDialog(
          context: context,
          builder: (builder) {
            return SecretKeyDialog(secretKey: encryptedSecretKey);
          });
    }
  }

  @override
  void initState() {
    _profileDomainController.text = widget.profileModel.aspProfile.domain;
    _profileNameController.text = widget.profileModel.aspProfile.name ?? '';
    _profileDescriptionController.text =
        widget.profileModel.aspProfile.description ?? '';

    if (hashedProof == '') {
      generateNewHashedProof();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      child: Builder(
        builder: (context) {
          return Scaffold(
            backgroundColor: Colors.transparent,
            body: Dialog(
              insetPadding: const EdgeInsets.all(10),
              child: BlocConsumer<AspCubit, AspState>(
                bloc: getIt<AspCubit>(),
                listener: (context, state) async {
                  if (state is UploadProfileSuccess) {
                    context.pop();
                    await getIt<UserCubit>().saveUserProfile(aspProfile: state.profile);
                  }
                  if (!context.mounted) return;
                  if (state is UploadProfileFailure) {
                    FocusScope.of(context).unfocus();
                    KeyoxideSnackBar().showOverlaySnackBar(context, state.error.message.localize);
                  }
                  if (state is DeleteProfileSuccess) {
                    context.pop();
                    KeyoxideSnackBar().showOverlaySnackBar(context, 'user_profile_delete_success'.localize);
                  }
                  if (state is DeleteProfileFailure) {
                    FocusScope.of(context).unfocus();
                    KeyoxideSnackBar().showOverlaySnackBar(context, state.error.message.localize);
                  }
                },
                builder: (context, state) {
                  if (state is AspLoading) {
                    _isLoading = true;
                  } else {
                    _isLoading = false;
                  }
                  return Stack(children: [
                    Column(
                      children: [
                        const SizedBox(
                          height: 20,
                        ),
                        PageViewDotIndicator(
                            currentItem: pageIndex,
                            count: 3,
                            unselectedColor: Colors.grey,
                            selectedColor: Theme.of(context).indicatorColor),
                        Expanded(
                          child: Form(
                            key: formKey,
                            child: PageView(
                                controller: pageController,
                                children: [
                                  ProfileInformationPage(
                                    profile: widget.profileModel.aspProfile,
                                    profileNameController: _profileNameController,
                                    profileDescriptionController:
                                        _profileDescriptionController,
                                    onGenerateHashedProof: generateNewHashedProof,
                                    hashedProof: hashedProof,
                                    onNextPage: onNextPage,
                                  ),
                                  IdentityClaimsPage(
                                    claims: widget.profileModel.aspProfile.claims,
                                    aspProfile: widget.profileModel.aspProfile,
                                    onNextPage: onNextPage,
                                  ),
                                  SecurityPage(
                                    fingerprint:
                                        widget.profileModel.aspProfile.fingerprint,
                                    uri: widget.profileModel.aspProfile.uri.toString(),
                                    profileDomainController: _profileDomainController,
                                    onUploadProfile: uploadProfile,
                                    onDeleteProfile: deleteProfile,
                                    onExportProfile: exportProfile,
                                    onSavedClaims: onSavedClaims,
                                    isProfileNew: widget.isProfileNew,
                                  ),
                                ],
                                onPageChanged: (index) {
                                  setState(() {
                                    pageIndex = index;
                                  });
                                  onSavedClaims();
                                }),
                          ),
                        ),
                      ],
                    ),
                    Align(alignment: Alignment.topRight, child: IconButton(onPressed: () => context.pop(), icon: const Icon(Icons.close_outlined),),),
                    if (_isLoading) const Center(child: CircularProgressIndicator()),
                  ]);
                },
              ),
            ),
          );
        }
      ),
    );
  }
}
