import 'dart:convert';
import 'dart:typed_data';

import 'package:aspe/aspe.dart';
import 'package:equatable/equatable.dart';
import 'package:keyoxide_flutter/features/generate_profile/model/keyoxide_profile.dart';

class AspProfileModel extends Equatable {
  // If local authentication is enabled, try authenticating with fingerprint or default device password.
  final AspProfile aspProfile;
  final Uint8List avatarImage;
  bool isHidden;
  DateTime updatedAt;
  KeyoxideProfile keyoxideProfile;

  AspProfileModel({
    required this.avatarImage,
    required this.aspProfile,
    required this.isHidden,
    required this.updatedAt,
    required this.keyoxideProfile,
  });

  factory AspProfileModel.fromJson(Map<String, dynamic> json) {
    return AspProfileModel(
      aspProfile: AspProfile.fromJson(json["aspProfile"]),
      avatarImage: base64Decode(json['avatarImage']),
      isHidden: json['isHidden'] ?? false,
      updatedAt: DateTime.tryParse(json['updatedAt']) ?? DateTime.now(),
      keyoxideProfile: KeyoxideProfile.fromJson(json["keyoxideProfile"]),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "aspProfile": aspProfile.toJson(),
      "avatarImage": base64Encode(avatarImage),
      "isHidden": isHidden,
      "updatedAt": updatedAt.toIso8601String(),
      "keyoxideProfile": keyoxideProfile.toJson(),
    };
  }

  @override
  List<Object?> get props => [aspProfile, avatarImage, isHidden, updatedAt, keyoxideProfile];
}
