import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/common/widgets/keyoxide_snackbar.dart';
import 'package:keyoxide_flutter/features/user/cubit/user_cubit.dart';

import '../../../common/services/locator_service.dart';
import '../cubit/asp_cubit.dart';

class LoadAspProfileDialog extends StatefulWidget {
  const LoadAspProfileDialog({super.key});

  @override
  State<LoadAspProfileDialog> createState() => _LoadAspProfileDialogState();
}

class _LoadAspProfileDialogState extends State<LoadAspProfileDialog> {
  final formKey = GlobalKey<FormState>();
  final FocusNode _secretKeyFocusNode = FocusNode();
  final FocusNode _aspeServerFocusNode = FocusNode();
  final FocusNode _loadProfileBtnFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();
  String secretKey = '';
  String aspeServer = '';
  String password = '';
  bool _isLoading = false;

  bool _saveForm() {
    if (formKey.currentState != null) {
      final isValid = formKey.currentState!.validate();
      if (isValid) {
        formKey.currentState!.save();
        return true;
      }
    }
    return false;
  }

  Future<void> loadProfile() async {
    if (_saveForm()) {
      await getIt<AspCubit>().importProfile(aspeServer, secretKey, password);
    }
  }

  @override
  void initState() {
    aspeServer =
        getIt<UserCubit>().keyoxideUser.userSettings.verificationApiDomain;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: const EdgeInsets.all(10),
      child: BlocConsumer<AspCubit, AspState>(
        bloc: getIt<AspCubit>(),
        listener: (context, state) {
          if (state is LoadProfileFailure) {
            KeyoxideSnackBar()
                .showOverlaySnackBar(context, state.error.message.localize);
          }
          if (state is LoadProfileSuccess) {
            context.pop();
          }
        },
        builder: (context, state) {
          if (state is AspLoading) {
            _isLoading = true;
          } else {
            _isLoading = false;
          }
          return Stack(children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const SizedBox(height: 10),
                  Text('load_create_profile_dialog_title'.localize,
                      style: Theme.of(context).textTheme.titleLarge),
                  const SizedBox(height: 20),
                  Form(
                      key: formKey,
                      child: Column(
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 8.0),
                            child: TextFormField(
                              initialValue: aspeServer,
                              focusNode: _aspeServerFocusNode,
                              decoration: InputDecoration(
                                border: const OutlineInputBorder(),
                                hintText:
                                    'load_create_profile_dialog_server_hint'
                                        .localize,
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'load_create_profile_dialog_server_empty'
                                      .localize;
                                }
                                return null;
                              },
                              onSaved: (value) {
                                aspeServer = value!;
                              },
                            ),
                          ),
                          const SizedBox(height: 20),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 8.0),
                            child: TextFormField(
                              maxLines: 2,
                              focusNode: _secretKeyFocusNode,
                              decoration: InputDecoration(
                                border: const OutlineInputBorder(),
                                hintText:
                                    'load_create_profile_dialog_secret_key_hint'
                                        .localize,
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'load_create_profile_dialog_secret_key_empty'
                                      .localize;
                                }
                                return null;
                              },
                              onSaved: (value) {
                                secretKey = value!;
                              },
                            ),
                          ),
                          const SizedBox(height: 20),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 8.0),
                            child: TextFormField(
                              focusNode: _passwordFocusNode,
                              obscureText: true,
                              decoration: InputDecoration(
                                  border: const OutlineInputBorder(),
                                  hintText:
                                      'password_dialog_password_hint'.localize),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'password_dialog_empty_password'
                                      .localize;
                                }
                                return null;
                              },
                              onSaved: (value) {
                                password = value!;
                              },
                            ),
                          ),
                        ],
                      )),
                  const SizedBox(height: 20),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Text('load_create_profile_dialog_warning'.localize,
                        textAlign: TextAlign.center),
                  ),
                  const SizedBox(height: 20),
                  FilledButton.icon(
                    focusNode: _loadProfileBtnFocusNode,
                    style: FilledButton.styleFrom(),
                    onPressed: () {
                      //dismiss keyboard
                      FocusScope.of(context).unfocus();
                      loadProfile();
                    },
                    icon: const Icon(Icons.person_search_outlined),
                    label: Text(
                      'load_create_profile_dialog_btn_load_profile'.localize,
                    ),
                  ),
                  const SizedBox(height: 12),
                ],
              ),
            ),
            if (_isLoading) const Center(child: CircularProgressIndicator())
          ]);
        },
      ),
    );
  }
}
