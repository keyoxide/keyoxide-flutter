import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/common/widgets/keyoxide_snackbar.dart';

class SecretKeyDialog extends StatelessWidget {
  const SecretKeyDialog({super.key, required this.secretKey});

  final String secretKey;

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      child: Builder(
        builder: (context) {
          return Scaffold(
            backgroundColor: Colors.transparent,
            body: Dialog(
                insetPadding: const EdgeInsets.all(10),
                child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      const SizedBox(height: 10),
                      Text('secret_key_dialog_title'.localize,
                          style: Theme.of(context).textTheme.titleLarge),
                      const SizedBox(height: 20),
                      Card(
                          child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(child: Text(secretKey)),
                          ],
                        ),
                      )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          TextButton(
                            onPressed: () => FlutterClipboard.copy(secretKey).then(
                                (value) => KeyoxideSnackBar()
                                    .showOverlaySnackBar(context, 'general_txt_add_copied_to_clipboard'.localize)),
                            child: Text('general_txt_copy'.localize),
                          ),
                          const SizedBox(width: 10),
                          TextButton(
                            onPressed: () => context.pop(),
                            child: Text('general_txt_done'.localize),
                          ),
                        ],
                      )
                    ]))),
          );
        }
      ),
    );
  }
}
