import 'package:aspe/aspe.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:go_router/go_router.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:kx_claim_service/kx_claim_service.dart';
import 'package:url_launcher/url_launcher.dart';

enum claimStep {
  input,
  instructions,
  feedback,
}

class ClaimCreationDialog extends StatefulWidget {
  ClaimCreationDialog(
      {super.key, required this.claim, required this.aspProfile});

  final KxClaimDefinition claim;
  AspProfile aspProfile;

  @override
  State<ClaimCreationDialog> createState() => _ClaimCreationDialogState();
}

class _ClaimCreationDialogState extends State<ClaimCreationDialog> {
  final formKey = GlobalKey<FormState>();
  Map<String, String> inputs = {};
  Map<String, String> feedback = {};
  ProofFormat? selectedProofFormat;
  claimStep currentStep = claimStep.input;
  String proofString = '';
  String computedProof = '';

  Widget _textField({
    required String id,
    required String placeholder,
    required String defaultValue,
    required Function validator,
    required String label,
  }) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 15),
      child: TextFormField(
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          hintText: placeholder,
          label: Text(label),
        ),
        validator: (value) {
          String? result = validator(value);
          return result != null ? result.localize : null;
        },
        onSaved: (value) {
          if (currentStep == claimStep.input) {
            inputs.putIfAbsent(id, () => value!);
          } else if (currentStep == claimStep.feedback) {
            feedback.putIfAbsent(id, () => value!);
          }
        },
      ),
    );
  }

  bool validateInputs() {
    if (formKey.currentState != null) {
      final isValid = formKey.currentState!.validate();
      if (isValid) {
        formKey.currentState!.save();
        return true;
      }
    }
    return false;
  }

  void constructProofString() {
    selectedProofFormat == ProofFormat.direct
        ? proofString = widget.aspProfile.uri.toString()
        : proofString = widget.aspProfile.generateHashedProof();
  }

  Widget _buildProofChoices() {
    if (widget.claim.supportedProofFormats.length == 1) {
      return Text(
          '${'claim_creation_proof_format'.localize} ${widget.claim.supportedProofFormats.first}');
    }
    if (widget.claim.supportedProofFormats.length == 2) {
      return Column(
        children: <Widget>[
          ListTile(
            visualDensity: VisualDensity.compact,
            title: Text('claim_creation_proof_direct'.localize),
            leading: Radio(
              value: ProofFormat.direct,
              groupValue: selectedProofFormat,
              onChanged: (ProofFormat? value) {
                setState(() {
                  selectedProofFormat = value!;
                  constructProofString();
                });
              },
            ),
          ),
          ListTile(
            visualDensity: VisualDensity.compact,
            title: Text('claim_creation_proof_hashed'.localize),
            leading: Radio(
              value: ProofFormat.hashed,
              groupValue: selectedProofFormat,
              onChanged: (ProofFormat? value) {
                setState(() {
                  selectedProofFormat = value!;
                  constructProofString();
                });
              },
            ),
          ),
        ],
      );
    }
    return Text('claim_creation_proof_not_available'.localize);
  }

  Widget _buildInputFields() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.network(widget.claim.iconUrl!,
                height: 20,
                width: 20,
                color: Theme.of(context).colorScheme.primary),
            const SizedBox(width: 10),
            Text(widget.claim.name,
                style: Theme.of(context).textTheme.bodyLarge),
          ],
        ),
        const SizedBox(height: 20),
        if (widget.claim.translationKeyClaimPre != null) ...[
          Text(widget.claim.translationKeyClaimPre.localize),
          const SizedBox(height: 20),
        ],
        Form(
            key: formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: widget.claim.inputs
                  .map((input) => _textField(
                        id: input.id,
                        placeholder: input.placeholder,
                        defaultValue: input.defaultValue ?? '',
                        validator: input.validator,
                        label: input.labelTranslationKey.localize,
                      ))
                  .toList(),
            )),
        const SizedBox(height: 20),
        if (widget.claim.translationKeyClaimPost != null) ...[
          Text(widget.claim.translationKeyClaimPost!.localize),
          const SizedBox(height: 20),
        ],
        Align(
          alignment: Alignment.centerRight,
          child: TextButton(
            onPressed: () {
              if (validateInputs()) {
                setState(() {
                  currentStep = claimStep.instructions;
                });
              }
            },
            child: Text('general_txt_continue'.localize),
          ),
        )
      ],
    );
  }

  Widget _buildInstructionFields() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.network(widget.claim.iconUrl!,
                height: 20,
                width: 20,
                color: Theme.of(context).colorScheme.primary),
            const SizedBox(width: 10),
            Text(widget.claim.name,
                style: Theme.of(context).textTheme.bodyLarge),
          ],
        ),
        const SizedBox(height: 20),
        if (widget.claim.translationKeyProofPre != null) ...[
          HtmlWidget(
            widget.claim.translationKeyProofPre!.localize,
            onTapUrl: (url) => launchUrl(Uri.parse(url)),
          ),
          const SizedBox(height: 20),
        ],
        _buildProofChoices(),
        const SizedBox(height: 20),
        Card(
            child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(child: Text(proofString)),
              IconButton(
                  onPressed: () => FlutterClipboard.copy(proofString),
                  icon: const Icon(Icons.copy_all_outlined))
            ],
          ),
        )),
        const SizedBox(height: 20),
        if (widget.claim.translationKeyProofPost != null) ...[
          Text(widget.claim.translationKeyProofPost!.localize),
          const SizedBox(height: 20),
        ],
        Align(
          alignment: Alignment.centerRight,
          child: TextButton(
            onPressed: () {
              if (widget.claim.requiresFeedbackStep) {
                setState(() {
                  currentStep = claimStep.feedback;
                });
              } else {
                computedProof = widget.claim.computeClaim(inputs, feedback);
                context.pop(computedProof);
              }
            },
            child: Text(widget.claim.requiresFeedbackStep
                ? 'general_txt_continue'.localize
                : 'general_txt_add'.localize),
          ),
        )
      ],
    );
    ;
  }

  Widget _buildFeedbackFields() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.network(widget.claim.iconUrl!,
                height: 20,
                width: 20,
                color: Theme.of(context).colorScheme.primary),
            const SizedBox(width: 10),
            Text(widget.claim.name,
                style: Theme.of(context).textTheme.bodyLarge),
          ],
        ),
        const SizedBox(height: 20),
        if (widget.claim.translationKeyFeedbackPre != null) ...[
          Text(widget.claim.translationKeyFeedbackPre!.localize),
          const SizedBox(height: 20),
        ],
        Form(
            key: formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: widget.claim.feedbackInputs
                  .map((input) => _textField(
                        id: input.id,
                        placeholder: input.placeholder,
                        defaultValue: input.defaultValue ?? '',
                        validator: input.validator,
                        label: input.labelTranslationKey.localize,
                      ))
                  .toList(),
            )),
        const SizedBox(height: 20),
        if (widget.claim.translationKeyFeedbackPost != null) ...[
          Text(widget.claim.translationKeyFeedbackPost!.localize),
          const SizedBox(height: 20),
        ],
        Align(
          alignment: Alignment.centerRight,
          child: TextButton(
            onPressed: () {
              if (validateInputs()) {
                computedProof = widget.claim.computeClaim(inputs, feedback);
                context.pop(computedProof);
              }
            },
            child: Text('general_txt_add'.localize),
          ),
        )
      ],
    );
  }

  Widget getStepFields() {
    switch (currentStep) {
      case claimStep.input:
        return _buildInputFields();
      case claimStep.instructions:
        return _buildInstructionFields();
      case claimStep.feedback:
        return _buildFeedbackFields();
    }
  }

  @override
  void initState() {
    if (widget.claim.supportedProofFormats.isNotEmpty) {
      selectedProofFormat = widget.claim.supportedProofFormats.first;
    }
    proofString = widget.aspProfile.uri.toString();
    currentStep = widget.claim.requiresInputStep
        ? claimStep.input
        : claimStep.instructions;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: const EdgeInsets.all(10),
      child: Column(mainAxisSize: MainAxisSize.min,
        children: [
          Stack(
              children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: getStepFields(),
            ),
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                onPressed: () => context.pop(),
                icon: const Icon(Icons.close_outlined),
              ),
            ),
          ]),
        ],
      ),
    );
  }
}
