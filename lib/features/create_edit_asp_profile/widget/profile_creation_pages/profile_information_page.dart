import 'package:aspe/aspe.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';

class ProfileInformationPage extends StatefulWidget {
  ProfileInformationPage({
    super.key,
    required this.profileNameController,
    required this.profileDescriptionController,
    required this.profile,
    required this.hashedProof,
    required this.onGenerateHashedProof,
    required this.onNextPage,
  });

  TextEditingController profileNameController;
  TextEditingController profileDescriptionController;
  AspProfile profile;
  String hashedProof;
  VoidCallback onGenerateHashedProof;
  VoidCallback onNextPage;

  @override
  State<ProfileInformationPage> createState() => _ProfileInformationPageState();
}

class _ProfileInformationPageState extends State<ProfileInformationPage> {
  final FocusNode _profileNameFocusNode = FocusNode();
  final FocusNode _profileDescriptionFocusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              Column(
                children: [
                  Text('asp_create_profile_information_title'.localize,
                      style: Theme.of(context).textTheme.titleLarge),
                  const SizedBox(
                    height: 20,
                  ),
                  Column(children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: TextFormField(
                        maxLines: 1,
                        controller: widget.profileNameController,
                        focusNode: _profileNameFocusNode,
                        decoration: InputDecoration(
                          border: const OutlineInputBorder(),
                          hintText:
                              'asp_create_profile_information_name'.localize,
                        ),
                        validator: (value) {
                          if (value! == '' || value.isEmpty) {
                            return 'general_txt_error_empty_field'.localize;
                          }
                          return null;
                        },
                      ),
                    ),
                    const SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: TextFormField(
                        controller: widget.profileDescriptionController,
                        maxLines: 3,
                        focusNode: _profileDescriptionFocusNode,
                        decoration: InputDecoration(
                          border: const OutlineInputBorder(),
                          hintText:
                              'asp_create_profile_information_desc'.localize,
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    const Text('URI:'),
                    Row(
                      children: [
                        Expanded(
                          child: Card(
                              child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Flexible(
                                  child: Text(widget.profile.uri.toString()),
                                ),
                                IconButton(
                                    onPressed: () => FlutterClipboard.copy(
                                        widget.profile.uri.toString()),
                                    icon: const Icon(Icons.copy_all_outlined))
                              ],
                            ),
                          )),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    Text('asp_create_profile_information_uri_instruction'
                        .localize),
                    const SizedBox(height: 20),
                    const Divider(indent: 15, endIndent: 15),

                    // Text('Identity proofs',
                    //     style: Theme.of(context).textTheme.titleLarge),
                    // SizedBox(height: 20),
                    // Text(
                    //   'Use a "direct proof" if you want people to find your profile. Use a "hashed proof" to hide your profile. Never use the same "hashed proof" twice!',
                    //   textAlign: TextAlign.center,
                    // ),
                    // SizedBox(height: 20),
                    // Text('Direct proof:'),
                    // Card(
                    //     child: Padding(
                    //   padding: const EdgeInsets.all(8.0),
                    //   child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //     children: [
                    //       Flexible(child: Text(widget.profile.uri.toString())),
                    //       IconButton(onPressed: () => FlutterClipboard.copy(widget.profile.uri.toString()), icon: Icon(Icons.copy_all_outlined))
                    //     ],
                    //   ),
                    // )),
                    // Text('Hashed proof:'),
                    // Card(
                    //     child: Padding(
                    //   padding: const EdgeInsets.all(8.0),
                    //   child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //     children: [
                    //       Flexible(child: Text(widget.hashedProof)),
                    //       IconButton(onPressed: () => FlutterClipboard.copy(widget.hashedProof), icon: Icon(Icons.copy_all_outlined))
                    //     ],
                    //   ),
                    // )),
                    // SizedBox(height: 20),
                    // FilledButton.icon(
                    //   style: FilledButton.styleFrom(),
                    //   onPressed: () {
                    //       widget.onGenerateHashedProof();
                    //   },
                    //   icon: const Icon(Icons.generating_tokens_outlined),
                    //   label: Text(
                    //     'Generate new proofs',
                    //   ),
                    // ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: TextButton(
                          onPressed: () => widget.onNextPage(),
                          child: Text('general_txt_next'.localize)),
                    ),
                    const SizedBox(height: 20),
                  ]),
                ],
              ),
            ],
          )),
    );
  }
}
