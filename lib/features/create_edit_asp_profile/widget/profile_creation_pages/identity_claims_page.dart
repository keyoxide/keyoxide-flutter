import 'package:aspe/aspe.dart';
import 'package:flutter/material.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/features/create_edit_asp_profile/cubit/asp_cubit.dart';
import 'package:keyoxide_flutter/features/create_edit_asp_profile/widget/claim_creation_dialog.dart';
import 'package:kx_claim_service/kx_claim_service.dart';

import '../../../../common/services/locator_service.dart';
import '../claims_grid_dialog.dart';

class IdentityClaimsPage extends StatefulWidget {
  const IdentityClaimsPage(
      {super.key,
      required this.claims,
      required this.aspProfile,
      required this.onNextPage});

  final List<String> claims;
  final AspProfile aspProfile;
  final VoidCallback onNextPage;

  @override
  State<IdentityClaimsPage> createState() => _IdentityClaimsPageState();
}

class _IdentityClaimsPageState extends State<IdentityClaimsPage> {
  Widget claimCard(String claim) {
    return Row(
      children: [
        Expanded(
          child: Card(
              child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: SingleChildScrollView(
                scrollDirection: Axis.horizontal, child: Text(claim)),
          )),
        ),
        IconButton(
          onPressed: () {
            setState(() {
              widget.claims.remove(claim);
            });
          },
          icon: const Icon(
            Icons.delete_outlined,
            color: Colors.red,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            Text('asp_create_profile_identity_claims_title'.localize,
                style: Theme.of(context).textTheme.titleLarge),
            if (widget.claims.isEmpty) ...[
              const SizedBox(height: 20),
              Text('asp_create_identity_claims_empty'.localize),
            ],
            const SizedBox(height: 20),
            ...widget.claims.map((claim) => claimCard(claim)),
            const SizedBox(height: 20),
            FilledButton.icon(
              style: FilledButton.styleFrom(),
              onPressed: () async {
                List<KxClaimDefinition> serviceProviderList =
                    await getIt<AspCubit>().getAllServiceProviders();

                if (!context.mounted) return;

                KxClaimDefinition? claim = await showDialog(
                    context: context,
                    builder: (builder) {
                      return ClaimsGridDialog(
                        serviceProviderList: serviceProviderList,
                      );
                    });

                if (!context.mounted) return;
                if (claim != null) {
                  String? claimText = await showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (builder) {
                        return ClaimCreationDialog(
                            claim: claim, aspProfile: widget.aspProfile);
                      });
                  if (claimText != null) {
                    setState(() {
                      widget.claims.add(claimText);
                    });
                  }
                }
              },
              icon: const Icon(Icons.add_outlined),
              label: Text(
                'asp_create_profile_identity_claims_btn_add_claim'.localize,
              ),
            ),
            const SizedBox(height: 10),
            const Divider(indent: 15, endIndent: 15),
            const SizedBox(height: 10),
            Align(
              alignment: Alignment.centerRight,
              child: TextButton(
                  onPressed: () => widget.onNextPage(),
                  child: Text('general_txt_next'.localize)),
            ),
          ],
        ),
      ),
    );
  }
}
