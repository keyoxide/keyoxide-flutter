import 'package:flutter/material.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/features/create_edit_asp_profile/widget/save_and_upload_buttons.dart';

class SecurityPage extends StatefulWidget {
  const SecurityPage({
    super.key,
    required this.fingerprint,
    required this.uri,
    required this.profileDomainController,
    required this.onUploadProfile,
    required this.onDeleteProfile,
    required this.onExportProfile,
    required this.onSavedClaims,
    required this.isProfileNew,
  });

  final String fingerprint;
  final String uri;
  final TextEditingController profileDomainController;
  final VoidCallback onUploadProfile;
  final VoidCallback onDeleteProfile;
  final VoidCallback onExportProfile;
  final VoidCallback onSavedClaims;
  final bool isProfileNew;

  @override
  State<SecurityPage> createState() => _SecurityPageState();
}

class _SecurityPageState extends State<SecurityPage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(children: [
            Text('asp_create_profile_identity_security_title'.localize,
                style: Theme.of(context).textTheme.titleLarge),
            const SizedBox(height: 20),
            Text(
              'asp_create_profile_identity_security_instruction'.localize,
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: TextFormField(
                maxLines: 1,
                controller: widget.profileDomainController,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  hintText:
                      'asp_create_profile_identity_security_title'.localize,
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'asp_create_profile_identity_security_domain_empty'
                        .localize;
                  }
                  return null;
                },
              ),
            ),
            const SizedBox(height: 20),
            Text(
              'asp_create_profile_identity_security_warning'.localize,
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 20),
            const Divider(indent: 15, endIndent: 15),
            const SizedBox(height: 20),
            SaveAndUploadButtons(
              onUploadProfile: widget.onUploadProfile,
              onDeleteProfile: widget.onDeleteProfile,
              onExportProfile: widget.onExportProfile,
              onSavedClaims: widget.onSavedClaims,
              isProfileNew: widget.isProfileNew,
            ),
          ])),
    );
  }
}
