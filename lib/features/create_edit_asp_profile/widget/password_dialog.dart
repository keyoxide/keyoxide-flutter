import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';

class PasswordDialog extends StatefulWidget {
  const PasswordDialog({super.key});

  @override
  State<PasswordDialog> createState() => _PasswordDialogState();
}

class _PasswordDialogState extends State<PasswordDialog> {
  final formKey = GlobalKey<FormState>();
  final FocusNode _passwordFocusNode = FocusNode();
  final FocusNode _confirmPasswordFocusNode = FocusNode();
  final FocusNode _confirmBtnFocusNode = FocusNode();
  String password = '';
  String confirmPassword = '';

  bool _saveForm() {
    if (formKey.currentState != null) {
      final isValid = formKey.currentState!.validate();
      if (isValid) {
        formKey.currentState!.save();
        return true;
      }
    }
    return false;
  }

  void confirmEnteredPassword() {
    context.pop(password);
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: const EdgeInsets.all(10),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(height: 10),
            Text('password_dialog_set_password_title'.localize,
                style: Theme.of(context).textTheme.titleLarge),
            const SizedBox(height: 20),
            Form(
              key: formKey,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: TextFormField(
                      obscureText: true,
                      focusNode: _passwordFocusNode,
                      decoration: InputDecoration(
                        border: const OutlineInputBorder(),
                        hintText: 'password_dialog_password_hint'.localize,
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'password_dialog_empty_password'.localize;
                        }
                        return null;
                      },
                      onChanged: (value) {
                        password = value;
                      },
                    ),
                  ),
                  const SizedBox(height: 20),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: TextFormField(
                      obscureText: true,
                      focusNode: _confirmPasswordFocusNode,
                      decoration: InputDecoration(
                        border: const OutlineInputBorder(),
                        hintText: 'password_dialog_confirm_hint'.localize,
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'password_dialog_empty_password'.localize;
                        }
                        if (password != value) {
                          return 'password_dialog_password_not_match'.localize;
                        }
                        return null;
                      },
                      onChanged: (value) {
                        confirmPassword = value;
                      },
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: FilledButton.icon(
                focusNode: _confirmBtnFocusNode,
                style: FilledButton.styleFrom(),
                onPressed: () {
                  if (_saveForm()) {
                    if (password == confirmPassword && password != '') {
                      context.pop(password);
                    }
                  }
                },
                icon: const Icon(Icons.password_outlined),
                label: Text(
                  'password_dialog_btn_confirm'.localize,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
