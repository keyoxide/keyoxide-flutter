import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:kx_claim_service/kx_claim_service.dart';

class ClaimsGridDialog extends StatefulWidget {
  const ClaimsGridDialog({super.key, required this.serviceProviderList});

  final List<KxClaimDefinition> serviceProviderList;

  @override
  State<ClaimsGridDialog> createState() => _ClaimsGridDialogState();
}

class _ClaimsGridDialogState extends State<ClaimsGridDialog> {
  Widget claimTile(KxClaimDefinition claim) {
    return GestureDetector(
        onTap: () {
          context.pop(claim);
        },
        child: Container(
          decoration: BoxDecoration(
              border: Border.all(color: Theme.of(context).colorScheme.primary),
              borderRadius: BorderRadius.circular(10)),
          child: Column(
            children: [
              const SizedBox(height: 13.3),
              SvgPicture.network(claim.iconUrl!,
                  height: 50,
                  width: 50,
                  color: Theme.of(context).colorScheme.primary),
              const SizedBox(height: 10),
              Text(
                claim.name,
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        insetPadding: const EdgeInsets.all(10),
        child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(height: 10),
                Text('claim_creation_title_available_claims'.localize,
                    style: Theme.of(context).textTheme.titleLarge),
                const SizedBox(height: 20),
                Expanded(
                  child: GridView.count(
                    shrinkWrap: true,
                    childAspectRatio: 1.7,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    semanticChildCount: widget.serviceProviderList.length,
                    crossAxisCount: 2,
                    children: widget.serviceProviderList
                        .map((claim) => claimTile(claim))
                        .toList(),
                  ),
                ),
              ],
            )));
  }
}
