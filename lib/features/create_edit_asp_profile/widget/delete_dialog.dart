import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/features/create_edit_asp_profile/cubit/asp_cubit.dart';

class DeleteDialog extends StatelessWidget {
  const DeleteDialog({super.key, required this.dialogType});

  final DeleteDialogType dialogType;

  Widget _buildAspDialog(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        const SizedBox(height: 10),
        Text(
          'delete_profile_are_you_sure'.localize,
          style: Theme.of(context).textTheme.headlineSmall,
          textAlign: TextAlign.center,
        ),
        const SizedBox(height: 20),
        Text('delete_profile_from'.localize,
            style: Theme.of(context).textTheme.bodyMedium),
        const SizedBox(height: 10),
        FittedBox(
          child: Row(children: [
            FilledButton(
              onPressed: () {
                context.pop(AspProfileDeletionType.fromServer);
              },
              child: Text('general_txt_server'.localize),
            ),
            const SizedBox(width: 10),
            FilledButton(
              onPressed: () {
                context.pop(AspProfileDeletionType.fromDevice);
              },
              child: Text('general_txt_device'.localize),
            ),
            const SizedBox(width: 10),
            FilledButton(
              onPressed: () {
                context.pop(AspProfileDeletionType.fromBoth);
              },
              child: Text('general_txt_both'.localize),
            ),
          ]),
        )
      ],
    );
  }

  Widget _buildPgpDialog(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        const SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text('delete_profile_are_you_sure'.localize,
              style: Theme.of(context).textTheme.titleLarge,
              textAlign: TextAlign.center),
        ),
        const SizedBox(height: 20),
        Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          TextButton(
            onPressed: () {
              context.pop(true);
            },
            child: Text('general_txt_yes'.localize),
          ),
          TextButton(
            onPressed: () {
              context.pop(false);
            },
            child: Text('general_txt_no'.localize),
          ),
        ])
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: const EdgeInsets.all(10),
      child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: dialogType == DeleteDialogType.aspProfile
              ? _buildAspDialog(context)
              : _buildPgpDialog(context)),
    );
  }
}

enum DeleteDialogType {
  pgpProfile,
  aspProfile,
}
