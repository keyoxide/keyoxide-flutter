import 'package:flutter/material.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';

class SaveAndUploadButtons extends StatefulWidget {
  SaveAndUploadButtons({
    super.key,
    required this.onUploadProfile,
    required this.onDeleteProfile,
    required this.onExportProfile,
    required this.onSavedClaims,
    required this.isProfileNew,
  });

  final VoidCallback onUploadProfile;
  final VoidCallback onDeleteProfile;
  final VoidCallback onExportProfile;
  final Function onSavedClaims;
  final bool isProfileNew;

  @override
  State<SaveAndUploadButtons> createState() => _SaveAndUploadButtonsState();
}

class _SaveAndUploadButtonsState extends State<SaveAndUploadButtons> {
  @override
  Widget build(BuildContext context) {
    return FittedBox(
      child: Row(
        children: [
          if (!widget.isProfileNew) ...[
            OutlinedButton.icon(
              style: FilledButton.styleFrom(),
              onPressed: () {
                if (widget.onSavedClaims()) {
                  widget.onExportProfile();
                }
              },
              icon: const Icon(Icons.import_export_outlined),
              label: Text(
                'general_txt_export'.localize,
              ),
            ),
            const SizedBox(width: 10),
          ],
          OutlinedButton.icon(
            style: FilledButton.styleFrom(),
            onPressed: () {
              widget.onDeleteProfile();
            },
            icon: const Icon(Icons.delete_outlined),
            label: Text(
              'general_txt_delete'.localize,
            ),
          ),
          const SizedBox(width: 10),
          FilledButton.icon(
            style: FilledButton.styleFrom(),
            onPressed: () {
              if (widget.onSavedClaims()) {
                widget.onUploadProfile();
              }
            },
            icon: const Icon(Icons.upload_outlined),
            label: Text(
              'general_txt_upload'.localize,
            ),
          ),
        ],
      ),
    );
  }
}
