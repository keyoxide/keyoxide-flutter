import 'package:flutter/material.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/features/settings/model/server_status_model.dart';
import 'package:super_tooltip/super_tooltip.dart';

import '../../../common/services/locator_service.dart';
import '../../../common/utils/utilities.dart';
import '../cubit/settings_cubit.dart';

class ServerVersionStatusWidget extends StatefulWidget {
  const ServerVersionStatusWidget({super.key, required this.serverStatus});

  final ServerStatusModel serverStatus;

  @override
  State<ServerVersionStatusWidget> createState() =>
      _ServerVersionStatusWidgetState();
}

class _ServerVersionStatusWidgetState extends State<ServerVersionStatusWidget> {
  final _tooltipController = SuperTooltipController();

  Widget _buildStatusText() {
    Color color = Theme.of(context).textTheme.bodyMedium!.color!;
    String text = 'settings_network_server_info'.localize;
    // if (widget.serverStatus.serverCheckError) {
    //   text = 'settings_network_server_check_failed'.localize;
    //   color = Colors.red;
    // }
    return Padding(
      padding: const EdgeInsets.only(left: 5.0),
      child: Text(
        text,
        style: Theme.of(context).textTheme.bodyMedium?.copyWith(color: color),
      ),
    );
  }

  Widget _buildTooltipWidget() {
    bool isUpToDate = widget.serverStatus.isUserServerUpToDate;
    String formattedLastChecked = Utilities.formatDateTimeToLocal(
        context, widget.serverStatus.lastCheckedAt);
    return Stack(children: [
      Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
                '${'settings_network_label_server_version'.localize} ${widget.serverStatus.userServerVersion}',
                style: Theme.of(context).textTheme.bodyMedium),
            Text(
                '${'settings_network_label_latest_version'.localize} ${widget.serverStatus.latestServerVersion}',
                style: Theme.of(context).textTheme.bodyMedium),
            Text(
                '${'settings_network_label_last_check'.localize} $formattedLastChecked',
                style: Theme.of(context).textTheme.bodyMedium),
            Text(
                widget.serverStatus.serverCheckError
                    ? 'settings_network_server_check_failed'.localize
                    : isUpToDate
                        ? 'general_txt_up_to_date'.localize
                        : 'settings_network_server_update_available'.localize,
                style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                    color: (widget.serverStatus.serverCheckError || !isUpToDate)
                        ? Colors.amber
                        : Colors.green)),
          ]),
      Positioned(
          right: -15,
          top: -15,
          child: IconButton(
            iconSize: 20,
            icon: const Icon(Icons.refresh_outlined),
            onPressed: () async =>
                await getIt<SettingsCubit>().checkServerVersion(),
          ))
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _tooltipController.showTooltip(),
      child: Padding(
        padding: const EdgeInsets.only(right: 5.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            _buildStatusText(),
            const SizedBox(width: 5),
            SuperTooltip(
                popupDirection: TooltipDirection.up,
                barrierColor: Colors.transparent,
                showBarrier: true,
                controller: _tooltipController,
                content: _buildTooltipWidget(),
                child: const Icon(
                  Icons.info_outline,
                  size: 20,
                ))
          ],
        ),
      ),
    );
  }
}
