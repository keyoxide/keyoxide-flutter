import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:internet_connection_checker_plus/internet_connection_checker_plus.dart';
import 'package:keyoxide_flutter/common/utils/debouncer.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/common/widgets/keyoxide_snackbar.dart';
import 'package:keyoxide_flutter/features/settings/cubit/settings_cubit.dart';
import 'package:keyoxide_flutter/features/settings/widget/system_settings_widget.dart';
import 'package:string_validator/string_validator.dart';

import '../../../common/services/locator_service.dart';
import '../../user/cubit/user_cubit.dart';

enum ValidationStatus {
  validating,
  valid,
  invalid,
}

class DomainDialog extends StatefulWidget {
  const DomainDialog({super.key, required this.domainType});

  final DomainType domainType;

  @override
  State<DomainDialog> createState() => _DomainDialogState();
}

class _DomainDialogState extends State<DomainDialog> {
  TextEditingController domainController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  ValidationStatus validationStatus = ValidationStatus.valid;
  bool _isDomainValid = true;
  final _debouncer = Debouncer(milliseconds: 1000);

  Widget _buildValidationStatus(ValidationStatus status) {
    switch (status) {
      case ValidationStatus.validating:
        return const SizedBox(
          height: 20.0,
          width: 20.0,
          child: Center(
            child: CircularProgressIndicator(
              strokeWidth: 2.0,
            ),
          ),
        );

      case ValidationStatus.invalid:
        return const Icon(Icons.close_outlined, color: Colors.red);
      case ValidationStatus.valid:
        return _isDomainValid
            ? const Icon(Icons.check_circle_outline, color: Colors.green)
            : const Icon(Icons.close_outlined, color: Colors.red);
    }
  }

  String? domainValidator(String url) {
    if (url.isEmpty || url == '') {
      _isDomainValid = false;
      return 'settings_system_domain'.localize;
    }
    if (!isFQDN(url)) {
      _isDomainValid = false;
      return 'settings_system_domain_error_invalid_domain'.localize;
    }
    _isDomainValid = true;
    return null;
  }

  Future<void> _validateDomain() async {
    if (formKey.currentState != null) {
      final isValid = formKey.currentState!.validate();
      if (isValid && _isDomainValid) {
        _debouncer.run(() async {
          widget.domainType == DomainType.verificationApiDomain
              ? await getIt<SettingsCubit>()
                  .validateVerificationDomain(domainController.text)
              : await getIt<SettingsCubit>()
                  .validateASPEDomain(domainController.text);
        });
      }
      // If the domain is invalid, set state to change the icon.
      else if (!_isDomainValid) {
        setState(() {});
      }
    }
  }

  @override
  void initState() {
    domainController.text =
        widget.domainType == DomainType.verificationApiDomain
            ? getIt<UserCubit>().keyoxideUser.userSettings.verificationApiDomain
            : getIt<UserCubit>().keyoxideUser.userSettings.aspeApiDomain;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        insetPadding: const EdgeInsets.all(10),
        child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              const SizedBox(height: 10),
              Text('settings_system_domain'.localize,
                  style: Theme.of(context).textTheme.titleLarge),
              const SizedBox(height: 20),
              Form(
                  key: formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: (url) => domainValidator(url!),
                          keyboardType: TextInputType.url,
                          textInputAction: TextInputAction.done,
                          maxLines: 1,
                          controller: domainController,
                          decoration: InputDecoration(
                              suffixIcon:
                                  BlocBuilder<SettingsCubit, SettingsState>(
                                bloc: getIt<SettingsCubit>(),
                                builder: (context, state) {
                                  if (state is DomainValidating) {
                                    validationStatus =
                                        ValidationStatus.validating;
                                  }
                                  if (state is DomainValidationSuccess) {
                                    validationStatus = ValidationStatus.valid;
                                  }
                                  if (state is DomainValidationFailure) {
                                    validationStatus = ValidationStatus.invalid;
                                  }
                                  return _buildValidationStatus(
                                      validationStatus);
                                },
                              ),
                              border: const OutlineInputBorder(),
                              hintText: 'domain.tld'),
                          onChanged: (url) async => await _validateDomain(),
                          // validator: (value) {
                          //   if (value!.isEmpty) {
                          //     return 'asp_create_profile_identity_security_domain_empty'
                          //         .localize;
                          //   }
                          //   return null;
                          // },
                        ),
                      ),
                      const SizedBox(height: 20),
                    ],
                  )),
              Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                TextButton(
                  onPressed: () {
                    context.pop();
                  },
                  child: Text('general_txt_cancel'.localize),
                ),
                TextButton(
                  onPressed: () async {
                    if (validationStatus == ValidationStatus.valid &&
                        _isDomainValid) {
                      widget.domainType == DomainType.verificationApiDomain
                          ? await getIt<SettingsCubit>()
                              .changeVerificationDomain(domainController.text)
                          : await getIt<SettingsCubit>()
                              .changeASPEDomain(domainController.text);
                      if (!context.mounted) return;
                      context.pop(domainController.text);
                    } else if ((validationStatus == ValidationStatus.invalid ||
                            validationStatus == ValidationStatus.validating) &&
                        _isDomainValid == true) {
                      FocusScope.of(context).unfocus();
                      bool hasInternet =
                          await InternetConnection().hasInternetAccess;
                      String errorMessage = hasInternet
                          ? 'settings_system_domain_error_invalid_domain'
                          : 'dio_exception_connection_error';

                      KeyoxideSnackBar()
                          .showOverlaySnackBar(context, errorMessage.localize);
                    }
                  },
                  child: Text('general_txt_done'.localize),
                ),
              ])
            ])));
  }
}
