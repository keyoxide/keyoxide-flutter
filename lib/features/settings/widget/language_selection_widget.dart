import 'package:flutter/material.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/features/settings/cubit/settings_cubit.dart';
import 'package:keyoxide_flutter/features/user/cubit/user_cubit.dart';

import '../../../common/services/locator_service.dart';

enum Languages {
  system('System', Locale('system')),
  english('English', Locale('en')),
  german('deutsch', Locale('de')),
  spanish('Español', Locale('es')),
  french('Francés', Locale('fr')),
  galician('Galician', Locale('gl')),
  dutch('Nederlands', Locale('nl')),
  polish('Polski', Locale('pl')),
  portuguese('Português', Locale('pt')),
  turkish('Türkçe', Locale('tr')),
  chinese('中文', Locale('zh')),
  japanese('日本語', Locale('ja'));

  const Languages(this.label, this.locale);

  final String label;
  final Locale locale;
}

class LanguageSelectionWidget extends StatefulWidget {
  const LanguageSelectionWidget({super.key});

  @override
  State<LanguageSelectionWidget> createState() =>
      _LanguageSelectionWidgetState();
}

class _LanguageSelectionWidgetState extends State<LanguageSelectionWidget> {
  TextEditingController languageController = TextEditingController();
  late Languages selectedLanguage;

  @override
  void initState() {
    String userLangString =
        getIt<UserCubit>().keyoxideUser.userSettings.language;
    selectedLanguage = userLangString == ''
        ? Languages.system
        : Languages.values.byName(userLangString);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      decoration: BoxDecoration(
        border: Border.all(
            style: BorderStyle.solid, color: Theme.of(context).dividerColor),
        borderRadius: BorderRadius.circular(30),
      ),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Padding(
          padding: const EdgeInsets.only(left: 17.0),
          child: Text('settings_system_language'.localize),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 17.0),
          child: PopupMenuButton<Languages>(
            itemBuilder: (context) {
              return Languages.values.map((lang) {
                String label = lang == Languages.system
                    ? 'settings_theming_system'.localize
                    : lang.label;
                return PopupMenuItem(
                  value: lang,
                  child: Text(label),
                );
              }).toList();
            },
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(selectedLanguage.label),
                const Icon(Icons.arrow_drop_down),
              ],
            ),
            onSelected: (lang) async {
              selectedLanguage = lang;
              await getIt<SettingsCubit>().changeLanguage(lang);
            },
          ),
        )
      ]),
    );
  }
}
