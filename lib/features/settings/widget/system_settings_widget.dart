import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/features/settings/cubit/settings_cubit.dart';
import 'package:keyoxide_flutter/features/settings/model/server_status_model.dart';
import 'package:keyoxide_flutter/features/settings/widget/language_selection_widget.dart';
import 'package:keyoxide_flutter/features/settings/widget/server_status_widget.dart';
import 'package:keyoxide_flutter/features/settings/widget/settings_switch.dart';

import '../../../common/services/locator_service.dart';
import '../../../common/widgets/keyoxide_snackbar.dart';
import '../../authentication/cubit/local_authentication_cubit.dart';
import '../../user/cubit/user_cubit.dart';
import 'domain_dialog.dart';

enum DomainType {
  aspeApiDomain,
  verificationApiDomain,
}

class SystemSettingsWidget extends StatefulWidget {
  const SystemSettingsWidget({super.key});

  @override
  State<SystemSettingsWidget> createState() => _SystemSettingsWidgetState();
}

class _SystemSettingsWidgetState extends State<SystemSettingsWidget> {
  bool isLocalAuthenticationEnabled = false;
  String verificationDomain = '';
  String aspeDomain = '';
  ServerStatusModel? verificationServerStatus;
  ServerStatusModel? aspeServerStatus;
  bool _isServerCheckInProgress = false;
  late bool _showHiddenProfiles;
  late bool _isServerUpdateCheckDisabled;
  late bool _isAnimationsDisabled;

  @override
  void initState() {
    isLocalAuthenticationEnabled = getIt<UserCubit>()
        .keyoxideUser
        .userSettings
        .isLocalAuthenticationEnabled;
    verificationDomain =
        getIt<UserCubit>().keyoxideUser.userSettings.verificationApiDomain;
    aspeDomain = getIt<UserCubit>().keyoxideUser.userSettings.aspeApiDomain;
    _showHiddenProfiles =
        getIt<UserCubit>().keyoxideUser.userSettings.showHiddenProfiles;
    _isServerUpdateCheckDisabled =
        getIt<UserCubit>().keyoxideUser.userSettings.disableServerUpdateCheck;
    _isAnimationsDisabled =
        getIt<UserCubit>().keyoxideUser.userSettings.disableAnimations;
    verificationServerStatus =
        getIt<UserCubit>().keyoxideUser.userSettings.verificationServerStatus;
    aspeServerStatus =
        getIt<UserCubit>().keyoxideUser.userSettings.aspeServerStatus;
    super.initState();
  }

  Widget domainCard(DomainType domainType) {
    return Column(children: [
      Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.only(left: 5.0),
            child: Text(domainType == DomainType.verificationApiDomain
                ? 'settings_network_verification_server'.localize
                : 'settings_network_aspe_server'.localize),
          )),
      Row(
        children: [
          Expanded(
            child: Card(
                child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Text(
                          domainType == DomainType.verificationApiDomain
                              ? verificationDomain
                              : aspeDomain,
                        ),
                      ),
                    ),
                  ),
                  IconButton(
                      visualDensity: VisualDensity.compact,
                      onPressed: () async {
                        String? newDomain = await showDialog(
                            context: context,
                            builder: (context) {
                              return DomainDialog(domainType: domainType);
                            });
                        if (newDomain != null) {
                          setState(() {
                            domainType == DomainType.verificationApiDomain
                                ? verificationDomain = newDomain
                                : aspeDomain = newDomain;
                          });
                          await getIt<SettingsCubit>().checkServerVersion();
                        }
                      },
                      icon: const Icon(Icons.edit))
                ],
              ),
            )),
          ),
        ],
      ),
      if ((aspeServerStatus != null || verificationServerStatus != null) &&
          !_isServerUpdateCheckDisabled) ...[
        _isServerCheckInProgress
            ? const Padding(
                padding: EdgeInsets.all(8.0),
                child: LinearProgressIndicator(),
              )
            : ServerVersionStatusWidget(
                serverStatus: domainType == DomainType.verificationApiDomain
                    ? verificationServerStatus!
                    : aspeServerStatus!)
      ]
    ]);
  }

  Widget _authSwitch() {
    return BlocConsumer<LocalAuthenticationCubit, LocalAuthenticationState>(
      bloc: getIt<LocalAuthenticationCubit>(),
      listener: (context, state) {
        if (state is LocalAuthenticationFailure) {
          KeyoxideSnackBar().showOverlaySnackBar(context, state.error.message!);
        }
        if (state is LocalAuthenticationToggleSuccess) {
          isLocalAuthenticationEnabled = !isLocalAuthenticationEnabled;
          getIt<SettingsCubit>()
              .toggleAuthentication(isLocalAuthenticationEnabled);
        }
      },
      builder: (context, state) {
        if (state is LocalAuthenticationToggleSuccess) {
          // Build when toggle success.
        }
        return SettingsSwitch(
            switchValue: isLocalAuthenticationEnabled,
            onChanged: () async {
              await getIt<LocalAuthenticationCubit>()
                  .authenticate(LocalAuthFor.toggleSettings);
            },
            text: 'settings_system_authentication'.localize);
      },
    );
  }

  Widget _buildHideProfileSwitch() {
    return BlocConsumer<LocalAuthenticationCubit, LocalAuthenticationState>(
      bloc: getIt<LocalAuthenticationCubit>(),
      listener: (BuildContext context, LocalAuthenticationState state) {
        if (state is LocalAuthenticationFailure) {
          KeyoxideSnackBar().showOverlaySnackBar(context, state.error.message!);
        }
        if (state is LocalAuthenticationSuccess) {
          _showHiddenProfiles = !_showHiddenProfiles;
          getIt<SettingsCubit>().showHiddenProfiles(_showHiddenProfiles);
        }
      },
      builder: (context, state) {
        if (state is LocalAuthenticationToggleSuccess) {
          // Build when toggle success.
        }
        return SettingsSwitch(
            switchValue: _showHiddenProfiles,
            onChanged: () async {
              isLocalAuthenticationEnabled
                  ? await getIt<LocalAuthenticationCubit>()
                      .authenticate(LocalAuthFor.authentication)
                  : setState(() {
                      _showHiddenProfiles = !_showHiddenProfiles;
                      getIt<SettingsCubit>()
                          .showHiddenProfiles(_showHiddenProfiles);
                    });
            },
            text: 'settings_system_hide_profile'.localize);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Padding(
        padding: const EdgeInsets.all(10),
        child: Container(
          width: double.infinity,
          decoration: BoxDecoration(
            color: Theme.of(context).colorScheme.onSecondary,
            border: Border.all(
                style: BorderStyle.solid,
                color: Theme.of(context).dividerColor),
            borderRadius: BorderRadius.circular(5),
          ),
          child: Column(
            children: [
              const SizedBox(height: 5),
              Text(
                'settings_title_system'.localize,
                style: Theme.of(context).textTheme.titleLarge,
              ),
              const Divider(
                //color: ColorConsts.buttonBackground,
                indent: 40,
                endIndent: 40,
              ),
              const SizedBox(height: 10),
              // Authentication switch
              _authSwitch(),
              const Padding(
                padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 12),
                child: LanguageSelectionWidget(),
              ),
              // Hide profile switch
              _buildHideProfileSwitch(),
              // Disable animations switch
              SettingsSwitch(
                  switchValue: _isAnimationsDisabled,
                  onChanged: () {
                    setState(() {
                      _isAnimationsDisabled = !_isAnimationsDisabled;
                    });
                    getIt<SettingsCubit>()
                        .disableAnimations(_isAnimationsDisabled);
                  },
                  text: 'settings_system_disable_animations'.localize),
              // Disable server update check switch
              SettingsSwitch(
                  switchValue: _isServerUpdateCheckDisabled,
                  onChanged: () {
                    setState(() {
                      _isServerUpdateCheckDisabled =
                          !_isServerUpdateCheckDisabled;
                    });
                    getIt<SettingsCubit>()
                        .disableUpdateCheck(_isServerUpdateCheckDisabled);
                  },
                  text: 'settings_system_disable_update_check'.localize),
              const SizedBox(height: 10),
              Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 5.0),
                    child: Text(
                      'settings_title_network'.localize,
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  )),
              const SizedBox(height: 10),
              BlocConsumer<SettingsCubit, SettingsState>(
                bloc: getIt<SettingsCubit>(),
                listener: (context, state) {
                  if (state is ServerVersionCheckFailure) {
                    aspeServerStatus?.serverCheckError = true;
                    verificationServerStatus?.serverCheckError = true;

                    KeyoxideSnackBar()
                        .showOverlaySnackBar(context, state.error.message!);
                  }
                },
                builder: (context, state) {
                  if (state is ServerVersionChecking) {
                    _isServerCheckInProgress = true;
                  } else {
                    _isServerCheckInProgress = false;
                  }

                  if (state is ServerVersionCheckSuccess) {
                    aspeServerStatus = state.aspeServerStatus;
                    verificationServerStatus = state.verificationServerStatus;
                  }
                  return Column(
                    children: [
                      Padding(
                          padding: const EdgeInsets.only(
                              left: 20.0, right: 20.0, bottom: 10),
                          child: domainCard(DomainType.aspeApiDomain)),
                      Padding(
                          padding: const EdgeInsets.only(
                              left: 20.0, right: 20.0, bottom: 10),
                          child: domainCard(DomainType.verificationApiDomain)),
                    ],
                  );
                },
              ),
            ],
          ),
        ),
      ),
      if (!_isServerUpdateCheckDisabled) ...[
        Positioned(
            right: 24,
            top: 378,
            child: IconButton(
              icon: const Icon(Icons.refresh_outlined),
              onPressed: () async =>
                  await getIt<SettingsCubit>().checkServerVersion(),
            ))
      ]
    ]);
  }
}
