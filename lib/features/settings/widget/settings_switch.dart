import 'package:flutter/material.dart';

class SettingsSwitch extends StatefulWidget {
  SettingsSwitch({required this.switchValue, required this.onChanged, required this.text, super.key});

  bool switchValue;
  VoidCallback onChanged;
  String text;

  @override
  State<SettingsSwitch> createState() => _SettingsSwitchState();
}

class _SettingsSwitchState extends State<SettingsSwitch> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 12),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
              style: BorderStyle.solid, color: Theme.of(context).dividerColor),
          borderRadius: BorderRadius.circular(30),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(left: 17.0),
                child: Text(widget.text),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 17.0),
              child: Switch(
                  value: widget.switchValue,
                  onChanged: (bool isDisabled) async {
                      widget.switchValue = !widget.switchValue;
                      widget.onChanged();
                  }),
            ),
          ],
        ),
      ),
    );;
  }
}
