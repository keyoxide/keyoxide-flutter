import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:url_launcher/url_launcher.dart';

class AppAboutWidget extends StatelessWidget {
  const AppAboutWidget({super.key});

  Future<PackageInfo> _getPackageInfo() {
    return PackageInfo.fromPlatform();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Image.asset(
                    "assets/logo/keyoxide_logo_circle.png",
                    semanticLabel:
                        'settings_about_keyoxide_logo_semantics'.localize,
                    scale: 30,
                  ),
                  const SizedBox(width: 10),
                  const Text('Keyoxide'),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(right: 10.0),
                child: FutureBuilder<PackageInfo>(
                    future: _getPackageInfo(),
                    builder: (BuildContext context,
                        AsyncSnapshot<PackageInfo> snapshot) {
                      final data = snapshot.data;

                      return data != null
                          ? Text(
                              data.version,
                              semanticsLabel:
                                  'settings_about_keyoxide_version_semantics'
                                      .localizeWithPlaceholders([data.version]),
                            )
                          : const Placeholder();
                    }),
              ),
            ],
          ),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              HtmlWidget(
                'settings_about_follow_on'.localize,
                onTapUrl: (url) => launchUrl(Uri.parse(url)),
              ),
            ],
          ),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              HtmlWidget(
                'settings_about_built_by'.localize,
                onTapUrl: (url) => launchUrl(Uri.parse(url)),
              ),
              // GestureDetector(
              //   onTap: () async {
              //     Uri url = Uri.parse(
              //         "https://keyoxide.org/0ae2d5b4de5749838fe54d15009be9f2f3baa000");
              //     if (await canLaunchUrl(url)) {
              //       await launchUrl(
              //         mode: LaunchMode.externalApplication,
              //         url,
              //       );
              //     } else {
              //       final snackBar = SnackBar(
              //         KeyoxideSnackBar().show(context, "Error launching $url");
              //     }
              //   },
              //   child: Row(
              //     children: [
              //       Text(
              //         '@',
              //         style: Theme.of(context)
              //             .textTheme
              //             .bodySmall
              //             ?.copyWith(decoration: TextDecoration.none),
              //       ),
              //       Text(
              //         'Berker',
              //         style: Theme.of(context).textTheme.bodySmall,
              //       ),
              //     ],
              //   ),
              // ),
            ],
          ),
          const SizedBox(height: 10),
          // FittedBox(
          //   child: TextToLink(
          //     urlText:
          //         "© 2021 - ${DateTime.now().year} https://mobile.keyoxide.org",
          //     urlTextStyle: Theme.of(context).textTheme.bodySmall!.copyWith(color: ColorConsts.htmlLinkText),
          //     textStyle: Theme.of(context).textTheme.bodyMedium!,
          //   ),
          // ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FittedBox(
                  child: HtmlWidget(
                'settings_about_keyoxide_mobile'.localize,
                onTapUrl: (url) => launchUrl(Uri.parse(url)),
              )),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: FilledButton(
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(150),
                    ),
                  ),
                  onPressed: () => showLicensePage(context: context),
                  child: SizedBox(
                    width: 70,
                    child: Text(
                      'settings_about_btn_licenses'.localize,
                      semanticsLabel:
                          'settings_about_btn_licenses_semantics'.localize,
                      textAlign: TextAlign.center,
                      // ?.copyWith(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
