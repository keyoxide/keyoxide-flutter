import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';
import 'package:keyoxide_flutter/features/settings/widget/app_about_widget.dart';
import 'package:keyoxide_flutter/features/settings/widget/system_settings_widget.dart';
import 'package:keyoxide_flutter/features/theme/widgets/dyslexic_font_switch.dart';
import 'package:keyoxide_flutter/features/theme/widgets/theme_mode_chips.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io';

import '../../../common/widgets/keyoxide_snackbar.dart';
import '../../theme/widgets/keyoxide_color_picker.dart';

class SettingsView extends StatelessWidget {
  SettingsView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          title: Text(
            'settings_title'.localize,
            semanticsLabel: 'settings_title_semantics'.localize,
            style: Theme.of(context).textTheme.titleLarge,
          ),
          automaticallyImplyLeading: true),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.onSecondary,
                    border: Border.all(
                      style: BorderStyle.solid,
                      color: Theme.of(context).dividerColor,
                    ),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 5,
                      ),
                      Text(
                        'settings_title_theming'.localize,
                        semanticsLabel:
                            'settings_title_theming_semantics'.localize,
                        style: Theme.of(context).textTheme.titleLarge,
                      ),
                      const Divider(
                        //color: ColorConsts.buttonBackground,
                        indent: 40,
                        endIndent: 40,
                      ),
                      const Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.only(
                                  left: 20, right: 20, top: 5.0, bottom: 12),
                              child: ThemeModeChips(),
                            ),
                          ),
                        ],
                      ),
                      const Padding(
                        padding:
                            EdgeInsets.only(left: 20, right: 20, bottom: 12),
                        child: KeyoxideColorPicker(),
                      ),
                      const Padding(
                        padding:
                            EdgeInsets.only(left: 20, right: 20, bottom: 12),
                        child: DyslexicFontSwitch(),
                      ),
                    ],
                  )),
            ),
            const SystemSettingsWidget(),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.onSecondary,
                  border: Border.all(
                    style: BorderStyle.solid,
                    color: Theme.of(context).dividerColor,
                  ),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Column(
                  children: [
                    const SizedBox(height: 5),
                    Text(
                      'settings_title_support'.localize,
                      semanticsLabel:
                          'settings_title_support_section_semantics'.localize,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const Divider(
                      //color: ColorConsts.buttonBackground,
                      indent: 40,
                      endIndent: 40,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: GestureDetector(
                        onLongPress: () {
                          String urlText = 'https://docs.keyoxide.org/';
                          FlutterClipboard.copy(urlText);
                          if (!context.mounted) return;
                          KeyoxideSnackBar().showOverlaySnackBar(
                              context, 'general_txt_clipboard_copy'.localizeWithPlaceholders([urlText])!);
                        },
                        onTap: () async {
                          Uri donateUrl =
                              Uri.parse("https://docs.keyoxide.org/");
                          if (await canLaunchUrl(donateUrl)) {
                            await launchUrl(
                              mode: LaunchMode.externalApplication,
                              donateUrl,
                            );
                          } else {
                            if (!context.mounted) return;
                            KeyoxideSnackBar().showOverlaySnackBar(
                                context, "Error launching $donateUrl");
                          }
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'settings_support_documentation'.localize,
                              semanticsLabel: 'settings_url_launch_semantics'
                                  .localizeWithPlaceholders([
                                'settings_support_documentation'.localize
                              ]),
                            ),
                            const Icon(Icons.open_in_new_outlined),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 10),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: GestureDetector(
                        onLongPress: () {
                          String urlText = 'https://community.keyoxide.org/';
                          FlutterClipboard.copy(urlText);
                          if (!context.mounted) return;
                          KeyoxideSnackBar().showOverlaySnackBar(
                              context, 'general_txt_clipboard_copy'.localizeWithPlaceholders([urlText])!);
                        },
                        onTap: () async {
                          Uri donateUrl =
                              Uri.parse("https://community.keyoxide.org/");
                          if (await canLaunchUrl(donateUrl)) {
                            await launchUrl(
                              mode: LaunchMode.externalApplication,
                              donateUrl,
                            );
                          } else {
                            if (!context.mounted) return;
                            KeyoxideSnackBar().showOverlaySnackBar(
                                context, "Error launching $donateUrl");
                          }
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'settings_support_community_forum'.localize,
                              semanticsLabel: 'settings_url_launch_semantics'
                                  .localizeWithPlaceholders([
                                'settings_support_community_forum'.localize
                              ]),
                            ),
                            const Icon(Icons.open_in_new_outlined),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 10),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: GestureDetector(
                        onLongPress: () {
                          String urlText = 'https://matrix.to/#/#keyoxide-mobile:matrix.org';
                          FlutterClipboard.copy(urlText);
                          if (!context.mounted) return;
                          KeyoxideSnackBar().showOverlaySnackBar(
                              context, 'general_txt_clipboard_copy'.localizeWithPlaceholders([urlText])!);
                        },
                        onTap: () async {
                          Uri donateUrl = Uri.parse(
                              "https://matrix.to/#/#keyoxide-mobile:matrix.org");
                          if (await canLaunchUrl(donateUrl)) {
                            await launchUrl(
                              mode: LaunchMode.externalApplication,
                              donateUrl,
                            );
                          } else {
                            if (!context.mounted) return;
                            KeyoxideSnackBar().showOverlaySnackBar(
                                context, "Error launching $donateUrl");
                          }
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'settings_support_matrix_room'.localize,
                              semanticsLabel: 'settings_url_launch_semantics'
                                  .localizeWithPlaceholders([
                                'settings_support_matrix_room'.localize
                              ]),
                            ),
                            const Icon(Icons.open_in_new_outlined),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 10),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: GestureDetector(
                        onLongPress: () {
                          String urlText = 'https://codeberg.org/berker/keyoxide-flutter/issues';
                          FlutterClipboard.copy(urlText);
                          if (!context.mounted) return;
                          KeyoxideSnackBar().showOverlaySnackBar(
                              context, 'general_txt_clipboard_copy'.localizeWithPlaceholders([urlText])!);
                        },
                        onTap: () async {
                          Uri issues = Uri.parse(
                              "https://codeberg.org/berker/keyoxide-flutter/issues");
                          if (await canLaunchUrl(issues)) {
                            await launchUrl(
                              mode: LaunchMode.externalApplication,
                              issues,
                            );
                          } else {
                            if (!context.mounted) return;
                            KeyoxideSnackBar().showOverlaySnackBar(
                                context, "Error launching $issues");
                          }
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: Text(
                                'settings_support_bug_feature_requests'
                                    .localize,
                                semanticsLabel: 'settings_url_launch_semantics'
                                    .localizeWithPlaceholders([
                                  'settings_support_bug_feature_requests'
                                      .localize
                                ]),
                              ),
                            ),
                            const Icon(Icons.open_in_new_outlined),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 10),
                  ],
                ),
              ),
            ),
            if (!Platform.isIOS)
            ...[Padding(
              padding: const EdgeInsets.all(10),
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.onSecondary,
                  border: Border.all(
                      style: BorderStyle.solid,
                      color: Theme.of(context).dividerColor),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Column(
                  children: [
                    const SizedBox(height: 5),
                    Text(
                      'settings_title_donate'.localize,
                      semanticsLabel:
                          'settings_title_donate_section_semantics'.localize,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const Divider(
                      //color: ColorConsts.buttonBackground,
                      indent: 40,
                      endIndent: 40,
                    ),
                    const SizedBox(height: 10),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: GestureDetector(
                        onLongPress: () {
                          String urlText = 'https://opencollective.com/keyoxide';
                          FlutterClipboard.copy(urlText);
                          if (!context.mounted) return;
                          KeyoxideSnackBar().showOverlaySnackBar(
                              context, 'general_txt_clipboard_copy'.localizeWithPlaceholders([urlText])!);
                        },
                        onTap: () async {
                          Uri donateUrl = Uri.parse("https://opencollective.com/keyoxide");
                          if (await canLaunchUrl(donateUrl)) {
                            await launchUrl(
                              mode: LaunchMode.externalApplication,
                              donateUrl,
                            );
                          } else {
                            if (!context.mounted) return;
                            KeyoxideSnackBar().showOverlaySnackBar(
                                context, "Error launching $donateUrl");
                          }
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Open Collective',
                              semanticsLabel: 'settings_url_launch_semantics'
                                  .localizeWithPlaceholders(
                                      ['Open collective']),
                            ),
                            const  Icon(Icons.open_in_new_outlined),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 10),
                  ],
                ),
              ),
            )],
            Padding(
              padding: const EdgeInsets.all(10),
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.onSecondary,
                  border: Border.all(
                      style: BorderStyle.solid,
                      color: Theme.of(context).dividerColor),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Column(
                  children: [
                    const SizedBox(height: 5),
                    Text(
                      'settings_title_about'.localize,
                      semanticsLabel:
                          'settings_title_about_section_semantics'.localize,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const Divider(
                      //color: ColorConsts.buttonBackground,
                      indent: 40,
                      endIndent: 40,
                    ),
                    const AppAboutWidget(),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
