import 'dart:io';
import 'dart:ui';

import 'package:keyoxide_flutter/features/settings/model/server_status_model.dart';

class UserSettingsModel {
  bool isOnboardingComplete;
  bool isTutorialComplete;
  bool isLocalAuthenticationEnabled;
  String verificationApiDomain;
  String aspeApiDomain;
  String themeMode;
  String seedColor;
  String font;
  String language;
  bool showHiddenProfiles;
  bool disableServerUpdateCheck;
  bool disableAnimations;
  ServerStatusModel verificationServerStatus;
  ServerStatusModel aspeServerStatus;

  UserSettingsModel({
    required this.isOnboardingComplete,
    required this.isTutorialComplete,
    required this.isLocalAuthenticationEnabled,
    required this.verificationApiDomain,
    required this.aspeApiDomain,
    required this.themeMode,
    required this.seedColor,
    required this.font,
    required this.language,
    required this.showHiddenProfiles,
    required this.disableServerUpdateCheck,
    required this.disableAnimations,
    required this.verificationServerStatus,
    required this.aspeServerStatus,
  });

  factory UserSettingsModel.fromJson(Map<String, dynamic> json) {
    return UserSettingsModel(
      isOnboardingComplete: json['isOnboardingComplete'],
      isTutorialComplete: json['isTutorialComplete'],
      isLocalAuthenticationEnabled: json['isLocalAuthenticationEnabled'],
      verificationApiDomain: json['userDomain'],
      aspeApiDomain: json['aspeApiDomain'],
      themeMode: json['themeMode'],
      seedColor: json['seedColor'],
      font: json['font'],
      language: json['language'] ?? '',
      showHiddenProfiles: json['showHiddenProfiles'] ?? false,
      disableServerUpdateCheck: json['disableServerUpdateCheck'] ?? false,
      disableAnimations: json['disableAnimations'] ?? false,
      verificationServerStatus: json['verificationServerStatus'] == null
          ? ServerStatusModel(
              userServerVersion: '',
              latestServerVersion: '',
              isUserServerUpToDate: false,
              serverCheckError: false,
              lastCheckedAt: DateTime.now())
          : ServerStatusModel.fromJson(json['verificationServerStatus']),
      aspeServerStatus: json['aspeServerStatus'] == null
          ? ServerStatusModel(
              userServerVersion: '',
              latestServerVersion: '',
              isUserServerUpToDate: false,
              serverCheckError: false,
              lastCheckedAt: DateTime.now())
          : ServerStatusModel.fromJson(json['aspeServerStatus']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'isOnboardingComplete': isOnboardingComplete,
      'isTutorialComplete': isTutorialComplete,
      'isLocalAuthenticationEnabled': isLocalAuthenticationEnabled,
      'userDomain': verificationApiDomain,
      'aspeApiDomain': aspeApiDomain,
      'themeMode': themeMode,
      'seedColor': seedColor,
      'font': font,
      'language': language,
      'showHiddenProfiles': showHiddenProfiles,
      'disableServerUpdateCheck': disableServerUpdateCheck,
      'disableAnimations': disableAnimations,
      'verificationServerStatus': verificationServerStatus.toJson(),
      'aspeServerStatus': aspeServerStatus.toJson(),
    };
  }
}
