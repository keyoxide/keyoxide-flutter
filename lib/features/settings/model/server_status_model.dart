class ServerStatusModel {
  String userServerVersion;
  String latestServerVersion;
  DateTime lastCheckedAt;
  bool isUserServerUpToDate;
  bool serverCheckError;

  ServerStatusModel({
    required this.userServerVersion,
    required this.latestServerVersion,
    required this.isUserServerUpToDate,
    required this.lastCheckedAt,
    required this.serverCheckError,
  });

  factory ServerStatusModel.fromJson(Map<String, dynamic> json) {
    return ServerStatusModel(
      userServerVersion: json['userServerVersion'],
      latestServerVersion: json['latestServerVersion'],
      isUserServerUpToDate: json['isUserServerUpToDate'],
      serverCheckError: json['serverCheckError'],
      lastCheckedAt: DateTime.parse(json['lastCheckedAt']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "userServerVersion": userServerVersion,
      "latestServerVersion": latestServerVersion,
      "isUserServerUpToDate": isUserServerUpToDate,
      "serverCheckError": serverCheckError,
      "lastCheckedAt": lastCheckedAt.toIso8601String(),
    };
  }
}
