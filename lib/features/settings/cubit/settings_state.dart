part of 'settings_cubit.dart';

abstract class SettingsState extends Equatable {
  const SettingsState();

  @override
  List<Object> get props => [];
}

class SettingsInitial extends SettingsState {}

class SettingsLoading extends SettingsState {}

class SettingChangeSuccess extends SettingsState {}

class SettingChangeFailure extends SettingsState {
  final KeyoxideError error;

  const SettingChangeFailure({required this.error});
}

class ChangeLanguageSuccess extends SettingsState {
  final Languages selectedLanguage;

  const ChangeLanguageSuccess({required this.selectedLanguage});
}

class ShowHiddenProfiles extends SettingsState {
  final bool showHiddenProfiles;

  const ShowHiddenProfiles({required this.showHiddenProfiles});
}

class DomainValidating extends SettingsState {}

class DomainValidationSuccess extends SettingsState {
  final String serverVersion;

  const DomainValidationSuccess({required this.serverVersion});
}

class DomainValidationFailure extends SettingsState {
  final KeyoxideError error;

  const DomainValidationFailure({required this.error});
}

class ServerVersionChecking extends SettingsState {}

class ServerVersionCheckSuccess extends SettingsState {
  final ServerStatusModel aspeServerStatus;
  final ServerStatusModel verificationServerStatus;

  const ServerVersionCheckSuccess(
      {required this.aspeServerStatus, required this.verificationServerStatus});
}

class ServerVersionCheckFailure extends SettingsState {
  final KeyoxideError error;

  final ServerStatusModel aspeServerStatus;
  final ServerStatusModel verificationServerStatus;

  const ServerVersionCheckFailure(
      {required this.error,
      required this.aspeServerStatus,
      required this.verificationServerStatus});
}
