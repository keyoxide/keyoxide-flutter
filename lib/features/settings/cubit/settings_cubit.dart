import 'package:aspe/aspe.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:keyoxide_flutter/common/models/keyoxide_result.dart';
import 'package:keyoxide_flutter/common/services/locator_service.dart';
import 'package:keyoxide_flutter/features/settings/model/server_status_model.dart';
import 'package:keyoxide_flutter/features/settings/repository/settings_repository.dart';
import 'package:keyoxide_flutter/features/settings/widget/language_selection_widget.dart';

import '../../../common/constants/keyoxide_constants.dart';
import '../../user/cubit/user_cubit.dart';
import '../../user/model/keyoxide_user.dart';

part 'settings_state.dart';

class SettingsCubit extends Cubit<SettingsState> {
  SettingsCubit() : super(SettingsInitial());

  Future<void> completeOnboarding() async {
    KeyoxideUser keyoxideUser = getIt<UserCubit>().keyoxideUser;
    keyoxideUser.userSettings.isOnboardingComplete = true;
    await getIt<UserCubit>().updateKeyoxideUser(keyoxideUser);
  }

  Future<void> completeTutorial() async {
    KeyoxideUser keyoxideUser = getIt<UserCubit>().keyoxideUser;
    keyoxideUser.userSettings.isTutorialComplete = true;
    await getIt<UserCubit>().updateKeyoxideUser(keyoxideUser);
  }

  Future<void> toggleAuthentication(bool isAuthEnabled) async {
    KeyoxideUser keyoxideUser = getIt<UserCubit>().keyoxideUser;
    keyoxideUser.userSettings.isLocalAuthenticationEnabled = isAuthEnabled;
    await getIt<UserCubit>().updateKeyoxideUser(keyoxideUser);
  }

  Future<void> changeVerificationDomain(String newDomain) async {
    KeyoxideUser keyoxideUser = getIt<UserCubit>().keyoxideUser;
    keyoxideUser.userSettings.verificationApiDomain = newDomain;
    await getIt<UserCubit>().updateKeyoxideUser(keyoxideUser);
  }

  Future<void> changeASPEDomain(String newDomain) async {
    KeyoxideUser keyoxideUser = getIt<UserCubit>().keyoxideUser;
    keyoxideUser.userSettings.aspeApiDomain = newDomain;
    await getIt<UserCubit>().updateKeyoxideUser(keyoxideUser);
  }

  Future<void> changeThemeMode(String themeMode) async {
    KeyoxideUser keyoxideUser = getIt<UserCubit>().keyoxideUser;
    keyoxideUser.userSettings.themeMode = themeMode;
    await getIt<UserCubit>().updateKeyoxideUser(keyoxideUser);
  }

  Future<void> showHiddenProfiles(bool showHiddenProfiles) async {
    emit(SettingsLoading());
    KeyoxideUser keyoxideUser = getIt<UserCubit>().keyoxideUser;
    keyoxideUser.userSettings.showHiddenProfiles = showHiddenProfiles;
    await getIt<UserCubit>().updateKeyoxideUser(keyoxideUser);
    emit(ShowHiddenProfiles(showHiddenProfiles: showHiddenProfiles));
  }

  Future<void> changeSeedColor(String seedColor) async {
    KeyoxideUser keyoxideUser = getIt<UserCubit>().keyoxideUser;
    keyoxideUser.userSettings.seedColor = seedColor;
    await getIt<UserCubit>().updateKeyoxideUser(keyoxideUser);
  }

  Future<void> changeFont(String font) async {
    KeyoxideUser keyoxideUser = getIt<UserCubit>().keyoxideUser;
    keyoxideUser.userSettings.font = font;
    await getIt<UserCubit>().updateKeyoxideUser(keyoxideUser);
  }

  Future<void> changeLanguage(Languages language) async {
    emit(SettingsLoading());
    KeyoxideUser keyoxideUser = getIt<UserCubit>().keyoxideUser;
    keyoxideUser.userSettings.language = language.name;
    await getIt<UserCubit>().updateKeyoxideUser(keyoxideUser);
    emit(ChangeLanguageSuccess(selectedLanguage: language));
  }

  Future<void> disableUpdateCheck(bool disableUpdateCheck) async {
    KeyoxideUser keyoxideUser = getIt<UserCubit>().keyoxideUser;
    keyoxideUser.userSettings.disableServerUpdateCheck = disableUpdateCheck;
    await getIt<UserCubit>().updateKeyoxideUser(keyoxideUser);
  }

  Future<void> disableAnimations(bool disableAnimations) async {
    KeyoxideUser keyoxideUser = getIt<UserCubit>().keyoxideUser;
    keyoxideUser.userSettings.disableAnimations = disableAnimations;
    await getIt<UserCubit>().updateKeyoxideUser(keyoxideUser);
  }

  Future<void> saveServerStatus(
      {required ServerStatusModel verficationServerStatus,
      required ServerStatusModel aspeServerStatus}) async {
    KeyoxideUser keyoxideUser = getIt<UserCubit>().keyoxideUser;
    keyoxideUser.userSettings.verificationServerStatus =
        verficationServerStatus;
    keyoxideUser.userSettings.aspeServerStatus = aspeServerStatus;
    await getIt<UserCubit>().updateKeyoxideUser(keyoxideUser);
  }

  Future<void> validateASPEDomain(String url) async {
    emit(DomainValidating());
    KeyoxideResult result =
        await getIt<SettingsRepository>().aspeDomainValidationRequested(url);

    if (result.error != null) {
      emit(DomainValidationFailure(error: result.error!));
    } else {
      AspeServer server = result.data as AspeServer;
      emit(DomainValidationSuccess(serverVersion: server.version!));
    }
  }

  Future<void> validateVerificationDomain(String domain) async {
    emit(DomainValidating());
    KeyoxideResult result = await getIt<SettingsRepository>()
        .verificationDomainValidationRequested(domain);

    if (result.error != null) {
      emit(DomainValidationFailure(error: result.error!));
    } else {
      String serverVersion = result.data.toString();
      emit(DomainValidationSuccess(serverVersion: serverVersion));
    }
  }

  Future<void> checkServerVersion() async {
    emit(ServerVersionChecking());
    String aspeDomain =
        getIt<UserCubit>().keyoxideUser.userSettings.aspeApiDomain;
    String verificationDomain =
        getIt<UserCubit>().keyoxideUser.userSettings.verificationApiDomain;
    KeyoxideResult verificationServerResult = await getIt<SettingsRepository>()
        .verificationDomainValidationRequested(verificationDomain);
    KeyoxideResult aspeServerResult = await getIt<SettingsRepository>()
        .aspeDomainValidationRequested(aspeDomain);
    KeyoxideResult latestVerificationServerVersion =
        await getIt<SettingsRepository>()
            .getLatestVersionFromCodebergApiRequested(
                APIConstants.verificationServerProjectName);
    KeyoxideResult latestAspeServerVersion = await getIt<SettingsRepository>()
        .getLatestVersionFromCodebergApiRequested(
            APIConstants.aspeServerProjectName);

    ServerStatusModel failedServerStatus = ServerStatusModel(
        serverCheckError: true,
        lastCheckedAt: DateTime.now(),
        isUserServerUpToDate: false,
        latestServerVersion: '',
        userServerVersion: '');

    if (verificationServerResult.error != null) {
      emit(ServerVersionCheckFailure(
        error: verificationServerResult.error!,
        aspeServerStatus: failedServerStatus,
        verificationServerStatus: failedServerStatus,
      ));
      return;
    }
    if (aspeServerResult.error != null) {
      emit(ServerVersionCheckFailure(
        error: aspeServerResult.error!,
        aspeServerStatus: failedServerStatus,
        verificationServerStatus: failedServerStatus,
      ));
      return;
    }
    if (latestVerificationServerVersion.error != null) {
      emit(ServerVersionCheckFailure(
        error: latestVerificationServerVersion.error!,
        aspeServerStatus: failedServerStatus,
        verificationServerStatus: failedServerStatus,
      ));
      return;
    }
    if (latestAspeServerVersion.error != null) {
      emit(ServerVersionCheckFailure(
        error: latestAspeServerVersion.error!,
        aspeServerStatus: failedServerStatus,
        verificationServerStatus: failedServerStatus,
      ));
      return;
    }

    AspeServer? aspeServer = aspeServerResult.data as AspeServer;

    ServerStatusModel aspeServerStatus = ServerStatusModel(
      latestServerVersion: latestAspeServerVersion.data ?? '',
      userServerVersion: aspeServer.version ?? '',
      isUserServerUpToDate: aspeServer.version == latestAspeServerVersion.data,
      serverCheckError: (aspeServerResult.error != null ||
          latestAspeServerVersion.error != null),
      lastCheckedAt: DateTime.now(),
    );

    ServerStatusModel verificationServerStatus = ServerStatusModel(
      latestServerVersion: latestVerificationServerVersion.data ?? '',
      userServerVersion: verificationServerResult.data ?? '',
      isUserServerUpToDate:
          verificationServerResult.data == latestVerificationServerVersion.data,
      serverCheckError: (verificationServerResult.error != null ||
          latestVerificationServerVersion.error != null),
      lastCheckedAt: DateTime.now(),
    );

    await saveServerStatus(
        verficationServerStatus: verificationServerStatus,
        aspeServerStatus: aspeServerStatus);

    emit(ServerVersionCheckSuccess(
        aspeServerStatus: aspeServerStatus,
        verificationServerStatus: verificationServerStatus));
  }
}
