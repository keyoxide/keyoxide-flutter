import 'package:aspe/aspe.dart';
import 'package:dio/dio.dart';
import 'package:keyoxide_flutter/common/utils/extensions.dart';

import '../../../common/api/crud/dio_crud_api.dart';
import '../../../common/constants/keyoxide_constants.dart';
import '../../../common/models/keyoxide_result.dart';
import '../../../common/services/locator_service.dart';

class SettingsRepository {
  Future<KeyoxideResult> aspeDomainValidationRequested(String url) async {
    try {
      AspeServer server = await AspeServer.fromDomain(url);
      server.version = server.version.toString().split('+')[0];
      return KeyoxideResult(data: server);
    } on AspeException catch (e) {
      KeyoxideError error = KeyoxideError(
        message: e.translationKey.localize,
        code: e.httpStatus,
      );
      return KeyoxideResult(error: error);
    }
  }

  Future<KeyoxideResult> verificationDomainValidationRequested(
      String domain) async {
    try {
      final url =
          Uri.https(domain, APIConstants.serverValidationEndpoint).toString();
      var response = await getIt<DioCrudApi>().get(url);
      String version = response.data.toString().split('/')[1].split('+')[0];
      return KeyoxideResult(data: version);
    } on DioException catch (e) {
      String? message = e.type == DioExceptionType.connectionError
          ? 'dio_exception_connection_error'.localize
          : e.message;
      KeyoxideError error = KeyoxideError(
        message: message,
        error: e.error.toString(),
      );
      return KeyoxideResult(error: error);
    }
  }

  Future<KeyoxideResult> getLatestVersionFromCodebergApiRequested(
      String projectName) async {
    try {
      final uri = Uri.https(APIConstants.codebergApiBaseUrl,
          '${APIConstants.codebergApiUrlPrefix}$projectName${APIConstants.codebergApiEndpoint}');
      var response = await getIt<DioCrudApi>().get(uri.toString());
      String version = response.data['tag_name'].toString();
      return KeyoxideResult(data: version);
    } on DioException catch (e) {
      String? message = e.type == DioExceptionType.connectionError
          ? 'dio_exception_connection_error'.localize
          : e.message;
      KeyoxideError error = KeyoxideError(
        message: message,
        error: e.error.toString(),
      );
      return KeyoxideResult(error: error);
    }
  }
}
