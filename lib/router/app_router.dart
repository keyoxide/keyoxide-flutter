import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:keyoxide_flutter/features/onboarding/view/onboarding_view.dart';
import 'package:keyoxide_flutter/features/settings/view/settings_view.dart';

import '../common/constants/keyoxide_constants.dart';
import '../common/services/locator_service.dart';
import '../features/display_profile/view/display_profile_view.dart';
import '../features/home/home_screen.dart';
import '../features/qr_code_scanner/view/qr_code_scanner_view.dart';
import '../features/user/cubit/user_cubit.dart';
import 'error_page.dart';

class AppRouter {
  AppRouter();

  late final router = GoRouter(
    //TODO: Remove debugLogDiagnostics before shipping the app.
    debugLogDiagnostics: false,

    routes: [
      GoRoute(
        name: rootRouteName,
        path: rootPath,
        builder: (context, state) => HomeScreen(tab: generateProfileRouteName),
      ),
      GoRoute(
        name: onboardingRouteName,
        path: onboardingPath,
        builder: (context, state) => const OnBoardingView(),
      ),
      GoRoute(
        name: displayProfileRouteName,
        path: displayProfilePath,
        pageBuilder: (context, state) => const MaterialPage<void>(
          child: DisplayProfileView(),
        ),
      ),
      GoRoute(
        name: qrCodeScannerRouteName,
        path: qrCodeScannerPath,
        pageBuilder: (context, state) => const MaterialPage<void>(
          child: QrCodeScannerView(),
        ),
      ),
      GoRoute(
          name: settingsRouteName,
          path: settingsPath,
          pageBuilder: (context, state) {
            return CupertinoPage(child: SettingsView());
          }),
      GoRoute(
        name: homeRouteName,
        path: '$homePath/:tab($generateProfileRouteName|$contactsRouteName|$userRouteName)',
        pageBuilder: (context, state) {
          final tab = state.pathParameters['tab']!;
          return MaterialPage<void>(
            key: state.pageKey,
            child: HomeScreen(tab: tab),
          );
        },
      ),
    ],
    redirect: (context, state) {
      bool isOnboardingComplete =
          getIt<UserCubit>().keyoxideUser.userSettings.isOnboardingComplete;

      if (!isOnboardingComplete) {
        return onboardingPath;
      }
      return null;
    },
    errorPageBuilder: (context, state) => MaterialPage<void>(
      key: state.pageKey,
      child: ErrorPage(error: state.error),
    ),
  );
}
