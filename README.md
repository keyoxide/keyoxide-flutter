# Keyoxide Mobile

A modern, secure and privacy-friendly platform to establish your decentralized online identity

Keyoxide allows you to prove “ownership” of accounts on websites, domain names, IM, etc., regardless of your username.

That last part is important: you could, for example, be ‘alice’ on Lobste.rs, but ‘@alice24’ on Twitter.
And if your website is ‘thatcoder.tld’, how are people supposed to know that all that online property is yours?

Of course, one could opt for full anonymity! In which case, keep these properties as separated as possible.
But if you’d like these properties to be linked and, by doing so, establish an online identity, you’ll need a clever solution.

Enter Keyoxide.

When you visit someone’s Keyoxide profile and see a green tick next to an account on some website, 
it was proven beyond doubt that the same person who set up this profile also holds that account.

<b>App features</b>

* Simple and straightforward UI
* Onboarding
* Guided tour
* Fully open source
* Built with Flutter
* Supports fingerprint and email address as identifiers
* Supports fetching, identities via keyservers and Web Key Directory
* Search history
* Contacts feature
* ASP Profile management, create, edit, delete, import, export profiles, add claims directly within the app
* Hide sensitive user profiles and protect with authentication
* Disable animations and update check
* Add multiple PGP or ASP profiles
* Custom domain validation and support for profile upload and look-up
* Check and display server update availability
* Biometric, password, PIN or pattern authentication for profile management
* User defined settings and data is saved in encrypted form
* Deep linking with 'openpgp4fpr' and 'aspe' URL schemes
* Localization, current available languages: German, English, Dutch, French, Galician, Polish, Spanish, Turkish, Portuguese, Chinese, Japanese
* Material 3 design
* Adaptive themeable icons for Android
* Accessibility enhancements such as semantic labels, dyslexic font, choice of seed color.

<b>About Keyoxide</b>

* Identity => verification using bidirectional linking
* Secure => use of trusted cryptography
* Decentralized => user data sovereignty
* Privacy-friendly => data explicitly provided by identity holder

## Contributing

Anyone can contribute!

Developers are invited to:

- fork the repository and play around
- submit [PRs](https://codeberg.org/Berker/keyoxide-flutter/pulls) to [implement new features or fix bugs](https://codeberg.org/berker/keyoxide-flutter/issues)
- use [develop](https://codeberg.org/Berker/keyoxide-flutter/src/branch/develop) branch for PRs

Everyone is invited to:

- find and [report bugs](https://codeberg.org/berker/keyoxide-flutter/issues/new/choose)
- suggesting [new features](https://codeberg.org/berker/keyoxide-flutter/issues/new/choose)
- [help with translations](https://translate.codeberg.org/projects/keyoxide/keyoxide-mobile/)
- [improve documentation](https://codeberg.org/keyoxide/keyoxide-docs)
- start using open source software and promote it

Please note that this project has a [Code of Conduct](https://codeberg.org/keyoxide/web/src/branch/main/CODE_OF_CONDUCT.md) that all contributors agree to abide when participating.

## About the Keyoxide project

The Keyoxide project strives for a healthier internet for all and has made its efforts fully [open source](https://codeberg.org/keyoxide). Our [community](https://docs.keyoxide.org/community/) is open and welcoming, feel free to say hi!

Funding for the project comes from the [NLnet foundation](https://nlnet.nl/), [NGI0](https://www.ngi.eu/) and the people supporting our [OpenCollective](https://opencollective.com/keyoxide). The project is grateful for all your support.

<br/>

[<img src="https://img.shields.io/badge/F_Droid-1976D2?style=for-the-badge&logo=f-droid&logoColor=white"  
alt="Get it on F-Droid"  
height="40">](https://f-droid.org/packages/org.keyoxide.keyoxide) &nbsp;&nbsp; [<img src="https://img.shields.io/badge/Google_Play-414141?style=for-the-badge&logo=google-play&logoColor=white"    
alt="Get it on Google Play"    
height="40">](https://play.google.com/store/apps/details?id=org.keyoxide.keyoxide)  &nbsp;&nbsp; [<img src="https://img.shields.io/badge/App_Store-0D96F6?style=for-the-badge&logo=app-store&logoColor=white"    
alt="Get it on App Store"    
height="40">](https://apps.apple.com/app/keyoxide/id1670664318)

<br/>

![screenshots](./screenshots/screenshots.png)


